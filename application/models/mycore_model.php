<?php

class Mycore_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_groups($user_id){
		
		$this -> db -> select("groups.group_id,group_name");
		$this -> db -> from('groups');
		$this -> db -> join('members',' groups.group_id = members.group_id','left');
		//$this -> db -> where('members.user_id',$userid);
		$this -> db -> where('members.user_id',$user_id);
		
		$query = $this -> db -> get();
		
		return $query -> result();
	}
	
}

?>