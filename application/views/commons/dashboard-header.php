<?php
    $data = $this -> session -> userdata('is_logged');
?>
<div class="header">
    <div class="container">
        <div class="row-fluid">
            <div class="span24 navbar">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a class="brand" href="#"> </a>
                <div class="nav-collapse collapse ">
                    <ul class="nav pull-right">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle username" data-toggle="dropdown"><?php  echo $data['username'];?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript: void(0);" onclick="javascript: dashboard_nav('settings');return false;">Settings</a></li>
                            <li><a href="logout">Logout</a></li>
                        </ul>
                      </li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>
    </div>
</div>