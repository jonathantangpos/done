			<?php $user_id = 2; ?>
			<!--Footer Start=======================-->			
			<div class="row" id="footer">
				<div class="four columns">
					Copyright c 2012 Zoog Technologies
				</div>
				<div class="four columns">
					
						DONE SYSTEM
				
				</div>
				<div class="four columns">
					All Rights Reserved
				</div>
			</div>
		</div>				
		<!--Footer Ends=======================-->			
				
		<!--SCRIPTS Start-->					
			<script type="text/javascript" src="<?php echo load_global_assets('system/js/jquery.foundation.tabs.js')?>"></script>
		<!--SCRIPTS End-->
		
		<!--Custom Scripts Starts-->
			<script type="text/javascript">
				$(document).foundationTabs();	

				$(document).ready(function(){
					var tab_count = 0;					
					preLoad();

					$("#add_group_ok_button").click(function(){
						$.ajax({
							type : "POST",						
							url : "<?php echo base_url();?>form",
							dataType : "json",
							data : $("#new_group_form").serialize(),
							success : function(data){							
								if(data.valid){
									$("#add_group_modal").trigger("reveal:close");
									// Clear input box
									$("#new_group_input").val("");
									//console.log(data);
									//updates vtabs
									$.ajax({
										type : "POST",									
										url : "<?php echo base_url();?>connections/render_vtabs",
										dataType : "html",
										data : "&user_id="+<?php echo $user_id; ?>,//user_id goes here
										success : function(data){
											$("#ver_tabs").replaceWith("<div class='three columns' id='ver_tabs'>"+data+"</div>");											
											preLoad();
										}
									});
								}else{
									$("#errMsg").replaceWith("<div id='errMsg'>"+data.msg+"</div>");
								}
							}
						});
					});


				});
				
				/**
				 * Description: Loads the first tab's content automatically by $(document).ready()
				 * Params: none
				 **/
				function preLoad(){
					tab_count = $("dl #ver_tab").length;
																
					$("#new_group_button").parent().removeAttr("class");					
					$("#ver_tab:nth-child(1)").attr("class","active");
					var group_name = $("#ver_tab_a").contents().filter(function(){return this.nodeType == 3;})[0]['data'];
					$.ajax({
						type : "POST",
						dataType: "json",
						url : "index.php/connections/ajax_func",
						cache: false,
						data : "&group_name=" + group_name,						
						success : function (data){		
						$('#group_content').html(data.groupcontent);
						}			
					});						
				}	

				/**
				 * Description: An ajax function to load the groupcontent_widget.php
				 * Params: group_name
				 **/
				function ajaxFunc(x, group_name){	
					var pos = $(x).closest("dd").index();						
					$.ajax({
						type : "POST",
						dataType: "json",
						url : "index.php/connections/ajax_func",
						data : "&group_name=" + group_name,						
						success : function (data){		
							$('#group_content').html(data.groupcontent);
						}		
					});
				 }

				/*
				 * Desc: Sets errMsg back to empty,
				 * sets input type back to placeholder,
				 * unselect new group tab
				 */
				$(".close_modal").click(function(){																											
					$("#errMsg").replaceWith("<div id='errMsg'></div>");
					$("#new_group_input").val("");														
				});
			</script>	
		<!--Custom Scripts Ends-->
	</body>		
</html>