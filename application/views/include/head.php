<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
	<title>test</title>
	<head>
		<link href="<?php echo load_global_assets('system/css/foundation.css')?>" type="text/css" rel="stylesheet" />		
		<link href="<?php echo load_global_assets('system/css/fc-webicons.css')?>" type="text/css" rel="stylesheet" />
		<link href="<?php echo load_global_assets('system/css/fileuploader.css')?>" type="text/css" rel="stylesheet" />
		<link href="<?php echo load_global_assets('fancybox/jquery.fancybox.css')?>" type="text/css" rel="stylesheet" />
		<style type="text/css">		
			.container{
				padding: 0 20px;				
				
			}
			#header{
				
				background: #419fd4;
				color: #fff;				
			}
			#neck{
				
			}
			#content{
				
			}
			#footer{
				
			}
			.one.column{
				border: solid white 1px;
			}
			.row{
				position: relative;
			}
			div#user-settings{
				position: absolute;
				bottom: 10px;
				right: 0;
				text-align: right;
			}		
			ul#user-settings-list{
				margin: 0;
			}
			ul#user-settings-list li{
				list-style: none;
				display: inline;					
			}
			ul#user-settings-list li img{
				margin-bottom: -3%;
			}
			
			.tab-content{
				padding: 20px;
				border-right: solid #e6e6e6 1px;
				border-bottom: solid #e6e6e6 1px;
				border-left: solid #e6e6e6 1px;					
			}			
			.connection-list li{
				list-style: none;
				display: inline;
				/*padding: 0 30px;*/
				margin: 0 auto;
				height: auto;
			}
			.connection-list li img{
				margin-bottom: -3.5%;						
			}
			.connection-list li img, .connection-list li input{
				display: inline;						
			}
			
			.tab-content .panel{
				text-align: center;		/*center tab contents*/	
			}
			
			#groups{
				background: #f2f2f2;
				padding-bottom: 15px;
				margin-bottom: 15px;
				/*border: solid #e6e6e6 1px;*/
			}
			#groups h4{
				padding-left: 15px;
				text-align: left;
			}
			.user-box{
				border: solid #e6e6e6 1px;
				padding: 5px;
				text-align: center;
				background: #fff;
				margin-bottom: 10px;
				display: inline-block;
			}
			.user-box img{
				width: 50px;
				height: 50px;
				border: solid #fff 1px;
				float: left;
			}
			.user-box #user_desc{
				font-style: italic;
				float: left;
				padding-left: 10px;
			}
			
			div#footer{
				border: solid #e6e6e6 1px;		
				background: #419fd4;
			}
			div#footer div{		
				padding: 20px 0;
				text-align: center;
				color: #fff;				
			}
			
			/*Modals CSS*/			
			#modal_head{
				background: #419FD4;
				margin-bottom: 10px;
				padding: 10px;
				font-weight: bold;
				color: #fff;
			}
			#modal_content{
				padding: 10px;
			}
			#modal_head #x_modal{
				position: absolute;
				top: 10px;
				right: 15px;
				font-size: 1.5em;
				line-height: 1em;	
				cursor: pointer;			
			}
			#modal_head #x_modal:hover{
				color: blue;				
			}
			/*Ovverried reveal-modal padding which is 30px*/
			.reveal-modal{ 
				padding: 5px !important;
			}
			.member-remove{
				position: absolute;
				top: -15px;
				right: 0;
				width: 36px;
				height: 36px;
				cursor: pointer;
				background-image:url("<?php echo load_global_assets('fancybox/fancybox_sprite.png')?>");				
			}
			#errMsg{
				color: red;
			}
			
			
		</style>
		
		<!--SCRIPTS Start-->			
			<script type="text/javascript" src="<?php echo load_global_assets('system/js/foundation.min.js')?>"></script>			
			<script type="text/javascript" src="<?php echo load_global_assets('system/js/fileuploader.js')?>"></script>
			<script type="text/javascript" src="<?php echo load_global_assets('fancybox/jquery.fancybox.js')?>"></script>
		<!--SCRIPTS End-->
		</head>