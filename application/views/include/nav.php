<?php $userdata = $this->session->userdata('is_logged');?>
<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<button class="btn btn-navbar" type="button" data-toggle="collapse" data-target=".nav-collapse">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="brand" href="#">Done System</a>
			<div class="nav-collapse collapse">
				<ul class="nav pull-right">
					<li><a href="#">Welcome back <?php echo $userdata['username'];?> </a></li>
					<li class="dropdown"><a class="dropdown-toggle" id="dLabel"  role="button"  data-toggle="dropdown" data-target="#" href="#"><b class="icon-envelope"> </b> Notification <b class="caret"></b></a>								
						<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
							<li class=""><a href="#">Bryan invited you to join BLUH Group</a></li>
							<li class="divider"></li>
							<li class=""><a href="#">Hara accepted your request.</a></li>
							<li class="divider"></li>
							<li class=""><a href="#">Nino added you in Chugs Group.</a></li>
						</ul>
					</li>
					<li class="dropdown"><a class="dropdown-toggle" id="dLabel"  role="button" data-toggle="dropdown" data-target="#" href="#"><b class="icon-cog"> </b> Settings <b class="caret"></b></a>								 
						<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
							<li class=""><a href="<?php echo base_url('profile')?>"><b class="icon-user"> </b> Profile</a></li>
							<li class=""><a href="<?php echo base_url('profile')?>"><b class="icon-signal"> </b> Account</a></li>
							<li class=""><a href="<?php echo base_url('logout')?>"><b class="icon-off"> </b> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>