<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bootstrap</title>
		<link type="text/css" rel="stylesheet" href="<?php echo load_global_assets("bootstrap/css/bootstrap.min.css")?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo load_global_assets("bootstrap/css/bootstrap-responsive.min.css")?>" />
		<script type="text/javascript" src="<?php echo load_global_assets("system/js/jquery.js")?>"></script>
		<script type="text/javascript" src="<?php echo load_global_assets("bootstrap/js/bootstrap.min.js")?>"></script>
		
		<style type="text/css">
			body{
				margin: 	0 20px;
				color: 		#ffffff;
				}
			#header{
				background-color: #419FD4;
				}
			#content{
				background-color: #F2F2F2;
				color:		#333;
				padding:	20px;
				}
			h3{
				padding:	0 20px;
				}
			.test{
				border:		solid red 1px;
				}
			.well{
				color:		#333;
				}
		</style>
	</head>
	<body>
		<div class="row-fluid">
			<div class="row-fluid" id="header">
				<div class="span7">
					<h3>Twitter Bootstrap</h3>
				</div>
				<div class="span5">
					<div class="row">						
						<div class="span6 offset6"></div>
					</div>
					<div class="row">						
						<div class="span8 offset4">Welcome back Samsung! | Notifications | Settings</div>
					</div>
				</div>
			</div>
			<div class="row-fluid" id="content">
				<div class="span12 centered">					
					<h1 class="large">Bootstrap v2.2.1</h1>
					<p>The quick brown fox jumps over the lazy dog near the riverbank.</p>
				</div>
				<div class="span12 centered">
					<img src="" class="img-polaroid" style="width: 300px; height: 300px;"/>
				</div>
			</div>
		</div>
	</body>
</html>