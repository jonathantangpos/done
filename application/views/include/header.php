<body>	
	<div id="main" class="container">
		<!--Header Starts=======================-->			
		<div class="row" id="header">
			<div class="three columns">
				<h2 id="log">Welcome!</h2>
			</div>
			<div class="five columns offset-by-four" id="user-settings">
				<ul id="user-settings-list">
					<a href="#"><li><img src="<?php echo load_global_assets('images/icons/balloon.png')?>" style="width: 32px; height: auto;" /></li></a>
					<li><span><?php echo $name; ?></span></li>
					<a href="#"><li><img src="<?php echo load_global_assets('images/icons/gear.png')?>" style="width: 32px; height: auto;" /></li></a>
				</ul>
			</div>
		</div>
				
		<!--Header Ends=======================-->			