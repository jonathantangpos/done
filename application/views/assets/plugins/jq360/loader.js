/*
 Loader function for easy integration of the javascript viewer
 * Version 2.2
 * Basic loader for the javascriptviewer, it loads the nessecary javascript and css files
 * Give your main image a id (default 'javascriptviewer').
 * Go to http://www.360-javascriptviewer for more information how to use this loader
 */

function loader(params){

    this.defaultLicense = '0x98552b40f35a7ada3df8a437a0014b9f';
    this.defaultTag = 'javascriptviewer';
    this.defaultZoom = false;
	this.defaultSpeed = 8.3;
	this.defaultEnding = 6;
    this.errors = false; // display errors
    this.fireBug = true; // log errors in firebug else alerts
    this.defaultPath = "http://360-javascriptviewer.appspot.com/latest";
    this.defaultFrames = 72;
    this.defaultDirection = true;
    this.useStartButton = true;
    this.chkLoaded = true;
    window.drag = 1;
    
    
    
    
    /*
     * You can change the content behind this line if you are a little familiar
     * with javascript If you are not that technical and you want some
     * customization please ask your web developer or contact us at
     * support@360-javascriptviewer.com
     */
    this.started = false;
    if (typeof params.tag !== "undefined" && params.test !== "") {
        this.tag = params.tag;
    }
    else {
        this.tag = this.defaultTag;
    }
	
	if (typeof params.speed !== "undefined" && params.test !== "") {
        this.speed = params.speed;
    }
    else {
        this.speed = this.defaultSpeed;
    }
	
	if (typeof params.ending !== "undefined" && params.test !== "") {
        this.ending = params.ending;
    }
    else {
        this.ending = this.defaultEnding;
    }
	
	if (typeof params.tag !== "undefined" && params.test !== "") {
        this.tag = params.tag;
    }
    else {
        this.tag = this.defaultTag;
    }
    
    if (typeof params.zoom !== "undefined" && params.test !== "") {
        this.zoom = params.zoom;
    }
    else {
        this.zoom = this.defaultZoom;
        
    }
    
    if (typeof params.frames !== "undefined" && params.frames !== "") {
        this.frames = params.frames;
        this.useCache = true;
    }
    else {
        this.frames = this.defaultFrames;
        this.useCache = false;
    }
    
    if (typeof params.license !== "undefined" && params.license !== "") {
        this.license = params.license;
    }
    else {
        this.license = this.defaultLicense;
    }
    if (typeof params.chkLoaded != "undefined") {
        this.chkLoaded = params.chkLoaded;
    }
    else {
        this.chkLoaded = this.chkLoaded;
    }
    
    if (typeof params.path !== "undefined") {
        this.path = params.path;
    }
    else {
        this.path = this.defaultPath;
    }
    
    if (typeof params.direction !== "undefined") {
        this.direction = params.direction;
    }
    else {
        this.direction = this.defaultDirection;
    }
    
    if (typeof params.tplImageUrl !== "undefined") {
        this.tplImageUrl = params.tplImageUrl;
    }
    else {
        this.tplImageUrl = "";
    }
    
    
    if (typeof(arrSingleton) === "undefined") {
        arrSingleton = [];
    }
    arrSingleton[this.tag] = this;
    var self = this;
    
    if (this.chkLoaded) {
        arrSingleton[this.tag].alreadyrunflag = 0;
        if (document.addEventListener) {
            document.addEventListener("DOMContentLoaded", function(){
                if (self.alreadyrunflag === 0) {
                    self.alreadyrunflag = 1;
                    this.tmo = setTimeout(function(){
                        self.begin();
                    }, 1000);
                }
            }, false);
        }
        else 
            if (document.all && !window.opera) {
                this.tmo = setTimeout(function(){
                    self.begin();
                }, 10);
            }
    }
    else {
        this.tmo = setTimeout(function(){
        
            self.begin();
        }, 100);
    }
    
    
    this.begin = function(){
    
        try {
        
            if (document.getElementById(this.tag).src === null) {
                if (this.errors) {
                    alert("could not find image, make sure the image has the id " + tag);
                }
            }
            else {
                this.loadjscssfile(this.path + "/js/3dweb.latest.js", "js");
                this.loadjscssfile(this.path + "/js/position.js", "js");
                
                self = this;
                this.tmo = setTimeout(function(){
                    this.tag = self.tag;
                    arrSingleton[this.tag].start3D();
                }, 2000);
                
            }
        } 
        catch (err) {
            if (this.errors) {
                // console.log(err);
            }
        }
    };
    
    this.loadjscssfile = function(filename, filetype){
        if (filetype === "js") { // if filename is a external JavaScript file
            var fileref = document.createElement('script');
            fileref.setAttribute("type", "text/javascript");
            fileref.setAttribute("src", filename);
        }
        else 
            if (filetype === "css") { // if filename is an external CSS
                // file
                var fileref = document.createElement("link");
                fileref.setAttribute("rel", "stylesheet");
                fileref.setAttribute("type", "text/css");
                fileref.setAttribute("href", filename);
            }
        if (typeof fileref !== "undefined") {
            document.getElementsByTagName("head")[0].appendChild(fileref);
        }
    };
    
    
    
    this.startPres = function(){
        var self = this;
        self.started = true;
        
        this.jsv = new Create3D();
        this.jsv.startValues({
            totalFrames: this.totalFrames,// amount of pictures
            imageUrl: this.tplImageUrl, // 'http://www.yourdomain/image_[#frame].jpg'
            imageUrlBig: this.tplImageUrlBigPres, // 'http://www.yourdomain/big/image_[#frame].jpg',
            info: this.errors,// shows de console with notifications and
            // errors
            tag: this.tag, // The id of the image
            pictureDirection: this.direction, // true for right false for left
            rotationSpeed: this.speed, // 1 is slow, 10 is fast
            rotationEnding: this.ending,// slowing down after rotation
            license: this.license,
            clickCallback: "zoomOption('" + self.tag + "')"
        });

        
        this.removeLoader();
        var tag=this.tag;
        this.jsv.start();
        this.jsv.customEvents.addListener("loaded", functionAfterLoad(tag));
        this.jsv.customEvents.addListener("drag", functionDuringDrag);
        
        
    };
    this.createLoader = function(position){
        var loaderHolder = document.createElement("div");
        loaderHolder.style.position = "absolute";
        loaderHolder.style.top = (position.top - 25) + 'px';
        loaderHolder.style.left = (position.left - 25) + 'px';
        loaderHolder.style.height = "50px";
        loaderHolder.style.width = "50px";
        loaderHolder.style.backgroundImage = "url(" + this.path + "/images/loader-bg.png)";
        loaderHolder.id = "jvs_loader";
        document.body.appendChild(loaderHolder);
        
        if (this.zoom) {
            var oImg = document.createElement("img");
            oImg.setAttribute('src', this.path + '/images/zoomloader.gif');
            oImg.setAttribute('alt', 'na');
            oImg.style.height = "30px";
            oImg.style.width = "30px";
            oImg.style.marginLeft = "10px";
            oImg.style.marginTop = "10px";
            loaderHolder.appendChild(oImg);
        }
    };
    
    this.removeLoader = function(){
        if (document.getElementById("jvs_loader") != null) {
            var node = document.getElementById("jvs_loader");
            var nodeParent = node.parentNode;
            nodeParent.removeChild(node);
        }
    };
    
    this.start3D = function(){
    
        this.img = document.getElementById(this.tag);
        var positie = Position.getCenter(this.tag);
        this.createLoader(positie);
        this.imgUrl = this.img.src;
        this.extension = this.getFileExtension(this.imgUrl);
        if (this.tplImageUrl == "") {
            this.tplImageUrl = this.getTplImageUrl(this.imgUrl, this.extension);
        }
        
        
        if (this.zoom) {
            this.tplImageUrlBigPres = this.getUrlTplBig(this.tplImageUrl);
        }
        else {
            this.tplImageUrlBigPres = "";
        }
        
        
        // looping for preloading and determining amount of pictures
        this.counterLoaded = 0;
        this.counterError = 0;
        var maxFrames = 72;
        this.arrResult = [];
        var self = this;
        var tplImageUrlPres = this.tplImageUrl;
        
        this.imagesQueue = this.imagesQ;
        
        for (frameNo = 1; frameNo <= this.frames; frameNo++) {
            var frame = (frameNo < 10 ? '0' : '') + frameNo;
            var result = tplImageUrlPres.replace("[#frame]", frame);
            if (!(this.useCache)) { // we know the number of images
                var now = (new Date).getTime();
                result = result + '?time=' + now;
            }
            this.arrResult[frameNo - 1] = result;
        }
        
        this.imagesQueue.onLoaded = function(){
            this.tag = self.tag;
            self.counterLoaded++;
            if (self.counterLoaded + self.counterError == self.frames) {
                self.totalFrames = self.counterLoaded;
                self.startPres();
            }
        };
        
        this.imagesQueue.onError = function(error){
        
            this.tplImageUrlBig = "";
            if (!(self.useCache)) {
                self.totalFrames = self.counterLoaded;
                if (self.started === false) {
                    self.startPres();
                }
            }
        }
        this.imagesQueue.queue_images(this.arrResult);
        this.imagesQueue.process_queue();
        
    };
    
    this.getTplImageUrl = function(imgUrl, extension){
        switch (extension) {
            case 'jpg':
                var tplImageUrl = imgUrl.replace(".jpg", "_[#frame].jpg");
                break;
                
            case 'jpeg':
                var tplImageUrl = imgUrl.replace(".jpg", "_[#frame].jpeg");
                break;
                
            case 'gif':
                var tplImageUrl = imgUrl.replace(".gif", "_[#frame].gif");
                break;
                
            case 'png':
                var tplImageUrl = imgUrl.replace(".png", "_[#frame].png");
                break;
                
            default:
                if (this.errors) {
                    alert("could not determine the extension: " + extension);
                    return false;
                }
        }
        return tplImageUrl;
    };
    
    
    this.getFileExtension = function(filename){
        var ext = /^.+\.([^.]+)$/.exec(filename);
        return ext == null ? "" : ext[1];
    };
    
    this.getUrlTplBig = function(tplImageUrl){
        var splitTpl = tplImageUrl;
        var y = splitTpl.lastIndexOf('/');
        var ys = splitTpl.slice(y);
        var ysb = '/big' + ys;
        tplImageUrlBigPres = splitTpl.replace(ys, ysb);
        return tplImageUrlBigPres;
    };
    
    this.errorHandling = function(errMsg){
        if (this.errors) {
            if (this.fireBug) {
             //   console.log(errMsg);
            }
            else {
                alert(errMsg);
            }
        }
        else {
        
        }
    };
    /*
     * imagesQueue.js FULL SOURCE A simple, cross-browser, *parallel* images
     * loader object. Check http://labs.lieldulev.com/imagesQueue/ for more
     * details.
     */
    this.imagesQ = {
        onComplete: function(){
        },
        onLoaded: function(){
        },
        onError: function(){
        },
        current: null,
        qLength: 0,
        images: [],
        inProcess: false,
        queue: [],
        queue_images: function(){
            var arg = arguments;
            var i = 0;
            for (i = 0; i < arg.length; i++) {
                if (arg[i].constructor === Array) {
                    this.queue = this.queue.concat(arg[i]);
                }
                else 
                    if (typeof arg[i] === 'string') {
                        this.queue.push(arg[i]);
                    }
            }
        },
        process_queue: function(){
            this.inProcess = true;
            this.qLength += this.queue.length;
            while (this.queue.length > 0) {
                this.load_image(this.queue.shift());
            }
            this.inProcess = false;
        },
        load_image: function(imageSrc){
            var th = this;
            var im = new Image();
            im.onerror = function(){
                (th.onError)(im.src);
                
            };
            im.onload = function(){
                th.current = im;
                th.images.push(im);
                (th.onLoaded)();
                if (th.queue.length > 0 && !th.inProcess) {
                    th.process_queue();
                }
                
                if (th.qLength == th.images.length) {
                    (th.onComplete)();
                }
            };
            im.src = imageSrc;
        }
    };
}


function zoomOption(tag){

    dwebObject.arrPres[tag].animation.stop(); // stop all animations
    var z = dwebObject.arrPres[tag].info.totalFrames;
    var y = dwebObject.arrPres[tag].info.frameNo;
    var imgUrlBig = dwebObject.arrPres[tag].info.imageUrlBig;
    
    var frame = (y < 10 ? '0' : '') + y;
    var imgSrc = imgUrlBig.replace("[#frame]", frame);
    CZjQuery('.link_3dweb_' + y).unbind();
    var im = new Image();
    im.onerror = function(){
        return false;
        
    };
    im.onload = function(){
    
        CZjQuery('.link_3dweb_' + y).CloudZoom();
        
        
    };
    im.src = imgSrc;
    
    if (document.getElementById("messagezoom") != null) {
        var node = document.getElementById("messagezoom");
        var nodeParent = node.parentNode;
        nodeParent.removeChild(node);
    }
    
    
}

function functionAfterLoad(tag){


    this.tmp = setTimeout(function(){
        var object = dwebObject.arrPres[tag];
        var source = object.source;
        source.startAnimation(5000, 360, 5, -2, 1.5);
        
        
    }, 1000);
    
    var position = Position.getCenter(this.tag);
	var object = dwebObject.arrPres[this.tag];
    var imageUrlBig = dwebObject.arrPres[tag].info.imageUrlBig;
    
    
    
//    var message = document.createElement("img");
//    message.style.position = "absolute";
//    message.style.top = (position.top - (position.height / 2)) + 35 + 'px';
//    message.style.left = (position.left + (position.width / 2)) - 35 + 'px';
//    message.style.height = "50px";
//    message.style.width = "50px";
//    message.style.zIndex = "2000";
//    message.src = "http://360-javascriptviewer.appspot.com/latest/images/logo_ready.png";
//    message.id = "message";
//    document.body.appendChild(message);
    
    if (imageUrlBig != "") {
    
        var messagezoom = document.createElement("img");
        
        messagezoom.style.position = "absolute";
        messagezoom.style.top = (position.top - (position.height / 2)) + 35 + 'px';
        messagezoom.style.left = (position.left + (position.width / 2)) - 35 + 'px';
        messagezoom.style.height = "50px";
        messagezoom.style.width = "50px";
        messagezoom.style.zIndex = "1900";
        messagezoom.style.visibility = "hidden";
        messagezoom.src = "http://360-javascriptviewer.appspot.com/latest/images/logo_ready_zoom.png";
        messagezoom.id = "messagezoom";
        document.body.appendChild(messagezoom);
    }
    
}

function functionDuringDrag(){

    window.drag++;
    if (window.drag > 1) {
        if (document.getElementById("message") != null) {
            var node = document.getElementById("message");
            var nodeParent = node.parentNode;
            nodeParent.removeChild(node);
            if (document.getElementById("messagezoom") != null) {
                var messagezoom = document.getElementById("messagezoom");
                messagezoom.style.visibility = "visible";
            }
            
        }
    }
    
}


