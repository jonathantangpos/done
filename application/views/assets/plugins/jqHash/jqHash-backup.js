/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$.extend({
    hash:function(options){
        console.log('hash parameters',options);
        /*
         * check if variables are defined.
         */
        if(typeof(options)==='undefined'){
            alert('url and container is required');
            console.log('hash errors >','url and container is required');
        }else{
            
            //container
            var containerText =  options.hasOwnProperty('container') ? (options.container!=='' ? '':'container should not empty') : 'container is required';

            //url
            var urlText =  options.hasOwnProperty('url') ? (options.url!=='' ? '':'url should not empty') : 'url is required';
           
           if(containerText+urlText!=''){
                //log errors
                console.log('hash errors >','\n\t'+containerText+'\n\t'+urlText);
           }else{
                var options = $.extend(options);
                /*
                 * making request
                 */
                return $.ajax({
                    type:'POST',
                    url:options.url,
                    success: function(data){
                       $(options.container).html(data);
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown){
                         $(options.container).html(XMLHttpRequest.responseText);
                    }
                });
           }
        }
    }
});

