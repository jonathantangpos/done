jQuery.noConflict();
jQuery(document).ready(function($){
    $.extend({
        /*
        * init functions.
        */
        setHash: function(options){
            data = {
                vars: options
            }
            $.extend({'jqHash':data});
            /*
            *setting methods.
            */
            return this;
        },
        constructHash:function(callback){
            $(window).bind('hashchange',function(){
                var url = $.param.fragment();
                options = {
                    'currentHash':url
                }
                $.extend(true,$.jqHash['vars'],options);
                if($.jqHash['vars']['logVars']==true){
                    $.logHash();
                }

                /*
                *running hash
                */
                var vars = $.jqHash['vars'];
                if($(vars['container']).length!=0){
                    $(vars['container']).each(function(){
                        if(vars['currentHash']==''){
                            vars['currentHash']=vars['defaultHash'];
                        }
                        $(this).load(vars['base']+vars['currentHash'],function(responseText,status){
                            if(status=='error'){
                                $(this).html(responseText);
                            }
                            /*
                            * callbacks
                            */
                            if (typeof callback == 'function') { // make sure the callback is a function
                                callback.call(this); // brings the scope to the callback
                            }
                        });
                    });
                }else{
                    alert('container doesn\'t exist');
                }
            });
            $(window).trigger('hashchange');
            return this;
        },
        logHash:function(){
            if(typeof console!=="undefined"){
                console.log($.jqHash['vars']); 
            }
        }
        /*
        * access functions.
        */
    });
});