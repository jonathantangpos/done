function dashboard_nav(tab){
	
	var container = $('#dashboard-container');
	
	//.html(tab);
	$.ajax({
		
		url:tab,
		dataType: 'html',
		success:function(data){
			
			container.html(data);
		},
		error:function (data){
			container.html("cannot connect to server");
		}
		
	});

}
function get_members(groupid,url){

	$.ajax({
	    url: "groups/members",
	    data: {'group_id': groupid}, //<--- Use Object
	    type: "post",
	    dataType: "JSON",
	    success: function(response){

	       	var values = eval(response);
	       	//console.log(values[values.length-1]);
	       	
	       	for(var x  = 0 ; x < values.length; x++){
	       		var html = "<div class='span3'><div class='member-container'><img src='"+url+
	       					values[x].avatar+"'><a href='#inf_"+x+values[x].username+"' data-toggle='modal'>"+values[x].username+"</a></div> <div class='modal hide fade image-preview-container' id='inf_"+x+values[x].username+
	       					"''><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button></div><div class='image-preview'><img src='"+url+values[x].avatar+
	       					"'/><p>"+values[x].username+"</p></div></div>";


	       		$("#members_view"+groupid).append(html);
	       		
	       	}
	       	
	      	
	    }
	});
	
	//console.log(members);
	return true;
}
function delete_group(id){
	
	
	$.ajax({
		url: "group/delete",
		data: {groupid: id},
		type: "post",
		dataType: "JSON",
		success: function(data){
			
			
			
		}
	});
	var alerts = "<div class='alert alert-success register-error'><button type='button' class='close' data-dismiss='alert'>&times;</button><p>Successfully deleted the group.</p></div>";
	$('#alerts-cont').html(alerts).fadeOut(4000);
	dashboard_nav('groups');
}