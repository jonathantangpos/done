/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    /*
     *  display errors in the page.
     */
    populate_errors = function(json_data){
        if(json_data.length!=0){
            /*
             * display generl message
             */
            $('.smc-general-notification').html(json_data['message']);
            /*
             * display response messages
             */
            $.each(json_data['data'], function(id,value){
                $('.smc-errors #'+id).html(value);
                $('input[name="'+id+'"]').css('border-color','red');
            });
        }
    }
});

