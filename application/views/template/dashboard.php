<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Groups</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <?php $this -> load -> view('loader/head_loader');?>
		<script>
			$(document).ready(function(){

                 <?php
                    $tag = $this -> input -> get('p');

                    if(empty($tag)){
                        $tag = $this->session->userdata('sess_tag');
                    }                                    

                    if(!empty($tag)){
                        echo "dashboard_nav('".$tag."')";                  
                    }else{
                        echo "dashboard_nav('logs')";
                    }

                    $this->session->unset_userdata('sess_tag');
                ?>


				// dashboard_nav('groups');
			});	
		</script>
    </head>
    <?php
    $data = $this -> session -> userdata('is_logged');
    ?>
    <body >
        <div class="header">
            <div class="container">
                <div class="row-fluid">
                    <div class="span24 navbar">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </a>
                        <a class="brand" href="#"> </a>
                        <div class="nav-collapse collapse ">
                            <ul class="nav pull-right">
                              <li class="dropdown">
                                <a href="#" class="dropdown-toggle username" data-toggle="dropdown"><?php  echo $data['username'];?> <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript: void(0);" onclick="javascript: dashboard_nav('settings');return false;">Settings</a></li>
                                    <li><a href="logout">Logout</a></li>
                                </ul>
                              </li>
                            </ul>
                        </div><!--/.nav-collapse -->
                    </div>
                </div>
            </div>
        </div>
        <div class="body">
            <div class="container" >
				<div id="alerts-cont">
					<!-- start error alerts -->
               
					<?php 
						
						if(!empty($_GET['err'])){
							$err = $_GET['err'];
							$alert = '';
							if($err==1) $alert = 'error';
							else if($err==2) $alert = 'error';
							else if($err==0) $alert = 'success';
							
							?>
							<div class="alert alert-<?php echo $alert; ?> register-error">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<?php 
								
									if($err==1) echo 'Password Mismatch!';
									else if($err==2) echo 'Check the fields you inputted';
									else if($err==3) echo 'Successfully updated your password';
								
								?>
							</div>

							<?php
						}else{
						}

					?>
					 <!-- end error alerts -->
				</div>
                <div class="span24 dashboard-wrapper">
                    <div class="dashboard-header-nav">
                        <div class="row-fluid">
                            <div class="span8 offset1 ">
                                <ul class="nav nav-pills dashboard-nav">
                                  <li ><a href="javascript: void(0);" onclick="javascript: dashboard_nav('logs');return false;"><img src="<?php echo base_url('application/views/assets/app/img/log-icon.png');?>"> <br />Logs</a></li>
                                  <li ><a href="javascript: void(0);" onclick="javascript: dashboard_nav('notifications');return false;"><img src="<?php echo base_url('application/views/assets/app/img/notification-icon.png');?>"><br />Notifications</a></li>
                                  <li ><a href="javascript: void(0);" onclick="javascript: dashboard_nav('groups');return false;"><img src="<?php echo base_url('application/views/assets/app/img/groups-icon.png');?>"><br />Groups</a></li>
                                  <li ><a href="javascript: void(0);" onclick="javascript: dashboard_nav('settings');return false;"><img src="<?php echo base_url('application/views/assets/app/img/setting-icon.png');?>"> <br />Settings</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="dashboard-container-main tab-content" id="dashboard-container">


                    </div>
                </div>
            </div>          
        </div>
        <?php echo $this -> load -> view('loader/bottom_loader');?>
    </body> 
</html>