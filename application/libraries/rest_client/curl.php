<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class curl{
    private $connection =   false;
    private $ci         =   false;
    private $base_url   =   false;
    public function __construct() {
        $this->ci = &get_instance();
        $this->ci->load->helper('file');
        $this->ci->config->load('rest_client');
    }
    /*
     * init curl
     */
    public function init($url=0){
        /*
         * get config values 
         */
        $appid      =   $this->ci->config->item('appid');
        $secret     =   $this->ci->config->item('secret');
        $api_url    =   $this->ci->config->item('api_url');
        /*
         * override url
         */
        if($url!=false){
            $api_url    =   $url;
        }
        $this->base_url     =   $api_url;
        $this->connection   =   curl_init();
        /*
         * setting headers
         */
        curl_setopt($this->connection,CURLOPT_HTTPHEADER,array('appid : '.$appid,'secret : '.$secret));
        return $this;
    }
    /*
     *  cookies management
     */
    public function cookie($cookie=array()){
        $this->_connection_define();
        $cookie_path    = $this->_create_cookie_path();
        /*
         * checking cookies to be send
         */
        if(empty($cookie)){
            /*
             * all cookies will be sent
             */
            $files  =   scandir($cookie_path);
            unset($files[0]);
            unset($files[1]);
            if(!empty($files)){
                foreach($files as $file){
                    $temp_path  =   $cookie_path.'/'.$file;
                    $cookie_file    =   getcwd().'/'.$temp_path;
                    curl_setopt($this->connection, CURLOPT_COOKIEFILE,$cookie_file);
                }
            }
        }else{
            if(is_array($cookie)==false){
                echo json_encode(array(
                    'error'     =>  1,
                    'message'   =>  'cookie parameters should be array',
                    'example'   =>  'array(\'cookie1\',\'cookie2\')'
                ));
                exit();
            }else{
               foreach($cookie as $cookie_file){
                   $cookie_file     =   $cookie_path.'/'.$cookie_file.'.cookie';
                   if(file_exists($cookie_file)){
                       $cookie_file    =   getcwd().'/'.$cookie_file;
                       curl_setopt($this->connection, CURLOPT_COOKIEFILE,$cookie_file);
                   }else{
                       echo json_encode(array(
                           'error'      =>  1,
                           'message'    =>  'cookie file not found'
                       ));
                       exit();
                   }
               }
            }
        }
        return $this;
    }
    public function refresh_cookie($cookie_name){
        $this->_connection_define();
        if($cookie_name==false){
            echo json_encode(array(
                'error'     =>  1,
                'message'   =>  'This option is still underconstruction'
            ));
            exit();
        }else{
            $cookie_path    = $this->_create_cookie_path();
            $cookie_file     =   $cookie_path.'/'.$cookie_name.'.cookie';
            if(file_exists($cookie_file)){
                $this->cookie(array($cookie_name))->add_cookie($cookie_name);
                return $this;
            }else{
                echo json_encode(array(
                    'error'      =>  1,
                    'message'    =>  'cookie file not found'
                ));
                exit();
            }
        }
    }
    /*
     * adding cookie
     */
    public function add_cookie($cookie_name=false){
        $this->_connection_define();
        if($cookie_name==false){
            echo json_encode(array(
                'error'     =>  1,
                'message'   =>  'parameter is required'
            ));
            exit();
        }
        $cookie_path    =   $this->_create_cookie_path();
        $cookie_file    =   $cookie_path.'/'.$cookie_name.'.cookie';
        $cookie_file    =   getcwd().'/'.$cookie_file;
        curl_setopt($this->connection, CURLOPT_COOKIEJAR,$cookie_file);
        return $this;
    }
    /*
     * method get
     */
    public function get($method=0,$parameters=0){
        $this->_connection_define();
        if($method==false){
            echo json_encode(array(
                'error'     =>  1,
                'message'   =>  'method is required'
            ));
            exit();
        }else{
            if($parameters!=false){
                if(!is_array($parameters)){
                    echo json_encode(array(
                        'error'     =>  1,
                        'message'   =>  'parameters should be array',
                        'example'   =>  'array(\'param\'=>\'value\')'
                    ));
                    exit();
                }else{
                    if(count($parameters)>0){
                        foreach($parameters as $key => $value){
                            curl_setopt($this->connection,CURLOPT_HTTPHEADER,array($key.' : '.$value));
                        }
                    }
                }
            }
            $url    =   $this->base_url.$method;
            curl_setopt ($this->connection, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($this->connection, CURLOPT_URL, $url); 
            $response = curl_exec($this->connection);
            curl_close($this->connection);
            return $response;
        }
    }
    /*
     * method post
     */
    public function post($method=0,$parameters=0){
        $this->_connection_define();
        if($method==false){
            echo json_encode(array(
                'error'     =>  1,
                'message'   =>  'method is required'
            ));
            exit();
        }else{
            if($parameters!=false){
                if(!is_array($parameters)){
                    echo json_encode(array(
                        'error'     =>  1,
                        'message'   =>  'parameters should be array',
                        'example'   =>  'array(\'param\'=>\'value\')'
                    ));
                    exit();
                }else{
                    curl_setopt($this->connection, CURLOPT_POST, true);
                    curl_setopt($this->connection, CURLOPT_POSTFIELDS, $parameters);
                }
            }
            $url    =   $this->base_url.$method;
            curl_setopt ($this->connection, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($this->connection, CURLOPT_URL, $url); 
            $response = curl_exec($this->connection);
            curl_close($this->connection);
            return $response;
        }
    }
    /***************************************************************************
     * 
     *              PRIVATE METHODS 
     * 
     **************************************************************************/
  /*
   * create cookie path
   */
    private function _create_cookie_path(){
        /*
         * define cookie id
         */
        $rest_client_id     =   $this->ci->session->userdata('rest_client_id');
        if($rest_client_id==false){
            $rest_client_id    =   rand().time();
            $this->ci->session->set_userdata('rest_client_id',$rest_client_id);
        }
        /*
         * define cookie path
         */
        date_default_timezone_set('Asia/Manila');
        $request_time = date("m_d_Y", time());
        $cookie_path    =   APPPATH.'cookies/'.$request_time;
        if(file_exists($cookie_path)==false){
            mkdir($cookie_path);
        }
        $cookie_path    =   $cookie_path.'/'.$rest_client_id;
        if(file_exists($cookie_path)==false){
            mkdir($cookie_path);
        }
        return $cookie_path;
    }
    /*
     * check if connection is defined
     */
    private function _connection_define(){
        if($this->connection==false || $this->ci==false){
            echo json_encode(array(
                'error'     =>      1,
                'message'   =>      'cookie not applicable without init'
            ));
            exit();
        }
    }
}
