<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');  ?>
<?php

/*
 * Library class for all the notifications email
 * 
 */

class Notification_lib{
    var $CI = null;
    var $recipient = null;
    
    function __construct(){
        $this->CI = & get_instance();
        //load phpmailer helper
        $this->CI->load->helper('phpmailer');
    }//endfct
    
    function send_verification_code($message){
       
        $subject = "Activation Code";

        send_email($this->recipient, $subject, $message);
    }

    function send_invite_notification($message){

        $subject = "Group Invitation Notification";

         send_email($this->recipient, $subject, $message);
    }
 //    function contact_us_send($message,$email){
 //    	$subject = "Orthofill.com Contact-Us Customer Information and Messages(".$email.")";
 //    	send_email($this->recipient, $subject, $message,$email);
 //    }
 //    function testimonial_send($message,$email){
 //    	$subject = "Orthofill.com Testimonial Customer Information and Messages(".$email.")";
 //    	send_email($this->recipient, $subject, $message,$email);
 //    }
    
	// function sign_up_send_reseller($message,$email){
	// 	$subject = "Sign up notification";
	// 	send_email($this->recipient, $subject, $message,$email);
	// }
 //    function autoresponder($message){
 //        $email = NULL;
 //        $subject = "Acknowledgement Receipt";

 //        send_email($this->recipient, $subject, $message,$email);
 //    }
}//endclass

