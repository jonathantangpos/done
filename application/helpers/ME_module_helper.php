<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * load module
 */
if (!function_exists('m_load_module')) {

    function m_load_module($module_name = false, $place_holder = false) {
        if ($module_name != false) {
            echo '<script type="text/javascript">';
            echo '$.get("' . base_url($module_name) . '",function(data){
                    $("' . $place_holder . '").html(data);
                })';
            echo '</script>';
        }
    }

}
/*
 * load module assets
 */
if (!function_exists('m_load_module_assets')) {

    function m_load_module_assets($module_name = false, $asset_path = false) {
        if ($module_name != false) {
             return  APPPATH . 'modules/' . $module_name . '/views/assets/' . $asset_path;
            //return '/done/' . APPPATH . 'modules/' . $module_name . '/views/assets/' . $asset_path;
        }
    }

}
/*
  temp workaround with an issue regarding module asset loader.
 */
if (!function_exists('m_load_assets')) {

    function m_load_assets($module_name = false, $asset_path = false) {
        if ($module_name != false) {
            
            $server_name=$_SERVER['SERVER_NAME'];
            $server_port=$_SERVER['SERVER_PORT'];
            
            return "http://$server_name:$server_port/admin/asset/$asset_path";
            
        }
    }

}
if (!function_exists('m_dvlp_dump')) {

    /*
     * DEVELOPMENT DUMP
     * @desc Dipslays string/array/object in a readable way.
     * @desc works ONLY in development environment.
     */

    function m_dvlp_dump() {

        $args = func_get_args();

        if (!count($args) || ENVIRONMENT != 'development')return;

        foreach ($args as $arg) {
            
            echo "<pre>";

            if (!is_array($arg) || is_string($arg))continue;

            print_r($arg);

            echo "</pre>";
        }
    }

}
/*
 * load upload asset
 */
if (!function_exists('m_load_upload_asset')) {

    function m_load_upload_asset($asset_filename) {
        if ($asset_filename != false) {
            return base_url().'assets/uploads/' . $asset_filename;
        }
    }

}