<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * load module assets
 */
if ( ! function_exists('load_global_assets')){
    function load_global_assets($asset_path = false){
        return APPPATH.'views/assets/'.$asset_path;
        // return '/done/'.APPPATH.'views/assets/'.$asset_path;
    }
}

/*
 * get uploaded file path
 */
if ( ! function_exists('get_upload_path')){
    function get_upload_path($file_name = false){
        return APPPATH.'uploads/'.$file_name;        
    }
}
