<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

<?php

function dashboard_left_nav(){
	$CI =& get_instance();
	$log = $CI ->session -> userdata('is_logged');
	$userid = $log['user_id'];
	
	$result = $CI-> mycore_model -> get_groups($userid);
	
	return $result;
	
}

function user_id(){
	$CI =& get_instance();
	$log = $CI ->session -> userdata('is_logged');
	$userid = $log['user_id'];

	return $userid;
}
