<!DOCTYPE html>

<html lang="en">
	<head>
		<title>Done</title>
		<script type="text/javascript" src="<?php echo load_global_assets('jquery/js/jquery.js');?>"></script>
		<script type="text/javascript" src="<?php echo load_global_assets('bootstrap/js/bootstrap.js');?>"></script>
		<link type="text/css" rel="stylesheet" href="<?php echo load_global_assets('bootstrap/css/bootstrap.css');?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo load_global_assets('bootstrap/css/bootstrap-responsive.css');?>" />
		<style>
			.container{
				margin-right:		auto;
				margin-left:		auto;
			}
			.show-grid [class*="span"]{
				background-color: 	#EEE;
				text-align:			center;
				-webkit-border-radius: 3px;
				border-radius:		3px;
				min-height:			40px;
				line-height:		40px;
			}			
			.show-grid{
				margin-top:			10px;
			}
			.show-grid .show-grid [class*="span"]{
				background-color: 	#ccc;			
			}
			.show-grid .show-grid{
				margin-top:			0px;
			}
			.icon-chevron-right{
				float:		right;
				opacity:	.25;
			}
			.logs{
				background-color: 	#DDD;
				text-align:			center;
				-webkit-border-radius: 3px;
				border-radius:		3px;
				min-height:			40px;
				line-height:		40px;
			}
		</style>
	</head>
	
	<body>
		<div class="container">
			<div class="row show-grid">
				<div class="span12"><h1>Hello World!</h1></div>
			</div>
			
			<div class="row">
				<div class="span2">
					<ul class="nav nav-tabs nav-stacked">
						<li class="active"><a href="#"><i class="icon-chevron-right"></i>Team One</a></li>
						<li><a href="#"><i class="icon-chevron-right"></i>Design Team</a></li>
						<li><a href="#"><i class="icon-chevron-right"></i>Research Dept.</a></li>
					</ul>
				</div>			
				<div class="span10">
					<div id="myCarousel" class="carousel slide">
						<div class="carousel-inner">
							<div class="active item">
								<div class="row">
									<div class="span2 logs">        												       
							            <h4>Span 2</h4>
							            <p>Carousel caption text<p>						       
							   		</div>
							   		<div class="span2 logs">        												       
							            <h4>Span 2</h4>
							            <p>Carousel caption text<p>						       
							   		</div>
							   		<div class="span2 logs">        												       
							            <h4>Span 2</h4>
							            <p>Carousel caption text<p>						       
							   		</div>
							   		<div class="span2 logs">        												       
							            <h4>Span 2</h4>
							            <p>Carousel caption text<p>						       
							   		</div>
							   		<div class="span2 logs">        												       
							            <h4>Span 2</h4>
							            <p>Carousel caption text<p>						       
							   		</div>
								</div>							
							</div>
							<div class="item">
								<div class="row">
									<div class="span2 logs">        												       
							            <h4>Span 2</h4>
							            <p>Carousel caption text<p>						       
							   		</div>
							   		<div class="span2 logs">        												       
							            <h4>Span 2</h4>
							            <p>Carousel caption text<p>						       
							   		</div>
							   		<div class="span2 logs">        												       
							            <h4>Span 2</h4>
							            <p>Carousel caption text<p>						       
							   		</div>					
								</div>							
							</div>							   
						</div>					
						<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
    					<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>						
					</div>
				</div>
			</div>
		</div>
	</body>
</html>