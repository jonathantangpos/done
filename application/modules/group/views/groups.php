
                         <!-- groups  -->

                        <div class="row-fluid tab-pane fade in active  groups-container" id="groups">
                            <div class="span5  dashboard-container-left">
                                <div class="add-group-container"><a  href="#add_group" data-toggle="modal"><img src="<?php echo base_url('application/views/assets/app/img/add-group-icon.png');?>"> Add Group</a></div>
                                <ul class="nav nav-pills nav-stacked groups-left-nav">
									
                                    <?php 
                                        $counter = 1;
                                        $active = "";
                                        foreach (dashboard_left_nav() as $value) {
                                            
                                            if($counter==2){
                                                $active = "active";
                                            }else{
                                                $active = "";
                                            }

                                            if($value->group_name=='Draft'){

                                            }else{

                                    ?>
                                        <li class="<?php echo $active;?>"><a href="#mini-tab-<?php echo str_replace(' ', '', $value->group_name);?>" data-toggle="tab"><?php echo $value->group_name;?></a></li>
                                    <?php   
                                            }
                                            $counter++;
                                        }
                                    ?>
                                  
                                </ul>
                            </div>
                            <div class="span19 dashboard-container-right">
                                

                                <!-- add Group Modal -->
                                <div id="add_group" class="modal hide fade add-group-popup" >
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h3 id="myModalLabel">Add Group</h3>
                                    </div>
                                    <?php echo form_open('group/create',"class='form-horizontal'");?>
                                        <div class="modal-body">
                                            <div class="control-group">
                                                <label class="control-label" >Group Name</label>
                                                <div class="controls">
                                                    <input type="text" name="groupname" value="" placeholder="Group Name"/>
                                                    <input type="hidden" name="groupid" value=""/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" >Group Description</label>
                                                <div class="controls">
                                                    <textarea name="group_description" placeholder="Group Description"></textarea>
                                                    
                                                </div>
                                            </div>
                                            <div>
                                                <input type="submit" value="Submit">
                                            </div>
                                        </div>
                                        <!-- <div class="modal-footer">
                                            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                            <input type="submit" value="Create Group" class="btn btn-primary"/>
                                        </div> -->
                                    <?php echo form_close();?>
                                </div>
                                <!-- end add Group Modal -->

                                <div class="tab-content">

                                    <?php 
                                        $counter = 1;
                                        $active = "";
                                        foreach ($groups as $value) {
                                            
                                            if($counter==2){
                                                $active = "active";
                                            }else{
                                                $active = "";
                                            }

                                            if($value->group_name=='Draft'){

                                            }else{

                                    ?>
                                        <div class="tab-pane fade in <?php echo $active;?>" id="mini-tab-<?php echo str_replace(' ', '', $value->group_name);?>">
                                            <?php

                                                if($value -> group_manager_id == $value -> user_id){
                                            ?>
                                                <div class="row-fluid groups-option-container">
                                                      <div class="span4 offset1"><a href="#invite_<?php echo str_replace(' ', '', $value->group_name);?>" data-toggle="modal"><img src="<?php echo base_url('application/views/assets/app/img/add-member-icon.png');?>">  Add Member</a></div>
                                                      <div class="span4"><a href="#update_<?php echo str_replace(' ', '', $value->group_name);?>" data-toggle="modal"><img src="<?php echo base_url('application/views/assets/app/img/update-group-icon.png');?>"> Update Group</a></div>
                                                      <div class="span4"><a href="javascript: void(0);" onclick="javascript: delete_group(<?php echo $value->group_id;?>);return false;" data-toggle="modal"><img src="<?php echo base_url('application/views/assets/app/img/delete-member-icon.png');?>"> Delete Group</a></div>
                                                </div>
                                            <?php
                                                }

                                            ?>
                                            

                                            <!-- update Group Modal -->
                                            <div id="update_<?php echo str_replace(' ', '', $value->group_name);?>" class="modal hide fade" >
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h3 id="myModalLabel">Update Group</h3>
                                              </div>
                                               <?php echo form_open('group/update',"class='form-horizontal'");?>
                                              <div class="modal-body">
                                                <div class="control-group">
                                                    <label class="control-label" >Group Name</label>
                                                    <div class="controls">
                                                        <input type="text" name="groupname" value="<?php echo $value->group_name;?>" placeholder="Group Name"/>
                                                        <input type="hidden" name="groupid" value="<?php echo $value -> group_id;?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label" >Group Description</label>
                                                    <div class="controls">
                                                        <textarea name="group_description" placeholder="Group Description"><?php echo $value -> group_description;?></textarea>
                                                        
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="modal-footer">
                                                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                <input type="submit" value="Update Group" class="btn btn-primary"/>
                                              </div>
                                              </form>
                                            </div>
                                            <!-- update  Group Modal -->

                                             <!-- invite Member Modal -->
                                            <div id="invite_<?php echo str_replace(' ', '', $value->group_name);?>" class="modal hide fade" >
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h3 id="myModalLabel">INVITE PEOPLE</h3>
                                              </div>
                                            <?php echo form_open('groups/member/add','class="form-horizontal"');?>
                                              <div class="modal-body">
                                                <div class="control-group">
                                                   <input class="input-xlarge" name="email" type="text" placeholder="username@yourdomain.com" >
                                                   <input type="hidden" name="group_id" value="<?php echo $value -> group_id;?>">
                                                    <input type="hidden" name="group_name" value="<?php echo $value -> group_name;?>">
                                                   <p>Add email address, separated by comma if more than one.</p>
                                                </div>
                                                
                                              </div>
                                              <div class="modal-footer">
                                                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                <input type="submit" value="Invite" class="btn btn-primary"/>
                                              </div>
                                              </form>
                                            </div>
                                            <!-- end invite Member Modal -->

                        
                                          <div class="row-fluid" id="members_view<?php echo $value -> group_id;?>">

                                          </div>
                                          <script type="text/javascript">
                                                $("tabs2-panel<?php echo $counter;?>").ready(function(){
                                                    get_members(<?php echo $value -> group_id;?>,"<?php echo base_url('application/views/assets/new/uploads');?>/");
                                                    //alert('wew');
                                                });
                                            </script>
                                        </div>
                                    <?php   
                                            }
                                            $counter++;
                                        }
                                    ?>

                                </div>
                            </div>
                        </div>

                        <!-- end groups -->
