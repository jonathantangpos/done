<?php

/**
* 
*/
class Group_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function add_group($groupname,$groupdescription,$userid){

		$userdata = array(
			'group_name' => $groupname,
			'group_manager_id' => $userid,
			'group_description' => $groupdescription,
			'date_created' => date('d-m-Y H:i:s', now())
		);
		
		$this -> db ->insert('groups', $userdata);

		$group = $this -> get_group_individual($userid,$groupname);
		
		$group_id =  $group[0] -> group_id;
		$this -> auto_add_membership($userid,$group_id);
	}

	public function auto_add_membership($user_id,$group_id){
		$data = array(
			'user_id' => $user_id,
			'group_id' => $group_id
		);

		$query = $this->db->insert('members', $data);
	}

	public function get_group_individual($user_id,$groupname){
		$this -> db -> select("*");
		$this -> db -> from('groups');
		$this -> db -> where('group_manager_id',$user_id);
		$this -> db -> where('group_name',$groupname);

		$query = $this -> db -> get();

		return $query -> result();
	}

	public function get_groups($user_id){

		$this -> db -> select("*");
		$this -> db -> from('groups');
		$this -> db -> join('members',' groups.group_id = members.group_id','left');
		//$this -> db -> where('members.user_id',$userid);
		$this -> db -> where('members.user_id',$user_id);
		//$this -> db -> where('members.user_id',$user_id);
		$query = $this -> db -> get();

		return $query -> result();
	}

	public function update_group($userid, $group_id,$group_description, $group_name){

		$data = array(
			'group_name' => $group_name,
			'group_description' => $group_description,
			'date_created' => date('d-m-Y H:i:s', now())
		);

		$this -> db -> where('group_id',$group_id);
		$this -> db -> where('group_manager_id',$userid);

		$query = $this -> db -> update('groups',$data);

		return $query;
	}
	
	public function delete_group($group_id, $user_id){
	
		$this -> db -> where('group_id', $group_id);
		$this -> db -> where('group_manager_id', $user_id);
		
		$query = $this -> db -> delete('groups'); 
		
		return $query;
		
	}

	public function check_email($email){

		$this -> db -> select("*");
		$this -> db -> where('email',$email);

		$query = $this -> db -> get('users');

		if($query -> num_rows() == 1){
			return $query -> result();
		}else if($query -> num_rows() > 1){
			return FALSE;
		}else{
			return FALSE;
		}

	}

	public function check_membership($group_id,$user_id){

		$this -> db -> select("*");
		$this -> db -> where('group_id',$group_id);
		$this -> db -> where('user_id',$user_id);

		$query = $this -> db -> get('members');
		if($query->num_rows() >= 1){
			return FALSE;
		}else{
			return TRUE;
		}

		// echo "<pre>";
		// print_r($query->num_rows());
		// echo "</pre>";
		// // if($query -> num_rows() == 1){
		// 	return $query -> result();
		// }else{
		// 	return FALSE;
		// }
	}
	public function add_group_member($username,$member_id,$email,$groupname,$group_id){

		$data = array(
			'email' => $email,
			'user_id' => $member_id,
			'details' => $username.' want to add you in group '.$groupname,
			'group_id' => $group_id,
			'date' => date('d-m-Y H:i:s', now()),
			'is_done' => 0
		);

		$query = $this->db->insert('notifications', $data);
		
		return $query;
	}




	public function accept_membership($user_id, $group_id,$notification_id){

		$data = array(
			'user_id' => $user_id,
			'group_id' => $group_id
		);

		$query = $this->db->insert('members', $data);
		if($query){
			$this -> confirm_log($notification_id);
			return TRUE;

		}else{
			return FALSE;

		}
		
		return $query;

	}

	public function confirm_log($notification_id){

		$data = array(
			'is_done' => 1
		);

		$this -> db -> where('notification_id',$notification_id);

		$query = $this -> db -> update('notifications',$data);

		return $query;
	}

	public function get_members($group_id){

		$this -> db -> select();
		$this -> db -> from('members');
		$this -> db -> join('users','users.user_id = members.user_id','left');
		$this -> db -> where('members.group_id',$group_id);
		
		$query = $this->db->get();
		
		return $query -> result();

	}
}