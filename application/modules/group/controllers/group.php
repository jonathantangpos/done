<?php

class Group extends MX_Controller{

	var $data;

	public function __construct(){
	
		parent::__construct();

		$this -> data = array();
		$this -> load -> model('group_model');
		//echo md5("hara1234");
	}
	


	public function create_group(){

		if($this -> session -> userdata('is_logged')){

			

				$groupname = $this -> input -> post('groupname');
				$groupdescription = $this -> input -> post('group_description');
				$log = $this -> session -> userdata('is_logged');
				$userid = $log['user_id'];

				$this -> group_model -> add_group($groupname,$groupdescription,$userid);
				//echo "else";
		
			redirect('dashboard?p=groups');

		}else{
			redirect('login','refresh');
		}
		
	}

	public function view_group(){

		if($this -> session -> userdata('is_logged')){


			$log = $this -> session -> userdata('is_logged');
			$userid = $log['user_id'];

			$result = $this -> group_model -> get_groups($userid);
			// $notifications = $this -> group_model -> get_notifications($userid);
			// echo "<pre>";
			// print_r($result);
			// echo "</pre>";
			//$this -> data['notifications'] = $notifications;
			$this -> data['groups'] = $result;

			// $this -> load -> view("group_view",$this -> data);
			$this -> load -> view("groups",$this -> data);


		}else{
			redirect('login','refresh');
		}
	}

	public function test(){
		$this -> load -> view('groups');
	}

	public function update_group(){

		if($this -> session -> userdata('is_logged')){

			$log = $this -> session -> userdata('is_logged');
			$userid = $log['user_id'];

			$group_id = $this -> input -> post('groupid');
			$group_name = $this -> input -> post('groupname');
			$group_description = $this -> input -> post('group_description');

			$result = $this -> group_model -> update_group($userid, $group_id,$group_description,$group_name);

			//used to stay on same tab
			$this->session->set_userdata('sess_tag', 'groups');

			redirect('dashboard','refresh');

		}else{
			redirect('login','refresh');
		}
	}
	
	public function delete_group(){

		if($this -> session -> userdata('is_logged')){

			$log = $this -> session -> userdata('is_logged');
			$userid = $log['user_id'];
			
			$group_id = $this -> input -> post('groupid');
			
			$success = $this -> group_model -> delete_group($group_id, $userid);
			
			return "done";

		}else{
			redirect('login','refresh');
		}
	}
	

	public function add_group_member(){

		$email = $this -> input -> post("email");
		$group_id = $this -> input -> post("group_id");
		$groupname = $this -> input -> post("group_name");
		
		//$email = explode($email,',');

		$data = $this -> group_model -> check_email($email);

		if($data){
			
			$member_id = $data[0] -> user_id;
			$log = $this -> session -> userdata('is_logged');
			$username = $log['username'];
			$exist = $this -> group_model -> check_membership($group_id,$member_id);
			if($exist){
				if($member_id == $log['user_id']){
					echo "fail";
				}else{
					$this -> group_model -> add_group_member($username,$member_id,$email,$groupname,$group_id);
					$message = $username.' want to add you in group '.$groupname;
					$message.= "<br/> Login to your account to proceed.";
					$message.= "<br/> <a href='".base_url()."'>Click here!</a>";

					$this -> load -> library('notification_lib');
					$this -> notification_lib -> recipient = $email;
					$this -> notification_lib -> send_invite_notification($message);
				}
			}else{
				//used to stay on same tab
				$this->session->set_userdata('sess_tag', 'groups');

				redirect('dashboard?p=groups','refresh');

			}
			
			//used to stay on same tab
			$this->session->set_userdata('sess_tag', 'groups');

			redirect('dashboard?p=groups','refresh');
		}
		else{
			echo "false";
		}
	}

	public function confirm_membership(){
		
		$user_id = $this -> input -> get('user_id');
		$group_id = $this -> input -> get('group_id');
		$notification_id = $this -> input -> get('notification_id');

		$accepted = $this -> group_model -> accept_membership($user_id, $group_id,$notification_id);
		if($accepted){
			//used to stay on same tab
			$this->session->set_userdata('sess_tag', 'groups');

			redirect('dashboard?p=groups','refresh');

		}else{
			//used to stay on same tab
			$this->session->set_userdata('sess_tag', 'groups');

			redirect('dashboard?p=groups','refresh');

		}
	}

	public function get_members(){

		$group_id = $this -> input -> post('group_id');
		// $group_id = 1;
		$members = $this -> group_model -> get_members($group_id);

		//$members[]= base_url('application/views/assets/new/uploads/');
		// echo "<pre>";
		// print_r($members);
		// echo "</pre>";

		echo json_encode($members);
	}

	public function testing(){
		/*echo "ewsssew";
		$this -> group_model -> check_membership(1,2);*/
		$email = 'jonathantangpos@yahoo.com';
		$message = 'rtedsaf want to add you in group ';

		$this -> load -> library('notification_lib');
		$this -> notification_lib -> recipient = $email;
		$this -> notification_lib -> send_invite_notification($message);
	}

}

