<?php
	class Delete extends MX_Controller{
		
		function __construct(){
			parent::__construct();
			
			$this->load->model('log_m');
			
			$this->load->library('session');			
		}
		
		public function delete_log(){
			$log_id = $this->input->post('log_id');
			
			$user_data = $this->session->userdata('is_logged');
			$user_id = $user_data['user_id'];

			$data['msg'] = $this->log_m->delete_log($log_id, $user_id);

			echo json_encode($data);
		}
				
	}
?>