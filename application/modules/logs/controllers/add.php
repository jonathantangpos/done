<?php
	class Add extends MX_Controller{
		
		function __construct(){
			parent::__construct();
			
			$this->load->model('log_m');
			
			$this->load->library('session');			
		}
		function attach(){									
			
			// list of valid extensions, ex. array("jpeg", "xml", "bmp")
			$allowedExtensions = array('jpeg', 'jpg', 'png', 'bmp', 'doc', 'docx', 'xls', 'xlsx');
			
			// max file size in bytes
			$sizeLimit = 2097152; //2MB
			
			require('uploader.php');
			$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
			
			// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
			$result = $uploader->handleUpload('application/uploads/');
			
			// to pass data through iframe you will need to encode all html tags
			echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
				
		}
		
		function share(){
			$user_data = $this->session->userdata('is_logged');
			//used to save attachment			

			$data['file_name'] = $this->input->post('file_name');
			$data['file_size'] = $this->input->post('file_size');	
							
			//used to save logs_groups_ref
			$data['group_id'] = $this->input->post('group_id');
			
			//used to save log
			$data['log_title'] = $this->input->post('log_title');						
			$data['log_content'] = $this->input->post('log_content');						
			$data['user_id'] = $user_data['user_id'];
			
			//save log to db
			$data['last_id'] = $this->log_m->save_log($data);
			
			//save log group reference
			$this->log_m->save_log_group_ref($data);
			
			//save attachment
			if(!empty($data['file_name'])){
				$this->log_m->save_attachment($data);				
			}
			$this->session->set_userdata('sess_tag', 'logs');

			redirect('dashboard', 'refresh');
		}				
				
	}