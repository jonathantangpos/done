<?php
class View extends MX_Controller{
	
	function __construct(){
		parent::__construct();
		
		$this->load->model('group_m');
		$this->load->model('log_m');
		$this->load->model('global_m');
		$this->load->helper('date');
	}
	function test(){
		if($this->session->userdata('is_logged')){
			
			$user_data = $this->session->userdata('is_logged');
			
			$data['groups'] = $this->group_m->fetch_groups($user_data['user_id']);
			$data['avatar'] = $this->session->userdata('avatar');
			
			//$data['logs_v'] = $this->render_view('logs_v', $data); 		
			echo $this->render_view('view_logs_v', $data);
			
			}else{			
			redirect('login','refresh');			
		}		
	}
	function index(){
		
		if($this->session->userdata('is_logged')){
			
			$user_data = $this->session->userdata('is_logged');
			
			$data['groups'] = $this->group_m->fetch_groups($user_data['user_id']);
			$data['avatar'] = $this->session->userdata('avatar');
			
			//$data['logs_v'] = $this->render_view('logs_v', $data); 		
			echo $this->render_view('logs', $data);
			
		}else{			
			redirect('login','refresh');			
		}		
	}
	

	/* Returns partial view of logs_v*/
	function render_view($view, $data){	
	
		return $this->load->view($view, $data, true);
		
	}
		
	
	/*
	 * @name : display_logs()
	 * @param : $group_id, $more_logs
	 * @return : json
	 * */		
	function display_logs_day(){
				
		$date = $this->input->post('date');
		$group_id = $this->input->post('group_id'); //get group_id from post					
		
	
		$data['logs'] = $this->log_m->get_logs_day($group_id, $date);			
	
		
		
		$data['logs_v'] = $this->render_view('logs_v_day', $data);
	
		echo json_encode($data);
	}
	
	function display_logs_week(){
		$start_end = $this->input->post('start_end');		

		$group_id = $this->input->post('group_id'); //get group_id from post		
		
		$s_origin = $this->session->userdata('s_origin');
		$s_offset = $this->session->userdata('s_offset');		
		$s_group_id = $this->session->userdata('s_group_id');									

		$data['logs'] = $this->log_m->get_logs_week($group_id, $start_end[0], $start_end[1]);
				

		$data['logs_v'] = $this->render_view('logs_v_week', $data);						
		
		echo json_encode($data);
	}

	function display_logs_month(){

		$start_end = $this->input->post('start_end');

		$temp_date_s = date_create($start_end[0]);	
		$temp_date_e = date_create($start_end[1]);

		$date_s = date_format($temp_date_s, "Y-m-d");
		$date_e = date_format($temp_date_e, "Y-m-d");	

		$data['month'] = date_format($temp_date_s, "n"); //passed to view to create calendar
		$data['year'] = date_format($temp_date_s, "Y"); //passed to view to create calendar

		$group_id = $this->input->post('group_id'); //get group_id from post
			

		$data['logs'] = $this->log_m->get_logs_month($group_id, $date_s, $date_e);
				

		$data['logs_v'] = $this->render_view('logs_v_month', $data);						
		
		echo json_encode($data);
	}
	
	function include_attachments(){
		/*get parameters send by modules::run*/
		$data['log_id'] = func_get_args();
		
		$data['attachments'] = $this->log_m->fetch_attachments($data['log_id'][0]);
				
		$this->load->view('attachments_v', $data);
		
	}
	
	function include_comments(){
		/*get parameters send by modules::run*/
		$data['log_id'] = func_get_args();		
		$data['comments'] = $this->log_m->fetch_comments($data['log_id'][0]);			
		$this->load->view('comments_v', $data);
	
	}
	
	function save_comment(){
		
		$log = $this->session->userdata('is_logged');
		
		$data['log_id'] = $this->input->post('log_id');
		$data['comment'] = $this->input->post('comment');
		$data['user_id'] = $log['user_id'];
				
		$this->log_m->save_comments($data);
		
		//$data['comments'] = $this->log_m->fetch_comments($data['log_id']);
		echo json_encode($data);
	}	
	
}