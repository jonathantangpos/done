<?php
	class Log_m extends CI_Model{
		
		function __construct(){
			parent::__construct();
		}	
		
		function get_logs($group_id, $offset, $origin){
			$dates = array();
			$new_array = array();
			$this->db->distinct();
			$this->db->select("DATE(log_date) AS date_cmpr, DATE_FORMAT(log_date, '%a , %b %d , %Y') AS log_date, count(*) AS count", FALSE);
			$this->db->from('logs AS l');
			$this->db->join('log_group_ref AS r', 'l.log_id = r.log_id');
			$this->db->where('r.group_id', $group_id);
			$this->db->group_by('DATE(log_date)');
			$this->db->order_by('DATE(log_date)', 'DESC');
			$this->db->limit($offset, $origin);
				
			$dates = $this->db->get()->result_array();


			$days = array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
			
			
			
			$date_counter = 0;
				
			foreach ($dates as $row){
		
				$new_array[] = array(
						'log_date' => $row['log_date'],
						'logs' => array()
				);
		
				for($i=0; $i<$row['count']; $i++){
						
					$this->db->select("l.log_id, l.log_content, DATE_FORMAT(l.log_date, '%l:%i %p') AS log_time, u.username AS log_author", FALSE);
					$this->db->from('logs AS l');
					$this->db->join('users AS u', 'l.log_author = u.user_id'); //join to get username of log_author instead of id
					$this->db->join('log_group_ref AS r', 'l.log_id = r.log_id');
					$this->db->where('r.group_id', $group_id);
					$this->db->where('DATE(l.log_date)', $row['date_cmpr']);
					$new_array[$date_counter]['logs'] = $this->db->get()->result_array(); //push new array to $new_array's inner array: logs[]
				}
		
				$date_counter++;
			}
		
			return $new_array;
		}
		
		
		
		
		
		
		function get_logs_day($group_id, $date){
			$new_array = array();						
			$this->db->select("l.log_id, l.log_title, l.log_content, DATE_FORMAT(l.log_date, '%h:%i %p') AS log_time, u.username", FALSE);
			$this->db->from('logs AS l');
			$this->db->join('log_group_ref AS r', 'l.log_id = r.log_id');
			$this->db->join('users AS u', 'l.log_author = u.user_id');
			$this->db->where('r.group_id', $group_id);
			$this->db->where('DATE(l.log_date)', $date);
			$this->db->order_by('log_time', 'DESC');			
		
			$old_array =  $this->db->get()->result_array();
			
			
			
			$main_hour = array(
					'12:00 AM','01:00 AM','02:00 AM','03:00 AM','04:00 AM','05:00 AM','06:00 AM',
					'07:00 AM','08:00 AM','09:00 AM','10:00 AM','11:00 AM','12:00 PM','01:00 PM',
					'02:00 PM','03:00 PM','04:00 PM','05:00 PM','06:00 PM','07:00 PM','08:00 PM',
					'09:00 PM','10:00 PM','11:00 PM'
			);
			for($h=0; $h<count($main_hour); $h++){
				$h_now = $main_hour[$h];
				
				$new_array[] = array(
						'log_hour' => $h_now,
						'logs' => array()						
						);
				$logs = array();
				foreach ($old_array as $row){
					
					if(substr($row['log_time'], 0, 2) == substr($h_now, 0, 2) && substr($row['log_time'], -2) == substr($h_now, -2)){
						$logs[] = array(
								'log_id' => $row['log_id'],
								'log_title' => $row['log_title'],
								'log_content' => $row['log_content'],
								'log_time' => $row['log_time'],
								'log_author' => $row['username']
						);
						
					}
					$new_array[$h]['logs'] = $logs;
				}
			}
			
			
			return $new_array;
		}


		function get_logs_week($group_id, $week_start, $week_end){
			//$week_start = "2013-02-03";
			//$week_end = "2013-02-10";
						
			$new_array = array();
						
			$this->db->select("DATE_FORMAT(log_date, '%W') AS wd_cmpr, l.log_id, l.log_title, log_content, DATE_FORMAT(l.log_date, '%h:%i %p') AS log_time, u.username", FALSE);
			$this->db->from('logs AS l');
			$this->db->join('users AS u', 'l.log_author = u.user_id'); //join to get username of log_author instead of id
			$this->db->join('log_group_ref AS r', 'l.log_id = r.log_id');
			$this->db->where('r.group_id', $group_id);
			$this->db->where('log_date >=', $week_start);
			$this->db->where('log_date <', $week_end);						
		
			$old_array = $this->db->get()->result_array();
		
			$week_days = array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
												
			$date_counter = 0;
			
			for($d=0; $d<count($week_days);$d++){
				$wd_now = $week_days[$d];
				
				$new_array[] = array(
						'log_wd' => $wd_now,
						'logs' => array()
					);
				$logs = array();
				foreach ($old_array as $row){
					if($row['wd_cmpr'] == $wd_now){
						$logs[] = array(
								'log_id' => $row['log_id'],
								'log_title' => $row['log_title'],
								'log_content' => $row['log_content'],
								'log_time' => $row['log_time'],
								'log_author' => $row['username']
								);
					}
					$new_array[$d]['logs'] = $logs;
				}
			}
			
		
			return $new_array;
		}

		function get_logs_month($group_id, $mont_start, $month_end){
									
						
			$this->db->select("l.log_id, l.log_title, l.log_content, u.username, l.log_date, DATE_FORMAT(l.log_date, '%h:%i %p') AS log_time", FALSE);
			$this->db->from('logs AS l');
			$this->db->join('users AS u', 'l.log_author = u.user_id'); //join to get username of log_author instead of id
			$this->db->join('log_group_ref AS r', 'l.log_id = r.log_id');
			$this->db->where('r.group_id', $group_id);
			$this->db->where('log_date >=', $mont_start);
			$this->db->where('log_date <', $month_end);						
		
			return $this->db->get()->result_array();					
								
		}
		
		
		function save_log($data){
			$myArray = array(
					'log_id' => '',
					'log_title' => $data['log_title'],
					'log_content' => $data['log_content'],					
					'log_date' => date('Y-m-d H:i:s'),					
					'log_author' => $data['user_id']
					);
			
			$this->db->insert('logs', $myArray);
			
			return $this->db->insert_id();
			
		}

		function delete_log($log_id, $user_id){

			$this->db->where('log_id', $log_id);
			$this->db->where('log_author', $user_id);
			$this->db->delete('logs');
		
			$query = $this->db->get_where('logs', array('log_id' => $log_id));

			if ($query->num_rows() > 0){
				return $msg = array(
				"code" => 1,
				"msg" => "Deletion Failed!"
				);

			}else{
				return $msg = array(
				"code" => 0,
				"msg" => "Deletion successful!"
				);
			}
			
		
		}
		
		function save_attachment($data){			
			$myArray = array();
			$size_count = 0;			
			foreach($data['file_name'] as $key){
				$myArray[] = array(
							'attachment_id' => '',
							'log_id' => $data['last_id'],
							'file_size' => $data['file_size'][$size_count],
							'file_name' => $key
							);
							
							$size_count++;
			}
			
			$this->db->insert_batch('attachments', $myArray);
			
		}
		
		function save_log_group_ref($data){
			$myArray = array(
					'log_id' => $data['last_id'],
					'group_id' => $data['group_id']
					);
			$this->db->insert('log_group_ref', $myArray);
		}		
		
		//Old implementation
		function fetch_logs($group_id, $offset, $origin){
			$select =   array(
							"DATE_FORMAT(l.log_date, '%W , %M %d , %Y') AS log_date",
							"GROUP_CONCAT(l.log_id SEPARATOR 0x1D) AS log_ids",
							"GROUP_CONCAT(l.log_content SEPARATOR 0x1D) AS log_contents",
							"GROUP_CONCAT(u.username SEPARATOR 0x1D ) AS log_authors",
							"GROUP_CONCAT(DATE_FORMAT(l.log_date,'%l:%i %p') SEPARATOR 0x1D) AS log_times"
						);
			$this->db->select($select);
			$this->db->from('logs AS l');
			$this->db->join('log_group_ref AS r', 'l.log_id = r.log_id');
			$this->db->join('users AS u', 'l.log_author = u.user_id');
			$this->db->where('r.group_id', $group_id);
			$this->db->group_by('DATE(l.log_date)');	// To group by Date only, excluding time from l.log_date		
			$this->db->order_by('DATE(l.log_date)', 'DESC'); // Show earliest logs from DB
			$this->db->limit($offset, $origin);
			return $this->db->get()->result_array();
		}
		
		function fetch_attachments($log_id){
			$this->db->select('file_name');
			$this->db->from('attachments');
			$this->db->where('log_id', $log_id);
			return $this->db->get()->result_array();			
		}
		
		function save_comments($data){
			$myArray = array(
					'comment_id' => '',
					'log_id' => $data['log_id'],
					'comment_author' => $data['user_id'],
					'comment_content' => $data['comment'],
					'comment_date' => date('Y-m-d H:i:s')
					);
			$this->db->insert('comments', $myArray);
		}
		
		function fetch_comments($log_id){
			$this->db->select('*, u.username');
			$this->db->from('comments AS c');
			$this->db->join('users AS u', 'c.comment_author = u.user_id');
			$this->db->where('c.log_id', $log_id);
			return $this->db->get()->result_array();
		}
				
	}
	
?>