
                        <!-- logs -->
                        <div class="row-fluid tab-pane fade in active log-container" id="logs1">
							<div class="span5  dashboard-container-left">
								<div class="search-container"><input type="text" placeholder="Search"></div>
								<ul class="nav nav-pills nav-stacked groups-left-nav" id="tabs">
									<?php							
										foreach($groups as $key){								
											echo "<li><a onclick='select_group(".$key['group_id'].")' data-group='".$key['group_id']."' data-toggle='tab'>".$key['group_name']."</a></li>";
										}
										
									?>
								</ul>
							</div>
							
							<div class="span19 dashboard-container-right">
								
								<div class="row-fluid form-container">
									<div class="span4 date pull-left">
										<?php
											$date = date('Y-m-d');
											$date = explode("-", $date)
										?>
										<h2><?php echo $date[2]?></h2>
										<ul>
											<li><?php echo $date[1]?></li>
											<li><?php echo $date[0]?></li>
										</ul>
									</div>
									<div class="span20 log-area">																				
											<?php echo form_open('logs/add/share', 'id="logs_form" class="span22"'); ?>
											<input type="text" name="log_title" class="input-small log_title" placeholder="Log Title" />
											<?php
												$ta_attr = array(
												'name' => 'log_content',												
												'id' => 'log_content',
												'cols' => 0,
												'rows' => 0,
												'placeholder' => 'Details'
												);
												echo form_textarea($ta_attr);			
											?>	
											<input type="hidden" name="group_id" id="group_id" value="1"> <!-- Acquires val from click of grouptab -->		
											<input type="submit" value="Share" class="btn" id="share_btn">
											<span id="uploader" class="btn" title="Attach files">
												<i class="icon-paper-clip"></i>												
											</span>
											<span id="" class="btn" title="Insert photos">
												<i class="icon-camera"></i>												
											</span>
											<span id="upload_msg"></span>
											<span id="upload_size" style="display: none;"></span>
										<?php echo form_close();?>
									</div>
									
								</div>
																							
								<hr style="border-top: 1px solid #000; border-bottom: solid 1px #fff;"/>
								<div class="row-fluid">
									<div class="span24">
										<div class="btn-group filters" data-toggle="buttons-radio">									
											<a type="button" class="btn btn-primary view_toggle active" id="by-day" data-id="">Day</a>
											<a type="button" class="btn btn-primary view_toggle" id="by-week" data-id="">Week</a>
											<a type="button" class="btn btn-primary view_toggle" id="by-month" data-id="">Month</a>
										</div>
										<div class="log_nav">
											<a href="#" type="button" class="btn btn-primary" id="log_nav_left"><i class="icon-white icon-chevron-left"></i></a>
											<span class="date_display"></span>
											<a href="#" type="button" class="btn btn-primary" id="log_nav_right"><i class="icon-white icon-chevron-right"></i></a>
										</div>										
									</div>
								</div>								
								
								<hr style="border-top: 1px solid #000; border-bottom: solid 1px #fff;"/>
									
								<div class="tab-content" id="logs_content">
									<!--logs loaded via ajax call-->
								</div>								
							</div>
						</div>
                        <!-- end logs -->

                        <!-- log modal start -->
						<div id="log_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-header">
						    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						    <h3 id="log_modal_title">Modal header</h3>
						  </div>
						  <div class="modal-body" id="log_modal_body">
						    <p>Hahahaha</p>
						  </div>
						  <div class="modal-footer">
						  	<button class="btn icons"><i class="icon-download-alt"></i></button>
						  	<button class="btn icons"><i class="icon-facebook"></i></button>
						  	<button class="btn icons"><i class="icon-twitter"></i></button>
						    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Close</button>
						  </div>
						</div>
						<!-- log modal end -->

                        
		<script type="text/javascript">									

			var week_start_day = 0;
			var start_end = new Array();

			var now = moment();
			var current = moment();

			var display_date = format_date(now, "display_d");

			var db_date = format_date(current, "db");

			var date_nav_caption = display_date;
			
			/*Load first tab contents on page load*/
			$(document).ready(function(){		

				first_load();										
				update_date_display(display_date);
				upload(); //init file uploader		
				load_js(); //load log.js	
							
				
			});
			
			/*
			* Click functions on every filter - day, week, month
			*/
			$(".view_toggle").click(function(){
				var active = $(this).text().toLowerCase();
				var group_id = $(this).attr("data-id");

				if(active=="day"){
					//day filter clicked

					date_nav_caption = format_date(current, "display_d");									
					
				}else if(active=="week"){
					//week filter clicked

					var cur_week = week_set("steady", week_start_day);					
					//week_start_day = cur_week[2];
					
					var week_start_db = format_date(cur_week[0], "db");					
					var week_end_db = format_date(cur_week[1], "db_end");	

					date_nav_caption = format_date(cur_week, "display_w");
					
					start_end[0] = week_start_db;
					start_end[1] = week_end_db;

				}else{
					//month filter clicked

					var cur_week = week_set("steady", week_start_day);
				
					current = cur_week[0];					

					//week_start_day = cur_week[2];

					var month = moment(current).date(1);
					
					start_end[0] = month.format("YYYY-MM-DD");
					start_end[1] = 	month.add("M", 1).format("YYYY-MM-DD");
					
					date_nav_caption = format_date(current, "display_m");

				}
				
				update_date_display(); //update date navigation caption	


				var strFun = "view_" + active;
				var fn = window[strFun];
				fn(group_id, start_end);
				
			});
		
			$("#log_nav_right").click(function(){
				move_next();
				});

			$("#log_nav_left").click(function(){
				move_prev();
				});

									
			function move_next(){
				var active = active_filter();								
				
				var next_day = now.add("d", 1);
				
				var group_id = $(".view_toggle").attr("data-id");				

				if(active=="day"){
					date_nav_caption = format_date(next_day, "display_d"); //update display date
					db_date = format_date(next_day, "db"); //update the db_date once next is clicked which is used for db query
				}else if(active=="week"){
					var cur_week = week_set("right", week_start_day);
					
					week_start_day = cur_week[2];										
					
					week_start_db = format_date(cur_week[0], "db");					
					week_end_db = format_date(cur_week[1], "db_end");

					date_nav_caption = format_date(cur_week, "display_w");

					start_end[0] = week_start_db;
					start_end[1] = week_end_db;
					
				}else{

					var cur_date = month_set("right", current);
				
					current = cur_date;					

					db_date = format_date(current, "db");

					var month = moment(current).date(1);
					
					start_end[0] = month.format("YYYY-MM-DD");
					start_end[1] = 	month.add("M", 1).format("YYYY-MM-DD");
					
					date_nav_caption = format_date(current, "display_m");					
				}
				
				update_date_display();
							
				var strFun = "view_" + active;				
				var fn = window[strFun];
				fn(group_id, start_end);
				
				}			
			
			function move_prev(){

				var active = active_filter(); //determine active filter - day/week/month
				
				var prev_day = now.subtract("d", 1);
				
				var group_id = $(".view_toggle").attr("data-id");				

				if(active=="day"){

					date_nav_caption = format_date(prev_day, "display_d");
					db_date = format_date(prev_day, "db");

				}else if(active=="week"){

					var cur_week = week_set("left", week_start_day);					
					
					week_start_day = cur_week[2]; //update week_start_day

					
					week_start_db = format_date(cur_week[0], "db");
					week_end_db = format_date(cur_week[1], "db_end");

					
					date_nav_caption = format_date(cur_week, "display_w");
				
					start_end[0] = week_start_db;
					start_end[1] = week_end_db;

					//update date_nav_caption here
					//update logs when clicked to move previous week

				}else{


					var cur_date = month_set("left", current);
				
					current = cur_date;		
					
					db_date = format_date(current, "db");			

					var month = moment(current).date(1);
					
					start_end[0] = month.format("YYYY-MM-DD");
					start_end[1] = 	month.add("M", 1).format("YYYY-MM-DD");
					
					date_nav_caption = format_date(current, "display_m");

					// var cur_date = month_set("left", current);

					// current = cur_date;

					// var cur_month = format_date(current, "month");
					// var cur_year = format_date(current, "year");
					
					// date_nav_caption = format_date(current, "display_m");

				}
				
				
				update_date_display();

				var strFun = "view_" + active;				
				var fn = window[strFun];
				fn(group_id, start_end);
				
				}


			/*
			* @name : week_set
			* @params : direction (values : "left", "right", type: string), w_s_d (value: week_start_day, int[date])
			* @return : week_array (values: start_o_week, end_o_week w_s_d type: int[date])
			*/
			function week_set(direction, w_s_d){
				var week_array = new Array();				
				var week_end_day = 0;
				
				if(direction=="left"){
					w_s_d -= 7;
				}else if(direction=="right"){
					w_s_d += 7;					
				}else{
					w_s_d = w_s_d;
				}

				week_end_day = w_s_d + 6;
			
				var start_o_week = moment(current).day(w_s_d);
				var end_o_week =  moment(current).day(week_end_day);				

				week_array[0] = start_o_week;
				week_array[1] = end_o_week;
				week_array[2] = w_s_d;

				return week_array;
			}

			function month_set(direction, month){
				if(direction == "left"){
					month = month.subtract("M", 1);
				}else{
					month = month.add("M", 1);
				}

				return month;
			}

			
			function update_date_display(){				
				$(".date_display").empty();
				$(".date_display").append(date_nav_caption);
				}

			
			/*
			* @name:	format_date
			* @params:	date - date, display - boolean, true=display_date, false=db_date
			* @return: 	date or date-range
			*/
			function format_date(date, type){
				var d = "";
				switch(type){
					case "display_d" :
						d = date.format("ddd, MMMM D YYYY"); break;
					case "display_w" :

						var week_start_d = format_date(date[0], "day");
						var week_end_d = format_date(date[1], "day");
						
						var week_start_m = format_date(date[0], "month");
						var week_end_m = format_date(date[1], "month");						
						
						var week_start_y = format_date(date[0], "year");
						var week_end_y = format_date(date[1], "year");

						if(week_start_m != week_end_m){
							d = week_start_m + " " + week_start_d + " - " + week_end_m + " " + week_end_d + " " + week_end_y;
						}else{
							d = week_start_m + " " + week_start_d + " - " + week_end_d + ", " + week_end_y;
						}
						
						break;
					case "display_m" : 
						d = date.format("MMMM YYYY"); break;
					case "db" : 
						d = date.format("YYYY-MM-DD"); break;
					case "day" : 
						d = date.format("DD"); break;
					case "month" : 
						d = date.format("MMM"); break;
					case "year" : 
						d = date.format("YYYY"); break;
					case "db_end" : 
						d = date.add("d", 1).format("YYYY-MM-DD"); break;

				}
				return d;
					
			}
			
			function select_group(group_id){
				$("#group_id").val(group_id); // //adds value to hidden field in add_log form to determine which group to share
				$("#by-day, #by-week, #by-month").attr("data-id", group_id);
				
				var strFun = "view_" + active_filter();
				
				var fn = window[strFun];
				fn(group_id, start_end);
			}

			/*
			* @name:	active_filter
			* @desc:	returns current filter as string - day, week, month
			*/
			
			function active_filter(){
				var active = "";
				$(".view_toggle").each(function(){
					if($(this).hasClass("active")){
						active = $(this).text().toLowerCase();	
						}
					});
				return active;
				}

			function view_day(group_id, param){
				//alert(db_date);									
				$.ajax({
					type : "POST",
					dataType: "json",
					url : "<?=base_url()?>logs/view/display_logs_day",					
					//data : "&group_id=" + group_id + "&more_logs=false",
					data : {"group_id" : group_id, "date" : db_date},
					success : function (data){	
						$("div#logs").remove();						
						$("#logs_content").append(data.logs_v);//logs_v							
					},
					error : function(data){
						console.log(data);
					}
				});
				
			}
			
			function view_week(group_id, start_end){								
				$.ajax({
					type : "POST",
					dataType: "json",
					url : "<?=base_url()?>logs/view/display_logs_week",					
					//data : "&group_id=" + group_id + "&more_logs=false",
					data : {"group_id" : group_id, "start_end" : start_end},
					success : function (data){	
						$("div#logs").remove();						
						$("#logs_content").append(data.logs_v);//logs_v				
					},
					error : function(data){
						console.log(data);
					}
				});				
			}
			

			function view_month(group_id, start_end){							
				$.ajax({
					type : "POST",
					dataType: "json",
					url : "<?=base_url()?>logs/view/display_logs_month",										
					data : {"group_id" : group_id, "start_end" : start_end},
					success : function (data){	
						$("div#logs").remove();						
						$("#logs_content").append(data.logs_v);//logs_v		
						//calendar();		
					},
					error : function(data){
						console.log(data);
					}
				});				
				}
			
			
			function first_load(){
				/* Make first item in vertical tab active*/
				$("#tabs li:first").attr("class", "active");

				$("#group_id").val(<?=$groups[0]['group_id']?>); // //adds value to hidden field in add_log form to determine which group to share
				$("#by-day, #by-week, #by-month").attr("data-id", <?=$groups[0]['group_id']?>);
				
				$.ajax({
					type : "POST",
					dataType: "json",
					url : "<?=base_url()?>logs/view/display_logs_day",										
					data : {group_id : <?=$groups[0]['group_id']?>, "date" : db_date}, //add date as 2nd param here					
					success : function (data){							
						$("#logs_content").append(data.logs_v); //logs_v
					}
				});				
				
			}
			
			function upload(){
				/*Uploader*/
				$fub = $('#uploader');
				$msg = $('#upload_msg');
				
				var uploader = new qq.FileUploaderBasic({
					button : $fub[0], 
					debug : true,
					action : 'logs/add/attach',		
					allowedExtensions : ['jpeg', 'jpg', 'png', 'bmp', 'doc', 'docx', 'xls', 'xlsx'],
					sizeLimit : 2097152, //2MB
					onSubmit: function(id, fileName) {
						$msg.append('<input type="checkbox" id="file-' + id + '" style="padding-left: 10px" name="file_name[]" checked value="' + fileName + '"/>' +fileName);
						//return false;
					},					
					onComplete : function(id, fileName, responseJSON){
						if(responseJSON.success){					
							$('#upload_size').append('<input type="checkbox" id="file-' + id + '" name="file_size[]" checked value="' + responseJSON.size/1024 + ' Kb">');					
							$('#file-' + id).attr('value' ,responseJSON.filename); //fileName
							}else{
							console.log(responseJSON.error);
						}
					}
					
				});
				
			}

			function load_js(){
				if($("#logs_script").length){
					//do nothing
				}else{
					$.ajax({
					  url: "<?php echo m_load_module_assets('logs', 'js/log.js')?>",
					  dataType: "script",
					  success: function(e){					  						  							  		
				  		var scriptElement = document.createElement('script');
					    scriptElement.setAttribute('type', 'text/javascript');
					    scriptElement.setAttribute('id', 'logs_script');						     
					    scriptElement.textContent = e;
					    var head = document.getElementsByTagName('head').item(0);
					    head.appendChild(scriptElement);					  
					  }
					});
				}
			}
			
		</script>
      