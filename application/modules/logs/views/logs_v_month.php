<div id="logs" class="tab-pane active">
	<div class="row-fluid">
		<div class="span24" id="calendar">
			<?php 	
			// echo "<pre>";
			// print_r($logs);
			// echo "</pre>";	


			 /* draw table */
			  $calendar = "<table id='cal'>";

			  /* table headings */
			  $headings = array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
			  $calendar.= "<thead><tr><th>".implode("</td><th>",$headings)."</th></tr></thead>";

			  /* days and weeks vars now ... */
			  $day_of_week = date("w",mktime(0,0,0,$month,1,$year)); //0 to 6
			  $days_in_month = date("t",mktime(0,0,0,$month,1,$year)); //28 or 31 or 30
			  $this_day = date("j"); //returns day of the month
			  $this_month = date("n"); //returns numeric representation of current month
			  $days_in_this_week = 1;
			  $day_counter = 0;
			  $dates_array = array();

			  /* row for week one */
			  $calendar.= "<tbody><tr>";

			  /* print "blank" days until the first of the current week */
			  for($x = 0; $x < $day_of_week; $x++){
			    $calendar.= "<td class='premonth'>&nbsp;</td>";
			    $days_in_this_week++;
			  }

			  /* keep going with days.... */
			  for($list_day = 1; $list_day <= $days_in_month; $list_day++){

				if($list_day == $this_day && $month == $this_month){	
					$calendar.= "<td class='today'>";			      				
				}else{
					$calendar.= "<td class='this_month'>";
				}

			    /* add in the day number */
			    $calendar.= "<div class='day'>".$list_day."</div>";

			    /* add logs if there is from the db */
			    $calendar.= "<div class='month_log'>";
			    foreach ($logs as $key) {

			    	$date_temp = date_create($key["log_date"]);

			    	$log_day = date_format($date_temp, "d");

					if($log_day == $list_day){

						 $calendar.= "<div class='expand' data-id=".$key["log_id"].">".$key["log_title"]." &middot; ".$key["username"]."</div>";
						 $calendar.= "<div class='log_data_".$key["log_id"]."' style='display:none'>".$key["username"]."-".$key["log_time"]."-".$key["log_title"]."-".$key["log_content"]."</div>";
						 

					}			    
			    }
			   			   
			      
			    $calendar.= "</div></td>";

				

			    if($day_of_week == 6){
			      $calendar.= "</tr>";
			      if(($day_counter+1) != $days_in_month){
			        $calendar.= "<tr>";
			      }

			      $day_of_week = -1;
			      $days_in_this_week = 0;

			    }

			    $days_in_this_week++;
			    $day_of_week++;
			    $day_counter++;
			  }

			  /* finish the rest of the days in the week */
			  if($days_in_this_week < 8){
			    for($x = 1; $x <= (8 - $days_in_this_week); $x++){
			      $calendar.= "<td class='aftermonth'>&nbsp;</td>";
			    }
			  }

			  /* final row */
			  $calendar.= "</tr>";

			  /* end the table */
			  $calendar.= "</tbody></table>";

				echo $calendar;			  
	  		?>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function(){

	/* make td height and width equal */
	var td_h = $("#cal tr td").css("width");		
	$("#cal tr td").css("height", td_h);

	$(".expand").click(function(){
		var id = $(this).attr("data-id");

		var log_data = $(".log_data_"+id).text();

		var log_data_arr = log_data.split("-"); //might be a problem if logs contains dash (-)

		
		var log_author = log_data_arr[0];
		var log_time = log_data_arr[1];
		var log_title = log_data_arr[2];
		var log_content = log_data_arr[3];
		
		var modal_body = "<div class='modal_log_time'>"+log_time+"</div>";
		modal_body += "<div class='modal_log_content'>"+log_content+"</div>";		
		
		//alert(log_author + " - " + log_time + " - " + log_content);	

		$("#log_modal_title").text(log_title);
		$("#log_modal_body").html(modal_body);

		$("#log_modal").modal();
	});	

});
</script>