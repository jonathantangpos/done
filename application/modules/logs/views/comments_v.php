<div id="comment_container">
	<?php 
	
		foreach($comments as $key){
			echo "<div class='comment'>";
			echo "<p>".$key['comment_content'];
			echo " | Author: ".$key['username']."</p>";
			echo "</div>";			
		}
	?>
	<form id="comment_form" action="">
		<textarea cols="2" id="comment_box" name="comment" onblur="hide_comment_button(this);" onfocus="show_comment_button(this);" ></textarea>
		<input type="hidden" value="<?php echo $log_id[0]?>" name="log_id" />			
		<input type="submit" class="btn btn-mini btn-primary" id="comment_button" style="display: none;" value="Comment" />
	</form>

</div>
