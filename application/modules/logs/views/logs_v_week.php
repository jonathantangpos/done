<div id="logs" class="tab-pane active">
	<?php 

  		// echo "<pre>";
  		// print_r($logs);
  		// echo "</pre>";
		$s_car_identifier = $this->session->userdata('s_car_identifier');
		
		if($s_car_identifier){
			$car_identifier = $s_car_identifier;									
		}else{
			$car_identifier = 0;			
		}
		
		
		if(count($logs) > 0 ){			
			$car_counter = 0;
			foreach ($logs as $row){
				if(count($row['logs'])>0 ){
					echo "<div class='row-fluid' style='padding-top: 10px;'>";
						echo "<div class='span3 hour'><h4>".$row['log_wd']."</h4></div>";
						echo "<div class='span21' id='log_row'>";
						
							echo "<div class='row-fluid'>";
								echo "<div class='span2'>";								
									echo "<a id='carousel-left' href='#myCarousel_".$car_identifier."' data-slide='prev'>&lsaquo;</a>";
								echo "</div>";
								echo "<div class='span20'>";
									echo "<div id='myCarousel_".$car_identifier."' class='carousel slide' data-interval='false'>"; //find a way to make each of this unique from each other
									echo "<div class='carousel-inner'>";
										
									
										$display = 6;
										$logs_count = count($row['logs']);
										$car_items = ceil($logs_count / $display);
											
										$slices = array_chunk($row['logs'], $display);
										for($i = 0; $i < $car_items; $i++){
											echo "<div class='item'><div class='row-fluid'>";
											foreach($slices[$i] as $key){
												echo "<div class='span4 logs' data-id='".$key['log_id']."'>";
													echo "<div class='log_head'>";
														echo "<div class='log_author'>".ucfirst($key['log_author'])."</div>";
														echo "<div class='log_time'>".$key['log_time']."</div>";
														echo "<hr class='log_hr' />";
													echo "</div>";
													echo "<div class='log_body'>";
														echo "<div class='log_title'>".$key['log_title']."</div>"; //log_content
														echo "<div class='log_content' style='display: none;'>".$key['log_content']."</div>"; //log_content
													echo "</div>";
													echo "<div class='log_footer'>";
														echo "<span class='log_btn' title='Download attachments'><i class='icon-paper-clip'></i></span>";
														echo "<span class='log_btn' title='Share to Facebook'><i class='icon-facebook'></i></span>";
														echo "<span class='log_btn' title='Post to twitter'><i class='icon-twitter'></i></span>";
														echo "<span class='log_btn expand' title='Expand log'><i class='icon-fullscreen'></i></span>";

														//echo "<span class='log_btn delete' title='Delete log' data-id=".$key['log_id']."><i class='icon-trash'></i></span>";
													echo "</div>";											
												echo "</div>";
											}
											echo "</div></div>";
										}																			
									echo "</div>";
								echo "</div>";
							echo "</div>";
							echo "<div class='span2'>";
							echo "<a id='carousel-right' href='#myCarousel_".$car_identifier."' data-slide='next'>&rsaquo;</a>";
							echo "</div>";
							
								
								
								
								
							echo "</div>";						
						echo "</div>";
					echo "</div>";
					$car_identifier++;	
				}			
			}
		}else{
			
			$this->session->unset_userdata('s_group_id');	
			
			echo "<div class='item'><div class='row-fluid'>";
			echo "<div class='span24 logs'><h4 style='text-align: center;'>This group has no logs.</h4></div>";
			echo "</div></div>";			

		}
		
		$this->session->set_userdata('s_car_identifier', $car_identifier);
	?>
		
</div>

<script type="text/javascript">
$(function(){

	/* Tweak to make first item in tab active*/
	$(".carousel-inner").each(function(){
		$(this).children('div:first').attr("class", "active item");
		
	});	

	$(".expand").click(function(){
		var el = $(this).parents(".logs");
		
		var log_author = el.find(".log_author").text();
		var log_time = el.find(".log_time").text();
		var log_title = el.find(".log_title").text();
		var log_content = el.find(".log_content").text();		
		
		var modal_body = "<div class='modal_log_time'>"+log_time+"</div>";
		modal_body += "<div class='modal_log_content'>"+log_content+"</div>";		
		//alert(log_author + " - " + log_time + " - " + log_content);	

		$("#log_modal_title").text(log_title);
		$("#log_modal_body").html(modal_body);

		$("#log_modal").modal();
	});	

});
</script>