/*
* @name: delete_log
* @params: log_id
* @desc: disabled functionality, not avaialable
*/
$(".delete").click(function(){
	var el = $(this);

	var log_id = el.attr("data-id");
	
	$.ajax({
		type : "POST",
		dataType: "json",
		url : "logs/delete/delete_log",					
		data : {"log_id" : log_id},
		success : function (data){	
			if(data.msg['code'] == 0){
				el.parents("div.logs").remove();

			}else{
				console.log(data.msg['msg']);
			}				
		}
	});
});	
	
				
/*
 * Save Comment
 * 
 * */

$("input#comment_button").click(function(e){
	var form = e.target.form;
	e.preventDefault();
	$.ajax({
		type : "POST",
		dataType : "json",
		url : "<?php echo base_url()?>logs/view/save_comment",						
		data : $(form).serialize(),
		success : function(data){
			console.debug(data);
			var f = $(form).find("textarea").val("");
			hide_comment_button(f);
			var new_comment = "<div class='comment'><p>"+data["comment"]+" | "+data["user_id"]+"</p></div>";
			$(new_comment).insertBefore(form);				
			},
		error : function(data){
			console.log("Error: "+data);
			}
		});
});


/* Show/Hide comments */			
$("input#show_comments").click(function(){
	if($(this).val() == "Show Comments"){
		$(this).siblings("div#comment_container").show(400);
		$(this).attr("value", "Hide Comments");
	}else{
		$(this).siblings("div#comment_container").hide(400);
		$(this).attr("value", "Show Comments");
		}
});
	


function hide_comment_button(e){
	var val = $(e).val();
	if(!val){				
		$(e).siblings("#comment_button").css("display", "none");
	}else{			
		$(e).siblings("#comment_button").css("display", "inline");
		}	
}


function show_comment_button(e){		
	$(e).siblings("#comment_button").css("display", "inline");
}



