<!DOCTYPE html>
<html lang="en">
    <head>
        <title>LogIn</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php $this -> load -> view('loader/head_loader');?>
    </head>
    <body >
        <div class="header">
            <div class="container">
                <div class="row-fluid">
                    <div class="span3 pull-right sign-up">
                        <?php echo anchor('','SIGN UP');?>
                    </div>
                </div>
            </div>
        </div>
        <div class="body">

            <div class="container">

                <!-- start error alerts -->
                <?php 

                    if(validation_errors()){
                        ?>
                        <div class="alert alert-error login-error">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <?php echo validation_errors()?>
                        </div>

                        <?php
                    }else if($message !== ""){
                        ?>
                        <div class="alert alert-error login-error">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <?php echo $message;?>
                        </div>
                        <?php
                    }else{

                    }

                ?>
                <!-- end error alerts -->

                <div class="span11 offset6">
                    <div class="login-container">
                        <h2>Sign In</h2>
                        <!-- login form -->
                        <?php echo form_open("login");?>
                            <input type="text" name="username" placeholder="Username" />
                            <input type="password" name="password" placeholder="Password"/>
                            <input type="submit" value="LOGIN"/><br />
                        <?php echo form_close();?>
                        <!-- end login form -->
                        <div class="social-network-container">
                            <ul>
                                <li><a href="<?php echo base_url()?>authenticate/validate/fb_request"><div><img src="<?php echo base_url('application/views/assets/app/img/fb-icon.png');?>"/>Sign In with Facebook</div></a></li>
                                <li><a href="#"><div><img src="<?php echo base_url('application/views/assets/app/img/likedIn-icon.png');?>"/>Sign In with LikedIn</div></a></li>
                            </ul>
                            <ul>
                                <li><a href="#"><div><img src="<?php echo base_url('application/views/assets/app/img/google-icon.png');?>"/>Sign In with Google</div></a></li>
                                <li><a href="#"><div><img src="<?php echo base_url('application/views/assets/app/img/twitter-icon.png');?>"/>Sign In with Twitter</div></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo $this -> load -> view('loader/bottom_loader');?>
    </body> 
</html>