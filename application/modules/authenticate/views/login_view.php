<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bootstrap</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php $this -> load -> view('loader/head_tag');?>
	</head>
	<body>

		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<button class="btn btn-navbar" type="button" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="brand" href="#">Done System</a>
					<div class="nav-collapse collapse">
						<ul class="nav pull-right">
							<li><a href="#">Welcome back Jonathan! </a></li>
							<li class="dropdown">
								<a class="dropdown-toggle" id="dLabel"  role="button"  data-toggle="dropdown" data-target="#" href="#"><b class="icon-envelope"> </b> Notification <b class="caret"></b></a>
								
								<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
									<li class=""><a href="#">Bryan invited you to join BLUH Group</a></li>
									<li class="divider"></li>
									<li class=""><a href="#">Hara accepted your request.</a></li>
									<li class="divider"></li>
									<li class=""><a href="#">Nino added you in Chugs Group.</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a class="dropdown-toggle" id="dLabel"  role="button" data-toggle="dropdown" data-target="#" href="#"><b class="icon-cog"> </b> Settings <b class="caret"></b></a>
								 
								<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
									<li class=""><a href="<?php echo base_url('profile')?>"><b class="icon-user"> </b> Profile</a></li>
									<li class=""><a href="<?php echo base_url('profile')?>"><b class="icon-signal"> </b> Account</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-top: 100px;">
			<div class="container">
				<?php echo anchor('','Register');?>
				<?php echo validation_errors()?>
				<?php echo form_open("login","class='form-horizontal'");?>
					<fieldset>
						<legend>Login</legend>
						<div class="control-group">
						    <label class="control-label" >Username</label>
						    <div class="controls">
						      <input type="text" name="username" placeholder="Username"/>
						    </div>
					  	</div>
						<div class="control-group">
						    <label class="control-label" for="inputPassword">Password</label>
						    <div class="controls">
						      <input type="password" name="password" id="inputPassword" placeholder="Password">
						    </div>
						</div>
					  	<div class="control-group">
						    <div class="controls">
						      <label class="checkbox">
						        
						      </label>
						      <button type="submit" class="btn">Sign in</button>
						    </div>
					  	</div>
					<fieldset>
				<?php echo form_close();?>
			</div>
		</div>




	<?php echo $this -> load -> view('loader/footer_tags');?>
	</body>
</html>