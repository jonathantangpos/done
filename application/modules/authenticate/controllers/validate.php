<?php

class Validate extends MX_Controller{

	var $data;
	
	public function __construct(){
		
		parent::__construct();
		
		$this -> data = array();
		$this -> load -> library('rest_client/curl');
		$this -> load -> library('form_validation');
		$this -> load -> model('login_model');
		$this->load->library('rest_client/curl');
		
	}
	
	

	public function fb_request(){

		$this -> load -> library('fbconnect');

		$data = array(
			'redirect_uri' 	=> site_url('authenticate/validate/handle_fb_login'),
			'scope'			=> 'email,user_birthday,user_hometown'
		);

		redirect($this -> fbconnect -> getLoginUrl($data));

	}

	public function handle_fb_login(){

 		$this -> load -> library('fbconnect');
 
 		if ($this -> fbconnect -> user) {

 			$temp_data = $this->fbconnect->user;
			
			
 			if($this -> login_model -> is_member($temp_data['id'],$temp_data['email'])){

 				$username = $temp_data['username'];
				$password = $temp_data['id'];
			
				$user_info =  $this->curl->init()->add_cookie('user')->post('auth/login',array(
					'username'      =>  $username,
					'password'      =>  $password
				));
				// echo "<pre>";
				// print_r($user_info);
				// echo "</pre>";
				$user_info = json_decode($user_info);
				if($user_info-> error == 0){
					
					$nicheez_id = $user_info-> data -> user_id;
					$success = $this -> login_model -> data_login($nicheez_id);
					
					$info = array();
					
					foreach ($success->result() as $value){
						
						$info['user_id'] = $value -> user_id;
						$info['username'] = $value -> username;
						$this -> session -> set_userdata('avatar',$value -> avatar);
					}
					$this -> session -> set_userdata('is_logged',$info);
					

				}else{
				
				
				
				
				
				
				
				
				
				
				
				
				
				
					echo "contact jonathan";
				}
 								
				
				/*$lol = $this -> session -> userdata('is_logged');
				echo "<pre>";
				print_r($success->result());
				echo "</pre>";
				echo $temp_data['id'];*/
				//redirect('dashboard?p=groups');
				//echo "losls";
 			}else{
				$this -> session -> set_userdata('fb_info',$temp_data);
 				$temp['fb'] = $temp_data;
 				$temp['captcha'] =  $this -> get_captcha();
				$this -> load -> view('captcha',$temp);
 				
				echo "ddddd";
 				
 			}

 		}else{
 			echo "Could not login at this time";
			
 		}
	}

	public function fbregister(){
		$this -> load -> model('group/group_model');
		$this -> load -> model('user/user_model');
		$info = $this -> input -> post('info');
		$captcha = $this -> input -> post('captcha');

		$fb_info = explode(':', $info);

		
		// print_r($fb_info);
		
		$parameters = array(
			'captcha'	=>	$captcha,
			'username'	=>	$fb_info['2'],
			'password'	=>	$fb_info['1'],
			'email'		=>	$fb_info['0']
		);
		
		// $con = curl_init('http://nicheezapi.com.nu/register/user');
		
		$register = $this -> auto_register($parameters);
		
		if($register->error==1){
			//$this -> handle_fb_login();
			echo "lol";
		}else{
			
			
			//$user_info =  $this->curl->init()->cookie()->post('users/update_profile',$parameters);
			$this -> user_model -> insert_user_information($register -> data, $fb_info['2'], $fb_info['0'],1,$fb_info['1']);
			//echo $fb_data['last_name'];
			//echo "<pre>";
			//print_r($fb_data);
			//echo "</pre>";
			
			
			$user_info =  $this->curl->init()->add_cookie('user')->post('auth/login',array(
				'username'      =>  $fb_info['2'],
				'password'      =>  $fb_info['1']
			));
			
			$user_info = json_decode($user_info);
			if($user_info-> error == 0){
				$nicheez_id = $user_info-> data -> user_id;
				$success = $this -> login_model -> data_login($nicheez_id);
				
				
				$info = array();
				
				foreach ($success->result() as $value){
					
					$info['user_id'] = $value -> user_id;
					$info['username'] = $value -> username;
					$this -> session -> set_userdata('avatar',$value -> avatar);
				}
				$this -> group_model -> add_group('Draft','For drafts only',$info['user_id']);
				$this -> session -> set_userdata('is_logged',$info);
				
				
				$fb_data = $this -> session -> userdata('fb_info');
			
			
				$firstname = $fb_data['first_name'];
				$lastname = $fb_data['last_name'];
				$birthdate =  $fb_data['birthday'];
				$address = $fb_data['hometown']['name'];

				$info =  array(
					'firstname'	=>  $firstname,
					'lastname'	=>  $lastname,
					'birthdate'	=>  $birthdate,
					'address'	=>  $address
				);
				
				$this->curl->init()->cookie()->post('users/update_profile',$info);
				 
			}else{
				echo "Please contact jonathan for this just make the subject 'FB log errs'";
			}
			
			
			
			$profile = $this->curl->init()->cookie()->get('users/view_profile');
			$profile = json_decode($profile);
			
			$this->session->unset_userdata('fb_info');
			
			redirect('dashboard?p=groups');
		}

		
		// return $register;

	}
	
	public function auto_register( $parameters){
		//$con = curl_init('http://nicheezapi.com.nu/register/user');
		$con = curl_init('http://api.nicheez.zoog/register/user');
		curl_setopt($con, CURLOPT_POST, true);
		curl_setopt($con, CURLOPT_POSTFIELDS, $parameters);
		curl_setopt ($con, CURLOPT_RETURNTRANSFER, true);
		$temp_file = $this->session->userdata('tmp_file');
        curl_setopt($con, CURLOPT_COOKIEFILE,$temp_file);
		curl_setopt($con,CURLOPT_HTTPHEADER,array('appid : 1234','secret : 6b2ded51d81a4403d8a4bd25fa1e57ee'));
		$register = curl_exec($con);
		curl_close($con);
		$register = json_decode($register);
	
		return $register;
	}
	
	public function get_captcha(){
		 //$con = curl_init('http://nicheezapi.com.nu/register/user');
		$con = curl_init('http://api.nicheez.zoog/register/user');
		curl_setopt ($con, CURLOPT_RETURNTRANSFER, true);
		$temp_file = tempnam('/tmp', 'nich');
        curl_setopt($con, CURLOPT_COOKIEJAR,$temp_file);
		$this->session->set_userdata('tmp_file', $temp_file);
		curl_setopt($con,CURLOPT_HTTPHEADER,array('appid : 1234','secret : 6b2ded51d81a4403d8a4bd25fa1e57ee'));
		$captcha = curl_exec($con);
		curl_close($con);
		$captcha = json_decode($captcha);
		
		return $captcha -> data -> url;
	}

	public function login(){

		$this -> data['message'] = "";

		if($this -> session -> userdata('is_logged')){

			redirect('dashboard','refresh');

		}else{

		
		$this -> form_validation -> set_rules('username','Username','required|trim');
		$this -> form_validation -> set_rules('password','Password','trim|required');
		
		if($this -> form_validation -> run() == FALSE){
			
			$this -> load -> view('login',$this -> data);

		}else{
			
				$username = $this -> input -> post('username');
				$password = $this -> input -> post('password');
			
				$user_info =  $this->curl->init()->add_cookie('user')->post('auth/login',array(
					'username'      =>  $username,
					'password'      =>  $password
				));
				// echo "<pre>";
				// print_r($user_info);
				// echo "</pre>";
				$user_info = json_decode($user_info);
				if($user_info-> error == 0){
					
					$nicheez_id = $user_info-> data -> user_id;
					$success = $this -> login_model -> data_login($nicheez_id);
					
					$info = array();
					
					foreach ($success->result() as $value){
						
						$info['user_id'] = $value -> user_id;
						$info['username'] = $value -> username;
						$this -> session -> set_userdata('avatar',$value -> avatar);
					}
					$this -> session -> set_userdata('is_logged',$info);
					//$profile = $this->curl->init()->cookie('user')->get('users/view_profile');
					 //echo "<pre>";
					 //print_r($profile);
					 //echo "</pre>";
					// $log = $this -> session -> userdata('is_logged');
					
					// echo $log['user_id'];

					redirect('login','refresh');

				}else{

					$this -> data['message'] = "<strong>Login Incorrect! </strong>Check your username and password";
					$this -> load -> view('login',$this -> data);
				}
			
			}//end else

		}//end else





	}
	
	public function logout(){
		
		//$this->curl->init()->refresh_cookie('user')->post('auth/logout');
		
		$this->session->unset_userdata('is_logged');
		$this->session->unset_userdata('avatar');
		$this->session->unset_userdata('activated');
		$this->session->flashdata('is_logged');
		$this->session->flashdata('activated');
		$this->session->flashdata('avatar');
		
		$this->session->unset_userdata('s_origin');
		$this->session->unset_userdata('s_offset');
		$this->session->unset_userdata('s_group_id');
		$this->session->unset_userdata('s_car_identifier');
		$this->session->sess_destroy();
		redirect('login','refresh');
	}
	
}