<?php
class Connections extends MX_Controller{
	
	function __construct(){
		parent::__construct();			
		$this->load->model('group_model');
		$this->load->model('user_model');
	}
			
	function load_connections(){
		$user_id = 2;			
		$data['tabs'] = $this->group_model->get_groups($user_id);								
		$data['back'] = $this->render_widgets('backtolog_widget', '');
		$data['vertical'] = $this->render_widgets('verticaltabs_widget',$data);
		
		$this->load->view('connection_view', $data);		
		$this->load->view('widgets/groupnamemodal_widget', $data);					
	}

	function render_widgets($widget, $param){
		return $this->load->view('widgets/'.$widget,$param,TRUE);
	}
	
	function get_group_manager(){
		$group = $this->input->post('group_name');
		$data['mgr'] = $this->group_model->get_group_manager($group);
		echo $data['mgr']['group_manager'];
	}
	
	function ajax_func(){
		$group_name = $this->input->post('group_name');
		$data['manager'] = $this->group_model->get_group_manager($group_name);
		$data['ids'] = $this->group_model->get_group_members($group_name);
		$data['group_id'] = $this->group_model->get_group_id($group_name);
		$data['groupcontent'] = $this->render_widgets('groupcontent_widget', $data);
		// in model function run your query to fetch data   		
		echo json_encode($data);
		}

	/*
	 * Desc: Deletes group from db
	 * Params: group_id
	 */
	function delete_group(){
		$group_id = $this->input->post('group_id');
		$this->group_model->delete_group($group_id);
		$msg = "Group was successfully deleted!";
		echo $msg;
	}

	/*
	 * Desc: Used to render the vertical tabs from ajax load in connection_view controller
	 * Params: user_id - taken from ajax function
	 */	
	function render_vtabs(){
		$user_id = $this->input->post('user_id');
		$data['tabs'] = $this->group_model->get_groups($user_id);
		echo $this->load->view('widgets/verticaltabs_widget', $data, TRUE);			
	}
	
}
?>