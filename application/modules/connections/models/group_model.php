<?php
class Group_model extends CI_Model{	
	
	/**
	 * Get all groups in db
	 * Returns all group_name as array
	 **/	
	function get_groups($user_id){
		$this->db->select('g.group_name');
		$this->db->from('groups AS g');
		$this->db->join('members AS m', 'g.group_id = m.group_id');
		$this->db->where('m.user_id', $user_id);
		return $this->db->get()->result_array();
	}
	/**
	 * Get group members, returns nicheez_id
	 * used to look up data on Nicheez db
	 * Returns array of int
	 * */
	function get_group_members($group_name){				
		
		/*active records way*/		
		$this->db->select('u.user_id');
		$this->db->from('users AS u');
		$this->db->join('members AS m', 'u.user_id = m.user_id');
		$this->db->join('groups AS g', 'm.group_id = g.group_id');
		$this->db->where('g.group_name', $group_name);
		return $this->db->get()->result_array();			
	}
	
	/**
	 * Get group manager as an id to reference the user against user.user_id
	 * Returns an int
	 **/
	function get_group_manager($group_name){
		$this->db->select('g.group_manager');
		$this->db->from('groups AS g');
		$this->db->where('g.group_name', $group_name);
		return $this->db->get()->row_array(0);
		
	}

	function check_group_exist($group_name, $user_id){
		$this->db->select('m.member_id');
		$this->db->from('groups AS g');
		$this->db->join('members AS m', 'g.group_id=m.group_id');
		$this->db->where('g.group_name', $group_name);
		$this->db->where('m.user_id', $user_id);
		if($this->db->get()->num_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
		
	function add_group($group_name, $user_id){
		$this->load->helper('date');
		$datestring = '%Y-%m-%d';
		$data = array(
			 'group_id' => NULL,
			 'group_name' => $group_name,
			 'group_manager' => $user_id,
			 'date_created' => mdate($datestring)
			);
		$this->db->insert('groups', $data);
	}

	function add_member($group_id, $user_id){
		$data = array(
			'member_id' => NULL,
			'group_id' => $group_id,
			'user_id' => $user_id,
			);
		$this->db->insert('members', $data);
	}

	function get_group_id($group_name){
		$this->db->select('group_id');
		$this->db->from('groups');
		$this->db->where('group_name', $group_name);
		return $this->db->get()->row_array(0);
	}

	function delete_group($group_id){
		$this->db->delete('groups', array('group_id' => $group_id));
	}
}


?>