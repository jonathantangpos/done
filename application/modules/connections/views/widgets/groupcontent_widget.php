<?php
/**
 * Description: Widget to load the contents of vertical tab. 
 **/
?>
<?php $user_id = 2; ?>
<ul class="vertical tabs-content">
	<li class="active" id="developmentTab">
		<div class="panel">
			<div class="row" id="groups">	
				<h4>Group Manager</h4>
				<div class="four columns">
					
					<div class="user-box">
						<img src="" />
						<span style="float:left; padding-left: 10px;"><?php echo $manager['group_manager']; ?>Son Goku</span>
						<div id="user_desc">Manager</div>				
					</div>
				</div>													
			</div>
			<div class="row" id="groups">
				<h4>Members</h4>
				<?php
																				
					foreach($ids as $row){															
				?>													
					<div class="four column">
						
						<div class="user-box">
							<img src="" />
							<span style="float:left; padding-left: 10px;"><?php echo $row['user_id'];?>Bryan Serad</span>
							<div id="user_desc">Developer</div>
							<?php if($manager['group_manager']==$user_id){?><a title="Remove Member" class="member-remove"></a><?php }?>
						</div>

					</div>
				<?php }?>
				
			</div>
			<div class="row" id="groups">
				<h4>Add more Members</h4>
				<div class="ten columns">
					<input type="text" class="twelve" placeholder="Add users' email address/es"></input>
				</div>
				<div class="two columns">
					<input type="button" class="button medium" value="Add"></input>
				</div>
			</div>
			<?php if($manager['group_manager']==$user_id){?>
			<div class="row">
				<div class="one columns offset-by-eleven">
					<input id="del_group_button" type="button" class="button medium" value="Delete Group"></input>
				</div>
			</div>
			<?php }?>
		</div>
	</li>		
</ul>

<!--div id="info_msg"></div-->

<script type="text/javascript">	
	$(document).ready(function(){
		
		$("#del_group_button").click(function(){
			$("#del_group_confirm").reveal({
				dismissModalClass : "close_modal",
				 // opened: function(){
				 // 	alert("Modal is open!");
				 // }
			});

			$("#del_group_ok_button").click(function(){
				// Close the del_group_confirm modal
				$("del_group_confirm").trigger("reveal:close");

				/**
				 * Description: Run ajax function to delete a group
				 * Params: none
				 **/
				$.ajax({
					type : "POST",				
					url : "<?php echo base_url()?>connections/delete_group",
					dataType: "text",
					cache : false,
					data : "&group_id="+<?php echo $group_id['group_id']; ?>,
					success: function(data){
						// Set the message returned from ajax to #msg_content					
						$("#msg_content").html(data);
						// Show the message
						$("#delete_group_msg").reveal({
							dismissModalClass : 'close_modal',
							closed: function(){
								// Remove the deleted tab when $delete_group_msg is closed								
								$("dd#ver_tab.active").remove();
								preLoad();
							}						
						});
					}
				});

			});
			
			
		});			
	});
	

</script>