<dl class="vertical tabs">	
	<?php foreach($tabs as $key):?>
		<dd id="ver_tab"><a href="javascript:void(0)" onclick="ajaxFunc('<?php echo ($key['group_name']) ?>')" id="ver_tab_a"><?php echo ucwords($key['group_name']);?></a></dd>		
	<?php endforeach ?>
		<dd><a id="new_group_button" href="#">+ New Group</a></dd>
</dl>

<script type="text/javascript">
	$(document).ready(function(){
		/*Reveal Modal*/
		$("#new_group_button").click(function(e){
			$("#add_group_modal").reveal({
				dismissModalClass : 'close_modal',
				closeOnBackgroundClick : 0
			});
			e.preventDefault();
		});
	});	
</script>