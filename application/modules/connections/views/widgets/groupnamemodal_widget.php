<div id="add_group_modal" class="reveal-modal">
	<div id="modal_head">
			<span>Add Group</span><span class="close_modal" id="x_modal">&#215;</span>
	</div>	
	<div id="modal_content">
		<?php echo form_open('','id=new_group_form');?>									
			<label for="new_group_input">Add New Group</label>
			<input id="new_group_input" name="group_name" type="text" class="four" placeholder="Group Name" />
			<div id="errMsg"></div>
			<input id="add_group_button" type="button" value="Add" class="secondary button medium" />				
			<input id="cancel_modal_button" type="button" value="Cancel" class="secondary button medium close_modal" />
		<?php echo form_close();?>
	</div>
</div>

<div id="delete_group_msg" class="reveal-modal">
	<div id="modal_head">
			<span>Message</span><span class="close_modal" id="x_modal">&#215;</span>
	</div>	
	<div id="modal_content">
		<div id="msg_content" style="padding-bottom: 10px;"></div>
		<input type="button" value="OK" class="secondary button medium close_modal" />
	</div>
</div>

<div id="del_group_confirm" class="reveal-modal">
	<div id="modal_head">
			<span>Confirm</span><span class="close_modal" id="x_modal">&#215;</span>
	</div>	
	<div id="modal_content">
		<div style="padding-bottom: 10px;">Are you sure you want to delete this group?<br />This cannot be undone!</div>
		<input id="del_ok_button" type="button" value="OK" class="secondary button medium close_modal" />
		<input id="cancel_modal_button" type="button" value="Cancel" class="secondary button medium close_modal" />
	</div>
</div>