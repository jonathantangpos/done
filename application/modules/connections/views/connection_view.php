<!--
* Notes on id's
* ver_tab : refers to the specific tab, assigned on <dd> element in verticaltabs_widget
* ver_tabs : refers to the div which contains the whole vertical tab
-->

<?php echo $back?>
<!--Content Start==========================================-->	

<div class="row" id="content">
	<div class="twelve columns centered">
		<dl class="tabs">
		  <dd class="active"><a href="#connections">Connections</a></dd>
		  <dd><a href="#groups">Groups</a></dd>					  
		</dl>
		<ul class="tabs-content">
			<li class="active" id="connectionsTab">
				<div class="tab-content">								
					<div class="panel">
					<?php 
						$socialIcon = array('fb.png', 'twtr.png', 'linkedin.png', 'g+.png');
						$emailAdd = 'jtangpos@gmail.com';
						for($x = 0; $x<4; $x++){?>																													
						<ul class="connection-list">
							<li><img src="/assets/images/icons/<?php echo $socialIcon[$x];?>" /></li>
							<li><input type="email" placeholder="<?php echo $emailAdd;?>" class="six" /></li>
							<li><input type="button" class="medium button" value="Connect" /></li>
						</ul>
					<?php 
						}
					?>
					</div>
				</div>
			</li>
			<li id="groupsTab">
				<div class="tab-content">
					<div class="row">
						<div class="three columns" id="ver_tabs">
							<?php
								echo $vertical; //renders the verticaltabs_widgets.php
							?>
						</div>
						<div class="nine columns" id="group_content">
							<?php
								//renders the groupcontent_widget.php via ajax call
							?>
							
						</div>
					</div>
				</div>
			</li>					  
		</ul>
		<!--groupnamemodal_widget is rendered here invisibly-->
	</div>
</div>
<!--Content Ends==============================================================-->