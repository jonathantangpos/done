<?php

class Validate extends MX_Controller{

	var $data;
	
	public function __construct(){
		
		parent::__construct();
		
		$this -> data = array();
		$this -> load -> library('rest_client/curl');
		$this -> load -> library('form_validation');
		$this -> load -> model('login_model');
		
	}
	
	
	
	public function login(){

		if($this -> session -> userdata('is_logged')){

			redirect('logs','refresh'); //profile

		}else{

		
		$this -> form_validation -> set_rules('username','Username','required|trim');
		$this -> form_validation -> set_rules('password','Password','trim|required');
		
		if($this -> form_validation -> run() == FALSE){
			$this -> load -> view("login_view");

		}else{
			
				$username = $this -> input -> post('username');
				$password = $this -> input -> post('password');
			
				$user_info =  $this->curl->init()->add_cookie('user')->post('auth/login',array(
					'username'      =>  $username,
					'password'      =>  $password
				));
				// echo "<pre>";
				// print_r($user_info);
				// echo "</pre>";
				$user_info = json_decode($user_info);
				if($user_info-> error == 0){
					
					$nicheez_id = $user_info-> data -> user_id;
					$success = $this -> login_model -> data_login($nicheez_id);
					
					$info = array();
					
					foreach ($success->result() as $value){
						
						$info['user_id'] = $value -> user_id;
						$info['username'] = $value -> username;
						$this -> session -> set_userdata('avatar',$value -> avatar);
					}
					$this -> session -> set_userdata('is_logged',$info);
					// echo "<pre>";
					// print_r($user_info);
					// echo "</pre>";
					// $log = $this -> session -> userdata('is_logged');
					
					// echo $log['user_id'];

					redirect('login','refresh');

				}else{
					$this -> load -> view("login_view");
				}
			
			}//end else

		}//end else





	}
	
	public function logout(){
	
		$this->curl->init()->refresh_cookie('user')->post('auth/logout');
		$this->session->sess_destroy();
		$this->session->unset_userdata('is_logged');
		$this->session->unset_userdata('image');
		$this->session->flashdata('is_logged');
		$this->session->flashdata('image');

		$this->session->unset_userdata('s_origin');
		$this->session->unset_userdata('s_offset');
		$this->session->unset_userdata('s_group_id');
		$this->session->unset_userdata('s_car_identifier');
		
		
		redirect('login','refresh');
	}
	
}