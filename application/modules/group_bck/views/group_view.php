<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bootstrap</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
		<meta content="utf-8" http-equiv="encoding">
		<?php $this -> load -> view('loader/head_tag');?>
		<?php echo $this -> load -> view('loader/footer_tags');?>
		<style type="text/css">
			.optioness{
				margin-top: -30px;
				margin-right: 10px;
			}
			.member-wrapper{
				margin-top: 30px;
			}
			.member-wrapper .span1{
				margin-bottom: 20px;
			}
		</style>
	</head>
	<body>
		
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<button class="btn btn-navbar" type="button" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="brand" href="#">Done System</a>
					<div class="nav-collapse collapse">
						<ul class="nav pull-right">
							<li><a href="#">Welcome back Jonathan! </a></li>
							<li class="dropdown">
								<a class="dropdown-toggle" id="dLabel"  role="button"  data-toggle="dropdown" data-target="#" href="#"><b class="icon-envelope"> </b> Notification <b class="caret"></b></a>
								
								<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
									<li class="divider"></li>
									<?php
										foreach ($notifications as $values) {
									?>
										
										<li class="">
											<?php echo anchor('groups/member/accept?notification_id='.$values -> notification_id.'&amp;user_id='.$values-> user_id.'&amp;group_id='.$values -> group_id, $values -> details);?>
										</li>
										
									<?php
										}
									?>
									<li class="divider"></li>
								</ul>
							</li>
							<li class="dropdown">
								<a class="dropdown-toggle" id="dLabel"  role="button" data-toggle="dropdown" data-target="#" href="#"><b class="icon-cog"> </b> Settings <b class="caret"></b></a>
								 
								<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
									<li class=""><a href="<?php echo base_url('profile')?>"><b class="icon-user"> </b> Profile</a></li>
									<li class=""><a href="<?php echo base_url('profile')?>"><b class="icon-signal"> </b> Account</a></li>
									<li class=""><a href="<?php echo base_url('logout')?>"><b class="icon-off"> </b> Logout</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div style="margin-top: 90px;"></div>
			
			<div class="row">
				<div class="tabbable span12">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tabs1-panel1" data-toggle="tab">Logs</a></li>
					<li><a href="#tabs1-panel2" data-toggle="tab">Groups</a></li>
					<li><a href="#tabs1-panel3" data-toggle="tab">Connection</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tabs1-panel1">
						<h1>Logs Data found here!!!.1.</h1>
					</div>
					<div class="tab-pane " id="tabs1-panel2">
						
						

						<div class="row">
							<div class="tabbable  tabs-left span12">
								<ul class="nav nav-tabs span2">
									<?php 
									$counter = 1;
									$active = "";
									foreach ($groups as $value) {
										
										if($counter==1){
											$active = "active";
										}else{
											$active = "";
										}
									?>
									<li class="<?php echo $active;?>">
										
										<a href="#tabs2-panel<?php echo $counter;?>" data-toggle="tab"><?php echo $value->group_name;?></a>
										<?php
											if($value -> group_manager_id == $value -> user_id){
										?>
										<span class="dropdown pull-right optioness">
												<a href="#" class="dropdown-toggle " data-toggle="dropdown" id="dLabel"  role="button" data-target="#"><b class="icon-pencil "></b></a>
												<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
														<li class="">
															<a href="#update_<?php echo str_replace(' ', '', $value->group_name);?>" role="button" data-toggle="modal"><b class="icon-wrench"> </b> Update</a>
														</li>
														<li class="">
															<a  href="#delete_<?php echo str_replace(' ', '', $value->group_name);?>" role="button" data-toggle="modal"><b class="icon-trash"> </b> Delete</a>
														</li>
												</ul>
										</span>
										<?php
											}
										?>
										
									</li>

									<!-- Update Modal -->
									<div id="update_<?php echo str_replace(' ', '', $value->group_name);?>" class="modal hide fade" >
									  <div class="modal-header">
									    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									    <h3 id="myModalLabel">Update Group</h3>
									  </div>
									  <?php echo form_open('group/update',"class='form-horizontal'");?>
									  <div class="modal-body">
										<div class="control-group">
										    <label class="control-label" >Group Name</label>
										    <div class="controls">
										      	<input type="text" name="groupname" value="<?php echo $value->group_name;?>" placeholder="Group Name"/>
										      	<input type="hidden" name="groupid" value="<?php echo $value -> group_id;?>"/>
										    </div>
									  	</div>
									  	<div class="control-group">
										    <label class="control-label" >Group Description</label>
										    <div class="controls">
										    	<textarea name="group_description" placeholder="Group Description"><?php echo $value -> group_description;?></textarea>
										      	
										    </div>
									  	</div>
									  </div>
									  <div class="modal-footer">
									    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
									    <input type="submit" value="updateGroup" class="btn btn-primary"/>
									  </div>
									  <?php echo form_close();?>
									</div>
									<!-- end update Modal -->

									<!-- Delete Modal -->
									<div id="delete_<?php echo str_replace(' ', '', $value->group_name);?>" class="modal hide fade" >
									  <div class="modal-header">
									    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									    <h3 id="myModalLabel">Delete Group</h3>
									  </div>
									  <?php echo form_open('group/delete',"class='form-horizontal'");?>
									  <div class="modal-body">
										<p>This notice is hereby to confirm deleting this group. Isn't it?</p>
										<p>Are you sure you want to delete <?php echo $value->group_name;?> Group?</p>
										<input type="hidden" name="groupid" value="<?php echo $value -> group_id;?>"/>
										  
									  </div>
									  <div class="modal-footer">
									    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
									    <input type="submit" value="updateGroup" class="btn btn-primary"/>
									  </div>
									  <?php echo form_close();?>
									</div>
									<!-- end Delete Modal -->

									<?php
										
										$counter++;
									}
									?>

									<li class="">
										<a href="#Create" role="button" data-toggle="modal">Create Group <b class="pull-right icon-plus"></b></a>
										<div id="Create" class="modal hide fade" >
										  <div class="modal-header">
										    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										    <h3 id="myModalLabel">Create Group</h3>
										  </div>
										  <?php echo form_open('group/create',"class='form-horizontal'");?>
										  <div class="modal-body">
											<div class="control-group">
											    <label class="control-label" >Group Name</label>
											    <div class="controls">
											      	<input type="text" name="groupname" placeholder="Group Name"/>

											    </div>
										  	</div>
										  	<div class="control-group">
											    <label class="control-label" >Group Description</label>
											    <div class="controls">
											    	<textarea name="groupdescription" placeholder="Group Description"></textarea>
											      	
											    </div>
										  	</div>
										  </div>
										  <div class="modal-footer">
										    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
										    <input type="submit" value="createGroup" class="btn btn-primary"/>
										  </div>
										  <?php echo form_close();?>
										</div>
									</li>



								</ul>
								<div class="tab-content span9">
									<?php
									
										$count = 1;
										$active = "";
										foreach ($groups as $value) {
										
										if($count==1){
											$active = "active";
										}else{
											$active = "";
										}
										?>

										<div class="tab-pane <?php echo $active;?>" id="tabs2-panel<?php echo $count;?>">
											
											<div class="well">
												<div class="row">
													<div class="media span4">
													  <a class="pull-left" href="#">
													    <img class="media-object" src="http://placehold.it/140x130">
													  </a>
													  <div class="media-body">
													    <h4 class="media-heading"><?php echo $value -> group_name;?></h4>
													    <?php echo $value -> group_description;?><br>
													 	
													  </div>

													</div>
												</div>
												
											</div>

											<div class="input-append">
												<?php echo form_open('groups/member/add');?>
											  		<input class="span2" id="appendedInputButtons" type="text" name="email">
											  		<input type="hidden" name="group_id" value="<?php echo $value -> group_id;?>">
											  		<input type="hidden" name="group_name" value="<?php echo $value -> group_name;?>">
											  		<input type="submit" class="btn" value="Add">
											  	<?php echo form_close();?>
											</div>
											
											<div class="member-wrapper" id="members_view<?php echo $value -> group_id;?>">

											</div>
											<script type="text/javascript">
												$("tabs2-panel<?php echo $count;?>").ready(function(){
													get_members(<?php echo $value -> group_id;?>);
													//alert('wew');
												});
											</script>
										</div>

										<?php
											$count++;
										}
									?>									
								</div>
							</div>
						</div>						
						
						
					</div>
					<div class="tab-pane " id="tabs1-panel3">
						<h1>Logs Data found here!!!..3</h1>
					</div>
				</div>
				</div>
			</div>
		
		</div>
		





	</body>
</html>