<?php
class Group extends MX_Controller{

	var $data;

	public function __construct(){
	
		parent::__construct();

		$this -> data = array();
		$this -> load -> model('group_model');
		$this -> load -> library('form_validation');
		$this -> load -> helper('date');
		$this -> load -> helper('url');
	}
	
	public function create_group(){

		if($this -> session -> userdata('is_logged')){

			$this -> form_validation -> set_rules('groupname','Groupname','required|trim');
			$this -> form_validation -> set_rules('groupdescription','Groupdescription','required|trim');
			
			if($this -> form_validation -> run() == FALSE){

				redirect('groups','refresh');
			}else{

				$groupname = $this -> input -> post('groupname');
				$groupdescription = $this -> input -> post('groupdescription');
				$log = $this -> session -> userdata('is_logged');
				$userid = $log['user_id'];

				$this -> group_model -> add_group($groupname,$groupdescription,$userid);
			}
			
			redirect('groups','refresh');

		}else{
			redirect('login','refresh');
		}
		
	}

	public function view_group(){

		if($this -> session -> userdata('is_logged')){


			$log = $this -> session -> userdata('is_logged');
			$userid = $log['user_id'];

			$result = $this -> group_model -> get_groups($userid);
			$notifications = $this -> group_model -> get_notifications($userid);
			// echo "<pre>";
			// print_r($notifications);
			// echo "</pre>";
			$this -> data['notifications'] = $notifications;
			$this -> data['groups'] = $result;
			
			$this -> load -> view("group_view",$this -> data);
	
		}else{
			redirect('login','refresh');
		}
	}

	public function update_group(){

		if($this -> session -> userdata('is_logged')){

			$log = $this -> session -> userdata('is_logged');
			$userid = $log['user_id'];

			$group_id = $this -> input -> post('groupid');
			$group_name = $this -> input -> post('groupname');
			$group_description = $this -> input -> post('group_description');

			$result = $this -> group_model -> update_group($userid, $group_id,$group_description,$group_name);

			redirect('groups','refresh');

		}else{
			redirect('login','refresh');
		}
	}
	
	public function delete_group(){

		if($this -> session -> userdata('is_logged')){

			$log = $this -> session -> userdata('is_logged');
			$userid = $log['user_id'];
			
			$group_id = $this -> input -> post('groupid');
			
			$success = $this -> group_model -> delete_group($group_id, $userid);
			
			redirect('groups','refresh');

		}else{
			redirect('login','refresh');
		}
	}
	

	public function add_group_member(){

		$email = $this -> input -> post("email");
		$group_id = $this -> input -> post("group_id");
		$groupname = $this -> input -> post("group_name");
		

		$data = $this -> group_model -> check_email($email);

		if($data){
			
			$member_id = $data[0] -> user_id;
			$log = $this -> session -> userdata('is_logged');
			$username = $log['username'];
			$exist = $this -> group_model -> check_membership($group_id,$member_id);
			if($exist){
				if($member_id == $log['user_id']){
					echo "fail";
				}else{
					$this -> group_model -> add_group_member($username,$member_id,$email,$groupname,$group_id);
				}
			}else{
				redirect('groups','refresh');
			}
			

			redirect('groups','refresh');
		}
		else{
			echo "false";
		}
	}

	public function confirm_membership(){
		
		$user_id = $this -> input -> get('user_id');
		$group_id = $this -> input -> get('group_id');
		$notification_id = $this -> input -> get('notification_id');

		$accepted = $this -> group_model -> accept_membership($user_id, $group_id,$notification_id);
		if($accepted){
			redirect('groups','refresh');
		}else{
			redirect('groups','refresh');
		}
	}

	public function get_members(){

		$group_id = $this -> input -> post('group_id');
		// $group_id = 1;
		$members = $this -> group_model -> get_members($group_id);

		//$members[]= base_url('application/views/assets/new/uploads/');
		// echo "<pre>";
		// print_r($members);
		// echo "</pre>";

		echo json_encode($members);
	}

	public function testing(){
		echo "ewew";
		$this -> group_model -> check_membership(1,2);
	}

}

