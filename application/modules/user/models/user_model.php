<?php
/*
 * 
 *model for the users registration 
 * 
 * */
class User_model extends CI_Model{
	
	public function __construct(){
		
		parent::__construct();
		
	}
	
	public function insert_user_information($data,$username,$email,$verify,$fb_id){
		
		$userdata = array(
			'nicheez_id' => $data -> user_id,
			'verification_code' => $data -> email_verfication_code,
			'is_verified' => $verify,
			'username' => $username,
			'email' => $email,
			'avatar' => 'green.png',
			'fb_id' => $fb_id
		);
		
		$query = $this->db->insert('users', $userdata);
		
		return $query;
	}
	
	public function activate_account($code, $user_id){
		
		$this->db->set('is_verified', 1); 
		$this->db->where('user_id', $user_id);
		$this->db->where('verification_code', $code);
		$query = $this->db->update('users'); 
		
		return $query;
	}

	public function is_activated($user_id){

		$this -> db -> select();
		$this -> db -> where('user_id',$user_id);

		$query = $this -> db -> get('users');

		return $query -> result();
	}

	public function data_login($nicheez_id){
	
		$query = $this -> db -> get_where('users',array('nicheez_id'=>$nicheez_id));
	
		return $query;
	}
	public function update_image($image,$user_id){
		$data = array(
			'avatar' => $image
		);

		$this -> db -> where('user_id',$user_id);

		$query = $this -> db -> update('users',$data);

		return $query;
	}
}