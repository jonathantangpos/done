<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Activate Account</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php $this -> load -> view('loader/head_loader');?>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".disabled a").click(function () { 
                  $(this).removeAttr("href"); 
                });
              });
        </script>
    </head>
    <body >
        
        <?php $this -> load -> view('commons/dashboard-header');?>

        <div class="body">
            <div class="container">
                <!-- start error alerts -->
               
                <?php 

                    if(validation_errors()){
                        ?>
                        <div class="alert alert-error register-error">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <?php echo validation_errors()?>
                        </div>

                        <?php
                    }else if($message!==""){
                        ?>
                        <div class="alert alert-warning register-error">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <p><?php echo $message;?><p>
                        </div>
                        <?php
                    }else{

                    }

                ?>
                 <!-- end error alerts -->
                <div class="span24 dashboard-wrapper">
                    <div class="dashboard-header-nav">
                        <div class="row-fluid">
                            <div class="span9 offset1 account-options">
                                <ul class="nav nav-pills">
                                  <li class="active disabled"><a href="#confirm_account" data-toggle="tab">Confirm Account</a></li>
                                  <li><img src="<?php echo base_url('application/views/assets/app/img/arrow-right.png');?>"></li>
                                  <li ><a data-toggle="tab">Complete Profile</a></li>
                                  <li><img src="<?php echo base_url('application/views/assets/app/img/arrow-right.png');?>"></li>
                                  <li c><a data-toggle="tab">Upload Image</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="dashboard-container-main tab-content">

                        <!-- confirm account  -->
                        <div class="row-fluid tab-pane fade in active  confirm-account-container" id="confirm_account">
                           <div class="span15 offset2">

                                <?php echo form_open('activate');?>
                                    <h3>Confirm Your Account</h3>
                                    <label>Activation Code</label>
                                    <input type="text" name="verification_code" value="<?php echo set_value('verification_code');?>" placeholder="verification code"><br/>
                                    <input type="submit" value="ACTIVATE">  
                                <?php echo form_close();?>
                           </div>
                        </div>
                        <!-- end confirm account -->

                    </div>
                </div>
            </div>          
        </div>
        <?php echo $this -> load -> view('loader/bottom_loader');?>
    </body> 
</html>