<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Activate Account</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php $this -> load -> view('loader/head_loader');?>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".disabled a").click(function () { 
                  $(this).removeAttr("href"); 
                });
              });
        </script>
        
    </head>
    <body >
        
        <?php $this -> load -> view('commons/dashboard-header');?>

        <div class="body">
            <div class="container">
                <!-- start error alerts -->
               
                <?php 

                    if(validation_errors()){
                        ?>
                        <div class="alert alert-error register-error">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <?php echo validation_errors()?>
                        </div>

                        <?php
                    }else if($message!==""){
                        ?>
                        <div class="alert alert-warning register-error">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <p><?php echo $message;?><p>
                        </div>
                        <?php
                    }else{

                    }

                ?>
                 <!-- end error alerts -->

                <div class="span24 dashboard-wrapper">
                    <div class="dashboard-header-nav">
                        <div class="row-fluid">
                            <div class="span9 offset1 account-options">
                                <ul class="nav nav-pills">
                                  <li ><a data-toggle="tab">Confirm Account</a></li>
                                  <li><img src="<?php echo base_url('application/views/assets/app/img/arrow-right.png');?>"></li>
                                  <li class="active disabled"><a href="#complete_profile" data-toggle="tab">Complete Profile</a></li>
                                  <li><img src="<?php echo base_url('application/views/assets/app/img/arrow-right.png');?>"></li>
                                  <li ><a data-toggle="tab">Upload Image</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="dashboard-container-main tab-content">

                        <script type="text/javascript">
                            $(document).ready(function(){

                                $("#datepicker" ).datepicker();
                            });
                        </script>

                         <!-- complete profile -->
                        <div class="row-fluid tab-pane fade  in active complete-profile-container" id="complete_profile">
                           <div class="span15 offset2">

                                <!-- start update profile form -->
                                <?php echo form_open('profile/update');?>
                                    <h3>Profile Information</h3>
                                    <label>Firstname</label>
                                    <input type="text" placeholder="" name="firstname">
                                    <label>Lastname </label>
                                    <input type="text" placeholder="" name="lastname">
                                    <label>Phone Number</label>
                                    <input type="text" placeholder="" name="phonenumber">
                                    <label>Birthdate</label>
                                    <select class="span3" name="date">
											<?php 
												$z = 1;
												while($z!=12){
											?>
												<option  value="<?php echo $z?>"><?php echo $z;?></option>
											<?php
												$z++;
												}
											?>
											
										 </select>
										 <select class="span3" name="month">
											<?php  
												$y = 1;
												while($y!=31){
											?>
												<option  value="<?php echo $y?>"><?php echo $y;?></option>
											<?php
												$y++;
												}
											?>
											
										 </select>
										 <select class="span3" name="year">
											<?php 
												$x = 2013;
												while($x!=1970){
											?>
												<option  value="<?php echo $x?>"><?php echo $x?></option>
											<?php
												$x--;
												}
											?>
											
										 </select>
                                    <label>Address</label>
                                    <input type="text" placeholder="" name="address"><br/>
                                    <input type="submit" value="Save" >
                               <?php echo form_close();?>
                               <!-- end update profile form -->

                                <?php echo anchor('activate?skip=1','Skip >>');?>
                         </div>
                        </div>
                        <!-- end end complete profile -->


                    </div>
                </div>
            </div>          
        </div>
        <?php echo $this -> load -> view('loader/bottom_loader');?>
    </body> 
</html>