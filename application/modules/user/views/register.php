<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Register</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php $this -> load -> view('loader/head_loader');?>
    </head>
    <body >
        <div class="header">
            <div class="container">
                <div class="row-fluid">
                    <div class="span3 pull-right sign-up">
                        <?php echo anchor('login','LOGIN');?>
                    </div>
                </div>
            </div>
        </div>
        <div class="body">

            <div class="container">

                <!-- start error alerts -->
                
                <?php 

                    if(validation_errors()){
                        ?>
                        <div class="alert alert-error register-error">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <?php echo validation_errors()?>
                        </div>

                        <?php
                    }else if($message !== ""){
                        ?>
                        <div class="alert alert-error register-error">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <?php echo $message;?>
                        </div>
                        <?php
                    }else{

                    }

                ?>
              
                
                 <!-- end error alerts -->
                <div class="span11 offset6">
                    <div class="register-container">
                        <h2>GET STARTED</h2>
                        <!-- login form -->
                        <?php echo form_open("register");?>
                            <input type="text" name="username" value="<?php echo set_value('username');?>" placeholder="Username" />
                            <input type="text" name="email" value="<?php echo set_value('email'); ?>"  placeholder="Email">
                            <input type="password" name="password"  placeholder="Password">
                            <input type="password" name="cpassword" placeholder="Confirm Password">
                            <img class="img-captcha" src="<?php echo $captcha?>"/>
                            <input type="text" name="captcha" class="captcha" placeholder="captcha">
                            <div class="form-footer">
                                <?php echo anchor('login','I already have an account');?>
                                <button type="submit">REGISTER</button>
                            </div>
                        <?php echo form_close();?>
                        <!-- end login form -->

                    </div>
                </div>
            </div>
        </div>
        <?php echo $this -> load -> view('loader/bottom_loader');?>
    </body> 
</html>