<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Activate Account</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php $this -> load -> view('loader/head_loader');?>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".disabled a").click(function () { 
                  $(this).removeAttr("href"); 
                });
              });
        </script>
    </head>
    <body >
        
        <?php $this -> load -> view('commons/dashboard-header');?>

        <div class="body">
            <div class="container">
                <!-- start error alerts -->
               
                <?php 

                    if($message!==""){
                        ?>
                        <div class="alert alert-success register-error">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <p><?php echo $message;?><p>
                        </div>
                        <?php
                        
                    }else{

                    }

                ?>
                 <!-- end error alerts -->
                 
                <div class="span24 dashboard-wrapper">
                    <div class="dashboard-header-nav">
                        <div class="row-fluid">
                            <div class="span9 offset1 account-options">
                                <ul class="nav nav-pills">
                                  <li ><a data-toggle="tab">Confirm Account</a></li>
                                  <li><img src="<?php echo base_url('application/views/assets/app/img/arrow-right.png');?>"></li>
                                  <li ><a  data-toggle="tab">Complete Profile</a></li>
                                  <li><img src="<?php echo base_url('application/views/assets/app/img/arrow-right.png');?>"></li>
                                  <li class="active disabled"><a  data-toggle="tab">Upload Image</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="dashboard-container-main tab-content">

                       

                         <!-- upload image -->
                        <div class="row-fluid tab-pane fade in active  upload-avatar-container" id="upload_image">
                           <div class="span5 offset3">
                                <div class="image-container">
                                    <img src="<?php echo base_url('/application/views/assets/new/uploads/'.$this -> session -> userdata('avatar'));?>">
                                </div>
                                
                            </div>
                            <div class="span7">
                                <!-- start form uploader -->
                                <?php echo form_open_multipart('profile/upload');?>
                                    <div class="upload-container-custom">
                                        <input type="file" name="userfile" size="20" id="BrowserHidden" onchange="getElementById('FileField').value = getElementById('BrowserHidden').value;" >
                                        <div class="container-bot"><input type="text" placeholder="Click to Upload" id="FileField" ></div>
                                        <input type="button" class="btn" value="Upload">
                                    </div>
                                    <input type="submit" value="Save">
                                <?php echo form_close();?>
                                <!-- end form uploader -->


                            </div>
                            <div class="clearfix"></div>
                            <div class="span5 offset2">
                                <?php echo anchor('activate?continue=1','Click here! to continue');?>
                            </div>
                            
                        </div>
                        <!-- end upload image -->


                    </div>
                </div>
            </div>          
        </div>
        <?php echo $this -> load -> view('loader/bottom_loader');?>
    </body> 
</html>