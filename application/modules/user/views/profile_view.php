<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bootstrap</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php $this -> load -> view('loader/head_tag');?>
	</head>
	<body>

		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<button class="btn btn-navbar" type="button" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="brand" href="#">Done System</a>
					<div class="nav-collapse collapse">
						<ul class="nav pull-right">
							<li><a href="#">Welcome back Jonathan! </a></li>
							<li class="dropdown">
								<a class="dropdown-toggle" id="dLabel"  role="button"  data-toggle="dropdown" data-target="#" href="#"><b class="icon-envelope"> </b> Notification <b class="caret"></b></a>
								
								<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
									<li class=""><a href="#">Bryan invited you to join BLUH Group</a></li>
									<li class="divider"></li>
									<li class=""><a href="#">Hara accepted your request.</a></li>
									<li class="divider"></li>
									<li class=""><a href="#">Nino added you in Chugs Group.</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a class="dropdown-toggle" id="dLabel"  role="button" data-toggle="dropdown" data-target="#" href="#"><b class="icon-cog"> </b> Settings <b class="caret"></b></a>
								 
								<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
									<li class=""><a href="<?php echo base_url('profile')?>"><b class="icon-user"> </b> Profile</a></li>
									<li class=""><a href="<?php echo base_url('profile')?>"><b class="icon-signal"> </b> Account</a></li>
									<li class=""><a href="<?php echo base_url('logout')?>"><b class="icon-off"> </b> Logout</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-top: 100px;">
			<div class="container">
				<?php echo anchor('groups','Groups');?>
				<div class="span5">
					<a  href="#image_upload" role="button" data-toggle="modal">
						<img src="<?php echo base_url('/application/views/assets/new/uploads/'.$profile);?>" style="border: 1px solid pink;height: 150px;width: 200px;margin: 0 auto;"><br />
					</a>
					<div id="image_upload" class="modal hide fade" >
						  <div class="modal-header">
						    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						    <h3 id="myModalLabel">Upload Profile Picture</h3>
						  </div>
						  <?php echo form_open_multipart('profile/upload');?>
						  <div class="modal-body offset1">
						  	<img src="<?php echo base_url('/application/views/assets/new/uploads/'.$profile);?>" style="border: 1px solid pink;height: 150px;width: 200px;margin: 0 auto;"><br />
							<input type="file" name="userfile" size="20" />
						  </div>
						  <div class="modal-footer">
						    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
						    <input type="submit" value="Upload" class="btn btn-primary"/>
						  </div>
					  <?php echo form_close();?>
					</div>
					
					<h2>User Profile Infomation</h2>
					<strong>FirstName: </strong>
					<p><?php echo $user_info -> data -> firstname?></p>
					<strong>Lastname: </strong>
					<p><?php echo $user_info -> data -> lastname?></p>
					<strong>Phone Number: </strong>
					<p><?php echo $user_info -> data -> phone_number?></p>
					<strong>Birthdate: </strong>
					<p><?php echo $user_info -> data -> birthdate?></p>
					<strong>Address: </strong>
					<p><?php echo $user_info -> data -> address?></p>
				</div>
				<div class="span6">

					<?php echo validation_errors()?>
					<?php echo form_open("profile/update","class='form-horizontal'");?>
						<fieldset>
							<legend>Update Infomation</legend>
							
							<div class="control-group">
							    <label class="control-label" >FirstName</label>
							    <div class="controls">
							      	<input type="text" value="<?php echo $user_info -> data -> firstname;?>" name="firstname" placeholder="Firstname"/>
							    </div>
						  	</div>
						  	
						  	<div class="control-group">
							    <label class="control-label" >Lastname</label>
							    <div class="controls">
							      	<input type="text" value="<?php echo $user_info -> data -> lastname?>" name="lastname" placeholder="Lastname"/>
							    </div>
						  	</div>

						  	<div class="control-group">
							    <label class="control-label" >Birthdate</label>
							    <div class="controls">
							      	<input type="text" value="<?php echo $user_info -> data -> birthdate?>" name="birthdate" placeholder="Birthdate"/>
							    </div>
						  	</div>
						  	
							<div class="control-group">
							    <label class="control-label" >Phone Number</label>
							    <div class="controls">
							      	<input type="text" value="<?php echo $user_info -> data -> phone_number?>" name="phonenumber" placeholder="Phone Number"/>
							    </div>
						  	</div>
							<!-- CONFIRM PASSWORD -->
							<div class="control-group">
							    <label class="control-label" >Address</label>
							    <div class="controls">
							      	<input type="text" value="<?php echo $user_info -> data -> address?>" name="address" placeholder="Address"/>
							    </div>
						  	</div>
						  	<div class="control-group">
							    <div class="controls">
							      <label class="checkbox">
							        
							      </label>
							      <button type="submit" class="btn">Update</button>
							    </div>
						  	</div>
						</fieldset>
					<?php echo form_close();?>
				</div>
			</div>
		</div>




	<?php echo $this -> load -> view('loader/footer_tags');?>
	</body>
</html>