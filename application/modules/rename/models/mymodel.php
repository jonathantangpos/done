<?php
	class Mymodel extends CI_Model{

		function fetch_logs($group_id, $limit, $offset)
		{
			$select =   array(
							"DATE_FORMAT(l.log_date,'%a , %b %d , %Y') AS LogDate",
							"GROUP_CONCAT(l.log_id) AS LogIDs",
							"GROUP_CONCAT(l.log_content) AS LogContents",
							"GROUP_CONCAT(u.username) AS LogAuthors",
							"GROUP_CONCAT(DATE_FORMAT(l.log_date,'%h:%i %p')) AS LogTimes"
						);
			$this->db->select($select);
			$this->db->from('logs AS l');
			$this->db->join('log_group_ref AS r', 'l.log_id = r.log_id');
			$this->db->join('users AS u', 'l.log_author = u.user_id');
			$this->db->where('r.group_id', $group_id);
			$this->db->group_by('l.log_date');
			$this->db->limit($limit, $offset);
			return $this->db->get()->result_array();
		}  
	}
?>