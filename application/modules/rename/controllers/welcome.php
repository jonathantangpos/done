<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('mymodel');		
	}	
	
	function index()
	{
		$result =   $this->mymodel->fetch_logs(1, 5, 0);
		$new_array = array();
		$inner_array = array();
		$kounter = 0;
		foreach($result as $row){			
			$new_array[] = array(
			'LogDate' => $row['LogDate'],
			'Logs' => array()			
			);
			
			$log_ids = explode(',',$row['LogIDs']);
			$log_contents = explode(',',$row['LogContents']);
			$log_authors = explode(',',$row['LogAuthors']);
			$log_times = explode(',',$row['LogTimes']);
			
			$counter = count($log_ids);
			
			for($i=0; $i<$counter; $i++){
				$inner_array = array(
					'LogID' => $log_ids[$i],
					'LogContent' => $log_contents[$i],
					'LogAuthor' => $log_authors[$i],
					'LogTime' => $log_times[$i]
				);								
				//array_push($new_array, $inner_array);										
				$new_array[$kounter]['Logs'][$i] = $inner_array;
			}	
			$kounter++;
		}
		$data['data'] = $new_array;		
		$this->load->view('my_view',$data);
	}
}
