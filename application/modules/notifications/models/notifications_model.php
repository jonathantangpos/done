<?php

class Notifications_model extends CI_Model{
	
	function __construct(){
		parent::__construct();
	}
	
	function get_notifications($user_id){
		$this -> db -> select();
		$this -> db -> where('user_id',$user_id);
		$this -> db -> where('is_done',0);
		
		$query = $this -> db -> get('notifications');
		
		return $query -> result();
	}
	
	function get_all_nofications($user_id){
		$this -> db -> select();
		$this -> db -> where('user_id',$user_id);
		
		$query = $this -> db -> get('notifications');
		
		return $query -> result();
	}
}

?>