<?php

class Notifications extends MX_Controller{
	var $data;
	public function __construct(){
		parent::__construct();
		$this -> load -> model('notifications_model');
		
		$this -> data = array();
	}
	public function view_notifications(){
		if($this -> session -> userdata('is_logged')){
			
			$log = $this -> session -> userdata('is_logged');
			$userid = $log['user_id'];
			
			$notifications = $this -> notifications_model -> get_notifications($userid);
			$this -> data['notifications'] = $notifications;
			$this -> data['all_notifications'] = $this -> notifications_model -> get_all_nofications($userid);
			$this -> load -> view('notifications',$this -> data,FALSE);
			
			
			
			}else{
			redirect('login','refresh');
		}
	}

	// public function view_all_nofications(){


	// 	if($this -> session -> userdata('is_logged')){
			
	// 		$log = $this -> session -> userdata('is_logged');
	// 		$userid = $log['user_id'];
			
	// 		$notifications = $this -> notifications_model -> get_all_nofications($userid);
	// 		$this -> data['notifications'] = $notifications;
			
	// 		$this -> load -> view('notifications',$this -> data,FALSE);
			
			
			
	// 		}else{
	// 		redirect('login','refresh');
	// 	}


	// }
}
