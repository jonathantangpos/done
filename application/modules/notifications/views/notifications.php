 <!-- notifications -->
 <?php //print_r($all_notifications);?>
                        <div class="row-fluid tab-pane fade in active  notification-container" id="notifications">
                            <div class="span24">
                                <div class="filter row-fluid" style="margin-top: 20px;">
                                    <ul class="nav nav-pills offset4">
                                      <li class="active"><a href="#unread"  data-toggle="tab" >Unread Notifications </a></li>
                                      <li ><a href="#all"  data-toggle="tab">All Notifications</a></li>
                                    </ul>
                                </div>


                                 <div class="tab-content">
                                     <div class="row-fluid tab-pane fade in active " id="unread">
                                         <div class="span16 offset4">
                                            <table class="table table-striped ">
                                               
                                                <?php foreach($notifications as $value):?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $value -> details;?>
                                                            <p class="pull-right"><?php echo anchor('groups/member/accept?notification_id='.$value -> notification_id.'&amp;user_id='.$value -> user_id.'&amp;group_id='.$value -> group_id," Accept ");?><a href="#">Decline</a></p>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            </table>
                                        </div>

                                    </div>

                                     

                                    <div class="row-fluid tab-pane fade" id="all">
                                        <div class="span16 offset4">
                                            <table class="table table-striped ">
                                               
                                                <?php foreach($all_notifications as $values):?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $values -> details;?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            </table>
                                        </div> 
                                    </div>

                                </div>


                            </div>
                           
                        </div>
                        <!-- end notifications -->