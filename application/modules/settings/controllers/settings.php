<?php

class Settings extends MX_Controller{
	
	var $data;
	public function __construct(){
		parent::__construct();
		$this -> load -> model('user/user_model');
		$this->load->library('rest_client/curl');
		
		$this -> data = array();
	}
	public function account(){


		$user_info =  $this->curl->init()->cookie()->post('users/view_profile');
	 	$this-> data['user_info'] = json_decode($user_info);
	 	
		//$this -> load -> view("profile_view",);


		return $this -> load -> view('settings',$this -> data,FALSE);
	}
	public function upload_image(){

		if($this -> session -> userdata('is_logged')){


			$config['upload_path'] = './application/views/assets/new/uploads/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '10000';
			$config['max_width']  = '1024';
			$config['max_height']  = '768';

			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload())
			{
				$error = array('error' => $this->upload->display_errors());
				echo "<pre>";
				print_r($error);
				echo "</pre>";
			}
			else
			{
				$user = $this -> session -> userdata('is_logged');
				$user_id = $user['user_id'];
				$data = $this->upload->data();
				// echo "<pre>";
				// print_r($data['file_name']);
				// echo "</pre>";
				$this -> session -> unset_userdata('avatar');
				$this -> session -> set_userdata('avatar',$data['file_name']);
				$image = $data['file_name'];
				$this -> user_model ->  update_image($image,$user_id);

				//used to stay on same tab
				$this->session->set_userdata('sess_tag', 'settings');

				redirect('dashboard?p=settings','refresh');

				
			}

		}else{
			//used to stay on same tab
			$this->session->set_userdata('sess_tag', 'settings');

			redirect('dashboard?p=settings','refresh');

		}

	}

	public function update_profile(){


		$firstname = $this -> input -> post('firstname');
		$lastname = $this -> input -> post('lastname');
		$date =  $this -> input -> post('date');
		$month =  $this -> input -> post('month');
		$year =  $this -> input -> post('year');
		$phone_number = $this -> input -> post('phonenumber');
		$address = $this -> input -> post('address');
		$birthdate = "$date/$month/$year";
		$parameters =  array(
			'firstname'	=>  $firstname,
			'lastname'	=>  $lastname,
			'birthdate'	=>  $birthdate,
			'phone_number'	=>  $phone_number,
			'address'	=>  $address
        );
		
		$user_info =  $this->curl->init()->cookie()->post('users/update_profile',$parameters);
		
		// if($user_info){

		// }

		$user_info = json_decode($user_info);


		//used to stay on same tab
		$this->session->set_userdata('sess_tag', 'settings');
		
		redirect('dashboard');

		echo $user_info -> error;
		echo "<pre>";
		print_r($user_info);
		echo "</pre>";
		// redirect('profile','refresh');

	}


	public function change_password(){
		
	
		$this -> form_validation -> set_rules('c-pword','Password','required|trim');
		$this -> form_validation -> set_rules('n-pword','Confirm Password','required|trim|matches[r-pword]');
		$this -> form_validation -> set_rules('r-pword','Confirm Password','required|trim');
		
		if($this -> form_validation -> run() == FALSE){
			$this->session->set_userdata('sess_tag', 'settings');
			redirect('dashboard?err=2');
			
		}else{
			$cpword = $this -> input -> post('c-pword');
			$npword = $this -> input -> post('n-pword');
			$rpword = $this -> input -> post('r-pword');
			
			if($npword==$rpword){
				$pwords =  array(
					'currentpassword'	=>  $cpword,
					'newpassword'	=>  $npword
				);
			
				$ret = $this->curl->init()->cookie()->post('users/change_password',$pwords);
				$ret = json_decode($ret);
				
				if($ret->error==0){
					redirect('dashboard?err=3');
				}else{
					redirect('dashboard?err=2');
				}
				
				
			}else{
				$this->session->set_userdata('sess_tag', 'settings');
				redirect('dashboard?err=1');
			}
			
		}
	
	
		$this->session->set_userdata('sess_tag', 'settings');
	}
}
