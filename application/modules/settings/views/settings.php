				
                        <div class="row-fluid tab-pane fade in active settings-container" id="settings">
                            <div class="span5  dashboard-container-left">
                                <ul class="nav nav-pills nav-stacked groups-left-nav">
                                  <li class="active"><a href="#update_profile" data-toggle="tab">Update Profile</a></li>
                                  <li><a href="#update_avatar" data-toggle="tab">Update Avatar</a></li>
                                  <li><a href="#change-password" data-toggle="modal" >Change Password</a></li>
                                </ul>
                                 <!-- change password Modal -->
                                    <div id="change-password" class="modal hide fade" >
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h3 id="myModalLabel">Change Password</h3>
                                      </div>
                                     
									<?php echo form_open('settings/change_password','class="form-horizontal"');?>
                                      <div class="modal-body">
                                        <div class="control-group">
                                            <label class="control-label" >Current Password</label>
                                            <div class="controls">
                                                <input type="password" name="c-pword"  placeholder="Current Password"/>
                                                
                                            </div>
                                        </div>
                                         <div class="control-group">
                                            <label class="control-label" >New Password</label>
                                            <div class="controls">
                                                <input type="password" name="n-pword"  placeholder="New Password"/>
                                                
                                            </div>
                                        </div>
                                       <div class="control-group">
                                            <label class="control-label" >Confirm Password</label>
                                            <div class="controls">
                                                <input type="password" name="r-pword"  placeholder="Confirm Password"/>
                                            </div>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                        <input type="submit" value="Save" class="btn btn-primary"/>
                                      </div>
                                      </form>
                                    </div>
                                <!-- change password Modal -->
                            </div>
                            <div class="span19 dashboard-container-right">
                                <div class="tab-content">
                                  <div class="tab-pane fade in active" id="update_profile">
                                    <!-- start update profile form -->
                                     <?php echo form_open('settings/update','class="update-profile-container"');?>
                                         <h3>Profile Information</h3>
                                         <label>Firstname</label>
                                         <input type="text" placeholder="<?php echo $user_info->data->firstname?>" value="<?php echo $user_info->data->firstname?>" name="firstname">
                                         <label>Lastname </label>
                                         <input type="text" placeholder="<?php echo $user_info->data->lastname?>" value="<?php echo $user_info->data->lastname?>" name="lastname">
                                         <label>Phone Number</label>
                                         <input type="text" placeholder="<?php echo $user_info->data->phone_number?>" value="<?php echo $user_info->data->phone_number?>" name="phonenumber">
                                         <label>Birthdate</label>
										 <?php 
											$day = $user_info->data->birthdate;
											$bday = explode('/',$day);
											//echo $bday[0];
											//print_r($bday);
										 
										 ?>
										 <select class="span3" name="date">
											<?php 
												$z = 1;
												while($z!=12){
											?>
												<option <?php echo ($bday[0]==$z)? "selected=selected": "";?> value="<?php echo $z?>"><?php echo $z;?></option>
											<?php
												$z++;
												}
											?>
											
										 </select>
										 <select class="span3" name="month">
											<?php  
												$y = 1;
												while($y!=31){
											?>
												<option <?php echo ($bday[1]==$y)? "selected=selected": "";?> value="<?php echo $y?>"><?php echo $y;?></option>
											<?php
												$y++;
												}
											?>
											
										 </select>
										 <select class="span3" name="year">
											<?php 
												$x = 2013;
												while($x!=1970){
											?>
												<option <?php echo ($bday[2]==$x)? "selected=selected": "";?> value="<?php echo $x?>"><?php echo $x?></option>
											<?php
												$x--;
												}
											?>
											
										 </select>
                                         
                                         <label>Address</label>
                                         <input type="text" placeholder="<?php echo $user_info->data->address?>" value="<?php echo $user_info->data->address?>" name="address"><br/>
                                         <input type="submit" value="Save" >
                                    <?php echo form_close();?>
                                    <!-- end update profile form -->
                                  </div>
                                  <div class="tab-pane fade upload-avatar-container" id="update_avatar">
                                      
                                      <div class="row-fluid">

                                         <div class="span5">
                                            <div class="image-container">
                                                <img src="<?php echo base_url('/application/views/assets/new/uploads/'.$this -> session -> userdata('avatar'));?>">
                                            </div>
                                            
                                        </div>
                                        <div class="span7 offset3">
                                            <!-- start form uploader -->
                                            <?php echo form_open_multipart('settings/upload');?>
                                                <div class="upload-container-custom">
                                                    <input type="file" name="userfile" size="20" id="BrowserHidden" onchange="getElementById('FileField').value = getElementById('BrowserHidden').value;" >
                                                    <div class="container-bot"><input type="text" placeholder="Click to Upload" id="FileField" ></div>
                                                    <input type="button" class="btn" value="Upload">
                                                </div>
                                                <input type="submit" value="Save">
                                            <?php echo form_close();?>
                                            <!-- end form uploader -->


                                        </div>

                                      </div>
                                       

                                  </div>
                                </div>
                            </div>
                        </div>