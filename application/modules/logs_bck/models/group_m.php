<?php
class Group_m extends CI_Model{
	function fetch_groups($user_id){
		$this->db->select('g.group_id, g.group_name');
		$this->db->from('groups AS g');
		$this->db->join('members AS m', 'm.group_id = g.group_id');
		$this->db->where('m.user_id', $user_id);
		return $this->db->get()->result_array();
	}
}
