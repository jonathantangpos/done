<?php
	class Log_m extends CI_Model{
		
		function __construct(){
			parent::__construct();
		}	
		
		function save_log($data){
			$myArray = array(
					'log_id' => '',
					'log_content' => $data['log_content'],					
					'log_date' => date('Y-m-d H:i:s'),					
					'log_author' => $data['user_id']
					);
			
			$this->db->insert('logs', $myArray);
			
			return $this->db->insert_id();
			
		}
		
		function save_attachment($data){			
			$myArray = array();
			$size_count = 0;			
			foreach($data['file_name'] as $key){
				$myArray[] = array(
							'attachment_id' => '',
							'log_id' => $data['last_id'],
							'file_size' => $data['file_size'][$size_count],
							'file_name' => $key
							);
							
							$size_count++;
			}
			
			$this->db->insert_batch('attachments', $myArray);
			
		}
		
		function save_log_group_ref($data){
			$myArray = array(
					'log_id' => $data['last_id'],
					'group_id' => $data['group_id']
					);
			$this->db->insert('log_group_ref', $myArray);
		}		
		
		function fetch_logs($group_id, $offset, $origin){
			$select =   array(
							"DATE_FORMAT(l.log_date, '%W , %M %d , %Y') AS log_date",
							"GROUP_CONCAT(l.log_id SEPARATOR 0x1D) AS log_ids",
							"GROUP_CONCAT(l.log_content SEPARATOR 0x1D) AS log_contents",
							"GROUP_CONCAT(u.username SEPARATOR 0x1D ) AS log_authors",
							"GROUP_CONCAT(DATE_FORMAT(l.log_date,'%l:%i %p') SEPARATOR 0x1D) AS log_times"
						);
			$this->db->select($select);
			$this->db->from('logs AS l');
			$this->db->join('log_group_ref AS r', 'l.log_id = r.log_id');
			$this->db->join('users AS u', 'l.log_author = u.user_id');
			$this->db->where('r.group_id', $group_id);
			$this->db->group_by('DATE(l.log_date)');	// To group by Date only, excluding time from l.log_date		
			$this->db->order_by('DATE(l.log_date)', 'DESC'); // Show earliest logs from DB
			$this->db->limit($offset, $origin);
			return $this->db->get()->result_array();
		}
		
		function fetch_attachments($log_id){
			$this->db->select('file_name');
			$this->db->from('attachments');
			$this->db->where('log_id', $log_id);
			return $this->db->get()->result_array();			
		}
		
		function save_comments($data){
			$myArray = array(
					'comment_id' => '',
					'log_id' => $data['log_id'],
					'comment_author' => $data['user_id'],
					'comment_content' => $data['comment'],
					'comment_date' => date('Y-m-d H:i:s')
					);
			$this->db->insert('comments', $myArray);
		}
		
		function fetch_comments($log_id){
			$this->db->select('*, u.username');
			$this->db->from('comments AS c');
			$this->db->join('users AS u', 'c.comment_author = u.user_id');
			$this->db->where('c.log_id', $log_id);
			return $this->db->get()->result_array();
		}
				
	}
	
?>