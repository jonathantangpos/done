<?php
class View extends MX_Controller{
	
	function __construct(){
		parent::__construct();
		
		$this->load->model('group_m');
		$this->load->model('log_m');
		$this->load->model('global_m');
		$this->load->helper('date');
	}
	function test(){
		if($this->session->userdata('is_logged')){
			
			$user_data = $this->session->userdata('is_logged');
			
			$data['groups'] = $this->group_m->fetch_groups($user_data['user_id']);
			$data['avatar'] = $this->session->userdata('avatar');
			
			//$data['logs_v'] = $this->render_view('logs_v', $data); 		
			echo $this->render_view('view_logs_v', $data);
			
			}else{			
			redirect('login','refresh');			
		}		
	}
	function index(){
		
		if($this->session->userdata('is_logged')){
			
			$user_data = $this->session->userdata('is_logged');
			
			$data['groups'] = $this->group_m->fetch_groups($user_data['user_id']);
			$data['avatar'] = $this->session->userdata('avatar');
			
			//$data['logs_v'] = $this->render_view('logs_v', $data); 		
			echo $this->render_view('logs', $data);
			
		}else{			
			redirect('login','refresh');			
		}		
	}
	
	/* Returns partial view of logs_v*/
	
	function render_view($view, $data){	
	
		return $this->load->view($view, $data, true);
		
	}
		
	
	/*
	 * @name : display_logs()
	 * @param : $group_id, $more_logs
	 * @return : json
	 * */
	
	function display_logs(){		
		
		$group_id = $this->input->post('group_id'); //get group_id from post
		$more_logs = (int)$this->input->post('more_logs'); //get more_logs from post
		
		$s_origin = $this->session->userdata('s_origin');
		$s_offset = $this->session->userdata('s_offset');		
		$s_group_id = $this->session->userdata('s_group_id');					
		
		list($origin, $offset) = $this->arr_limits($group_id, $s_group_id, $more_logs, $s_origin, $s_offset);
		
		$result =   $this->log_m->fetch_logs($group_id, $offset, $origin);
				
		$data['debug'] = $more_logs;
		
		$new_array = array();
		$inner_array = array();
		$result_counter = 0; //refers to each result query, or total number of rows of $result
		foreach($result as $row){
			$new_array[] = array(
					'log_date' => $row['log_date'],
					'logs' => array()
			);
				
			$log_ids = explode(chr(0x1D),$row['log_ids']); //Used chr(0x1D) to split concatenated string from query using 0x1D as separator
			$log_contents = explode(chr(0x1D),$row['log_contents']);
			$log_authors = explode(chr(0x1D),$row['log_authors']);
			$log_times = explode(chr(0x1D),$row['log_times']);
				
			$log_counter = count($log_ids); //refers to logs in each day / week / month
				
			for($i=0; $i<$log_counter; $i++){
				$inner_array = array(
						'log_id' => $log_ids[$i],
						'log_content' => $log_contents[$i],
						'log_author' => $log_authors[$i],
						'log_time' => $log_times[$i]
				);
				$new_array[$result_counter]['logs'][$i] = $inner_array; //push new array to Logs array
			}
			$result_counter++;
		}
		$data['logs'] = $new_array;
		
		$data['logs_v'] = $this->render_view('logs_v', $data);						
		
		echo json_encode($data);
	}
	
	/*
	 * @name  : arr_limits
	 * @return  : $origin, $offset in array format
	 * @param  : $group_id, $s_group_id, $more_logs, $s_origin, $s_offset
	 * 
	 * */

	function arr_limits($group_id, $s_group_id, $more_logs, $s_origin, $s_offset){
		
		if($s_group_id){
			
			if($group_id == $s_group_id && $more_logs == 0){
				$origin = $s_origin + 5;
				$offset = $s_offset + 5;
				
				$this->session->set_userdata('s_origin', $origin);
				$this->session->set_userdata('s_offset', $offset);
				$this->session->set_userdata('s_group_id', $group_id);
				
				$data['debug'] = "Same group";
			}else{
				$origin = 0;
				$offset = 5;
				
				//$this->session->unset_userdata('s_car_identifier'); //to clear numbering of $car_identifier and start again to 0
				$this->session->set_userdata('s_origin', $origin);
				$this->session->set_userdata('s_offset', $offset);
				$this->session->set_userdata('s_group_id', $group_id);

				$data['debug'] = "New group";
			}						
			
		}else{
			$origin = 0;
			$offset = 5;
			
			//$this->session->unset_userdata('s_car_identifier'); //to clear numbering of $car_identifier and start again to 0
			$this->session->set_userdata('s_group_id', $group_id);
			$this->session->set_userdata('s_origin', 0);
			$this->session->set_userdata('s_offset', 5);
			
			$data['debug'] = 'First Load';
		}
		
		return array($origin, $offset);
	}
	
	
	function include_attachments(){
		/*get parameters send by modules::run*/
		$data['log_id'] = func_get_args();
		
		$data['attachments'] = $this->log_m->fetch_attachments($data['log_id'][0]);
				
		$this->load->view('attachments_v', $data);
		
	}
	
	function include_comments(){
		/*get parameters send by modules::run*/
		$data['log_id'] = func_get_args();		
		$data['comments'] = $this->log_m->fetch_comments($data['log_id'][0]);			
		$this->load->view('comments_v', $data);
	
	}
	
	function save_comment(){
		
		$log = $this->session->userdata('is_logged');
		
		$data['log_id'] = $this->input->post('log_id');
		$data['comment'] = $this->input->post('comment');
		$data['user_id'] = $log['user_id'];
				
		$this->log_m->save_comments($data);
		
		//$data['comments'] = $this->log_m->fetch_comments($data['log_id']);
		echo json_encode($data);
	}	
	
}
?>