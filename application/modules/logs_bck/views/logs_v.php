<div id="logs" class="tab-pane active">
	<?php 

		$s_car_identifier = $this->session->userdata('s_car_identifier');
		
		if($s_car_identifier){
			$car_identifier = $s_car_identifier;									
		}else{
			$car_identifier = 0;			
		}
		
		if(count($logs) > 0 ){
			$car_counter = 0;
			foreach ($logs as $row){
				echo "<div class='row-fluid' style='margin-top: 10px;'>";
					echo "<div class='span4'>".$row['log_date']."</div>";
					echo "<div class='span20' id='log_row'>";
						echo "<div id='myCarousel_".$car_identifier."' class='carousel slide'>"; //find a way to make each of this unique from each other
							echo "<div class='carousel-inner'>";
							$display = 6;
							$logs_count = count($row['logs']);
							$car_items = ceil($logs_count / $display);
							
							$slices = array_chunk($row['logs'], $display);
							for($i = 0; $i < $car_items; $i++){
								echo "<div class='item'><div class='row-fluid'>";
								foreach($slices[$i] as $key){
									echo "<div class='span4 logs' data-id='".$key['log_id']."'>";
									echo "<h5>".$key['log_author']." &bull; <em>".$key['log_time']."</em></h5>";
									echo "<p>".$key['log_content']."</p>";
									echo "</div>";
								}
								echo "</div></div>";
							}															
							echo "</div>";
							echo "<a class='carousel-control left' href='#myCarousel_".$car_identifier."' data-slide='prev'>&lsaquo;</a>";
							echo "<a class='carousel-control right' href='#myCarousel_".$car_identifier."' data-slide='next'>&rsaquo;</a>";
						echo "</div>";						
					echo "</div>";
				echo "</div>";
				$car_identifier++;				
			}
		}else{
			
			//$this->session->unset_userdata('s_group_id');	
			
			echo "<div class='item'><div class='row-fluid'>";
			echo "<div class='span24 logs'><h4 style='text-align: center;'>This group has no logs.</h4></div>";
			echo "</div></div>";			

		}
		$this->session->set_userdata('s_car_identifier', $car_identifier);
	?>
		
</div>

<script type="text/javascript">
	$(function(){		
		
		$(".span3").css({"height" : $(".span16").height() + "px"});
				
		$("#more_logs_btn").css({"width" : $(".span19").width() + "px"});

		/* Make first item in tab active*/		
		$(".carousel-inner").each(function(){
			$(this).children('div:first').attr("class", "active item");
			
		});
			
		/* Disable auto-scrolling of carousel */
		$(".carousel").carousel({
			interval : false
		});	
				
		
		/*
		 * Save Comment
		 * 
		 * */

		$("input#comment_button").click(function(e){
			var form = e.target.form;
			e.preventDefault();
			$.ajax({
				type : "POST",
				dataType : "json",
				url : "<?php echo base_url()?>logs/view/save_comment",						
				data : $(form).serialize(),
				success : function(data){
					console.debug(data);
					var f = $(form).find("textarea").val("");
					hide_comment_button(f);
					var new_comment = "<div class='comment'><p>"+data["comment"]+" | "+data["user_id"]+"</p></div>";
					$(new_comment).insertBefore(form);				
					},
				error : function(data){
					console.log("Error: "+data);
					}
				});
		});
		});

	/* Show/Hide comments */			
	$("input#show_comments").click(function(){
		if($(this).val() == "Show Comments"){
			$(this).siblings("div#comment_container").show(400);
			$(this).attr("value", "Hide Comments");
		}else{
			$(this).siblings("div#comment_container").hide(400);
			$(this).attr("value", "Show Comments");
			}
	});
	
	function hide_comment_button(e){
		var val = $(e).val();
		if(!val){				
			$(e).siblings("#comment_button").css("display", "none");
		}else{			
			$(e).siblings("#comment_button").css("display", "inline");
			}	
	}
	function show_comment_button(e){		
		$(e).siblings("#comment_button").css("display", "inline");
	}

</script>