
                        <!-- logs -->
                        <div class="row-fluid tab-pane fade in active log-container" id="logs1">
							<div class="span5  dashboard-container-left">
								<div class="search-container"><input type="text" placeholder="Search"></div>
								<ul class="nav nav-pills nav-stacked groups-left-nav" id="tabs">
									<?php							
										foreach($groups as $key){								
											echo "<li><a onclick='show_logs(".$key['group_id'].")' data-group='".$key['group_id']."' data-toggle='tab'>".$key['group_name']."</a></li>";
										}
									?>
								</ul>
							</div>
							
							<div class="span19 dashboard-container-right">
								
								<div class="row-fluid form-container">
									<div class="span4 date pull-left">
										<h2>28</h2>
										<ul>
											<li>05</li>
											<li>2012</li>
										</ul>
									</div>
									<div class="span20 log-area">										
										<?php echo form_open('logs/add/share', 'id="logs_form" class="span22"'); ?>
											<?php
												$ta_attr = array(
												'name' => 'log_content',												
												'id' => 'log_content',
												'cols' => 0,
												'rows' => 0,
												'class' => 'well'
												);
												echo form_textarea($ta_attr);			
											?>	
											<input type="hidden" name="group_id" id="group_id" value="1"> <!-- Acquires val from click of grouptab -->		
											<input type="submit" value="Share" class="btn" id="share_btn">
											<span id="uploader" class="btn">+ Attachment</span>
											<span id="upload_msg"></span>
											<span id="upload_size" style="display: none;"></span>
										<?php echo form_close();?>
									</div>
									
								</div>
																							
								<hr style="border-top: 1px solid #ccc;"/>
								<div class="filters"><p>Sort by: <span><a href="#">Day</a></span><span><a href="#">Week</a></span><span><a href="#">Month</a></span></p></div>
								<hr style="border-top: 1px solid #ccc;"/ >
									
								<div class="tab-content" id="logs_content">
									<!--logs loaded via ajax call-->
								</div>
								<div class="row-fluid" id="more_logs">
									<div class="span24">			
										<button class="btn btn-primary" id="more_logs_btn"><i class="icon-white icon-chevron-down"></i> More Logs</button>						
									</div>
								</div>
							</div>
						</div>
                        <!-- end logs -->
						


                       

                    </div>
                </div>
            </div>          
        </div>
		<script type="text/javascript">									
			
			
			/*Load first tab contents on page load*/
			$(document).ready(function(){
				
				first_load();						
				upload();
				
			});
			
			/*
			 * Display more logs through arrow down button
			 * 
			 * */
			
			$("#more_logs_btn").click(function(){
				
				var group_id = ($("#tabs").children(".active").children().attr("data-group"));
				console.log(group_id);
				$.ajax({
					type : "POST",
					dataType: "json",
					url : "<?=base_url()?>logs/view/display_logs",					
					//data : "&group_id=" + group_id + "&more_logs=true",	
					data : {group_id : group_id, more_logs : 0},						
					success : function (data){	
						$("#logs_content").append(data.logs_v);//logs_v
						//$(data.debug).hide().appendTo($("div.tab-content")).slideDown("slow"); //some animation test
					},
					error : function(data){
						console.log(data);
					}
				});
			});
			
			function show_logs(group_id){			
				$('#group_id').val(group_id); //adds value to hidden field in add_log form to determine which group to share
				
				$.ajax({
					type : "POST",
					dataType: "json",
					url : "<?=base_url()?>logs/view/display_logs",					
					//data : "&group_id=" + group_id + "&more_logs=false",
					data : {group_id : group_id, more_logs : 1},
					success : function (data){	
						$("div#logs").remove();						
						$("#logs_content").append(data.logs_v);//logs_v				
					},
					error : function(data){
						console.log(data);
					}
				});
				
			}
			
			function first_load(){
				/* Make first item in vertical tab active*/
				$("#tabs li:first").attr("class", "active");
				
				$.ajax({
					type : "POST",
					dataType: "json",
					url : "<?=base_url()?>logs/view/display_logs",					
					//data : "&group_id=" + <?=$groups[0]['group_id']?> + "&more_logs=false", //takes first group
					data : {group_id : <?=$groups[0]['group_id']?>, more_logs : 1},					
					success : function (data){							
						$("#logs_content").append(data.logs_v); //logs_v
					}
				});
				
			}
			
			function upload(){
				/*Uploader*/
				$fub = $('#uploader');
				$msg = $('#upload_msg');
				
				var uploader = new qq.FileUploaderBasic({
					button : $fub[0], 
					debug : true,
					action : 'logs/add/attach',		
					allowedExtensions : ['jpeg', 'jpg', 'png', 'bmp', 'doc', 'docx', 'xls', 'xlsx'],
					sizeLimit : 2097152, //2MB
					onSubmit: function(id, fileName) {
						$msg.append('<input type="checkbox" id="file-' + id + '" style="padding-left: 10px" name="file_name[]" checked value="' + fileName + '">' + '<a href="../uploads/'+fileName+'">'+fileName+'</a>');
					},					
					onComplete : function(id, fileName, responseJSON){
						if(responseJSON.success){					
							$('#upload_size').append('<input type="checkbox" id="file-' + id + '" name="file_size[]" checked value="' + responseJSON.size/1024 + ' Kb">');					
							$('#file-' + id).html(fileName);										
							}else{
							console.log(responseJSON.error);
						}
					}
					
				});
				
			}
			
		</script>
      