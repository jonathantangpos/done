<div id="logs" class="tab-pane active">
	<div class="row">
		<div class="span3 date" style="-webkit-transform: rotate(-90deg);">
			<h3 style="margin-top: 90%;">Date</h3>			
		</div>
		<div class="span16">
			<div id="myCarousel" class="carousel slide">
				<div class="carousel-inner">
					<?php
						if(count($logs) > 0){
							$display = 4;
							$log_count = count($logs);
							$batch = ceil($log_count / $display);
							
							
							$slices = array_chunk($logs, $display);
							for($i=0; $i<$batch; $i++){
								echo "<div class='item'><div class='row'>";
								foreach ($slices[$i] as $row){
									echo "<div class='span4 logs' data-id='".$row['log_id']."'><div class='media'>";
									echo "<a class='pull-left' href='#'><img class='media-object' id='log_avatar' src='".load_global_assets('new/uploads/'.$row['avatar'].'')."' /></a>";
									//echo "<div class='media-body'>";									
									echo "<h5 class='media-heading'>".$row['username']."</h5>";
									echo "<p>".$row['log_content']."</p>";																		
									echo "</div></div>";
									
									/* Modal Dialog start*/
									
									echo "<div id='myModal_".$row['log_id']."' class='modal hide fade' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>";
									echo "<div class='modal-header'>";
									echo "<button class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
									echo "<h3 id='myModalLabel'>".$row['log_date']."</h3>";
									echo "</div>";
									echo "<div class='modal-body'>";
									echo "<p>".$row['log_content']."</p>";
									echo modules::run('logs/view/include_attachments', $row['log_id']);
									echo "<input type='button' class='btn btn-mini' id='show_comments' value='Show Comments'></input>";
									echo modules::run('logs/view/include_comments', $row['log_id']);
									echo "</div>";
									echo "<div class='modal-footer'>";
									echo "<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>Close</button>";
									echo "</div>";
									echo "</div>";
								}
								echo "</div></div>";
								
								
								
							}					
						}else{
							echo "<div class='item'><div class='row'>";
							echo "<div class='span16 logs'>This group has no logs.</div>";
							echo "</div></div>";
						}
					
						
					?>									
				</div>
				<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
		    	<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
			</div>
		</div>
	</div>
	
		

	
</div>

<script type="text/javascript">
	$(function(){

		/*Make logs height equal to width*/
		var logw = $(".logs").width();
		$(".logs").css({"height" : logw + "px"});
		$(".date").css({"height" : logw + "px"});

		/* Lunch modal on specific Log*/
		$("div.logs").click(function(){
			var id = $(this).attr("data-id");
			$('#myModal_'+id).modal();			
			});
		
		/* Disable auto-scrolling of carousel */
		$(".carousel").carousel({
			interval : false
			});
		
		/* Make first item in tab active*/
		$(".carousel-inner div:first").attr("class", "active item");


		$("input#comment_button").click(function(e){
			var form = e.target.form;
			e.preventDefault();
			$.ajax({
				type : "POST",
				dataType : "json",
				url : "<?php echo base_url()?>logs/view/save_comment",						
				data : $(form).serialize(),
				success : function(data){
					console.debug(data);
					var f = $(form).find("textarea").val("");
					hide_comment_button(f);
					var new_comment = "<div class='comment'><p>"+data["comment"]+" | "+data["user_id"]+"</p></div>";
					$(new_comment).insertBefore(form);				
					},
				error : function(data){
					console.log("Oops! Something went wrong!");
					}
				});
		});
		});

	/* Show/Hide comments */			
	$("input#show_comments").click(function(){
		if($(this).val() == "Show Comments"){
			$(this).siblings("div#comment_container").show(400);
			$(this).attr("value", "Hide Comments");
		}else{
			$(this).siblings("div#comment_container").hide(400);
			$(this).attr("value", "Show Comments");
			}
	});
	
	function hide_comment_button(e){
		var val = $(e).val();
		if(!val){				
			$(e).siblings("#comment_button").css("display", "none");
		}else{			
			$(e).siblings("#comment_button").css("display", "inline");
			}	
	}
	function show_comment_button(e){		
		$(e).siblings("#comment_button").css("display", "inline");
	}

</script>