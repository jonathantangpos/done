<div class="row" id="share_box">
	<div class="span4 offset4">
		<img id="share_box_avatar" src="<?php echo load_global_assets('new/uploads/'.$avatar.'');?>" />
	</div>
	<?php echo form_open('logs/add/share', 'id="logs_form"'); ?>
	<div class="span12">
		<?php
			$ta_attr = array(
					'name' => 'log_content',
					'rows' => 3,
					'cols' => 20,
					'id' => 'log_content'
					);
			echo form_textarea($ta_attr);			
		?>	
		<input type="hidden" name="group_id" id="group_id" value="1"> <!-- Acquires val from click of grouptab -->		
		<input type="submit" value="Share" class="btn" id="share_btn">
		<span id="uploader" class="btn">+ Attachment</span>
		<span id="upload_msg"></span>
		<span id="upload_size" style="display: none;"></span>
	</div>
	<?php echo form_close();?>	
</div>
<script>
	$(document).ready(function(){
		/*Position Share box and avatar*/
		$('#log_content').width($('.span12').width());
		
		
		var share_box_avatar_dim = $('.span12').height();
		$('#share_box_avatar').height(share_box_avatar_dim);
		$('#share_box_avatar').width(share_box_avatar_dim);
		
		$('#share_btn').click(function(e){
			if(!$('#group_id').val()){
				e.preventDefacult();
				console.log("Select a group to share!");
			}
			
		});
		
		/*Uploader*/
		$fub = $('#uploader');
		$msg = $('#upload_msg');
		
		var uploader = new qq.FileUploaderBasic({
			button : $fub[0], 
			debug : true,
			action : '/logs/add/attach',		
			allowedExtensions : ['jpeg', 'jpg', 'png', 'bmp', 'doc', 'docx', 'xls', 'xlsx'],
			sizeLimit : 2097152, //2MB
			onSubmit: function(id, fileName) {
				$msg.append('<input type="checkbox" id="file-' + id + '" style="padding-left: 10px" name="file_name[]" checked value="' + fileName + '">' + '<a href="../uploads/'+fileName+'">'+fileName+'</a>');
			},					
			onComplete : function(id, fileName, responseJSON){
				if(responseJSON.success){					
					$('#upload_size').append('<input type="checkbox" id="file-' + id + '" name="file_size[]" checked value="' + responseJSON.size/1024 + ' Kb">');					
					$('#file-' + id).html(fileName);										
				}else{
					console.log(responseJSON.error);
				}
			}
			
			});
	});
		
</script>