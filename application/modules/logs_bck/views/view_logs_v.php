<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Done</title>
		<script type="text/javascript" src="<?=load_global_assets('jquery/js/jquery.js');?>"></script>
		<script type="text/javascript" src="<?=load_global_assets('bootstrap_24/js/bootstrap.js');?>"></script>
		<script type="text/javascript" src="<?=load_global_assets('system/js/fileuploader.js')?>"></script>
		<link type="text/css" rel="stylesheet" href="<?=load_global_assets('bootstrap_24/css/bootstrap.css');?>" />
		<!--link type="text/css" rel="stylesheet" href="<?=load_global_assets('bootstrap_24/css/bootstrap-responsive.css');?>" /-->
		<link type="text/css" rel="stylesheet" href="<?=m_load_module_assets('logs', 'css/style.css');?>" />
		<link href="<?=load_global_assets('system/css/fileuploader.css')?>" type="text/css" rel="stylesheet" />
	</head>
	
	<body>
		<?php $this->load->view('include/nav');?>
		<?php $this->load->view('add_log_v');?>
		<div class="container" style="padding-top: 20px;">
		<div class="row">									
			<div class="span5">
				<ul id="tabs" class="nav nav-tabs nav-stacked" data-tabs="tabs">
					<?php							
						foreach($groups as $key){								
							echo "<li><a href='#logs' onclick='show_logs(".$key['group_id'].")' data-group='".$key['group_id']."' data-toggle='tab'><i class='icon-chevron-right'></i>".$key['group_name']."</a></li>";
						}
					?>												
				</ul>
			</div>				
			<div class="span19">
				<div class="tab-content">					
					<!-- Logs rendered here through ajax -->				
				</div>	
				<div class="row" id="more_logs">
					<div class="span19">			
						<button class="btn btn-primary" id="more_logs_btn"><i class="icon-white icon-chevron-down"></i> More Logs</button>						
					</div>
				</div>				
			</div>	
		</div>
		</div>
		<script type="text/javascript">									


		/*Load first tab contents on page load*/
			$(function(){
				
				first_load();				
				
			});
			
			/*
			 * Display more logs through arrow down button
			 * 
			 * */
			
			$("#more_logs_btn").click(function(){
				
				var group_id = ($("#tabs").children(".active").children().attr("data-group"));
				console.log(group_id);
				$.ajax({
					type : "POST",
					dataType: "json",
					url : "<?=base_url()?>logs/view/display_logs",					
					//data : "&group_id=" + group_id + "&more_logs=true",	
					data : {group_id : group_id, more_logs : 0},						
					success : function (data){	
						$("div.tab-content").append(data.logs_v);//logs_v
						//$(data.debug).hide().appendTo($("div.tab-content")).slideDown("slow"); //some animation test
					},
					error : function(data){
						console.log(data);
					}
				});
			});
			
			function show_logs(group_id){			
				$('#group_id').val(group_id); //adds value to hidden field in add_log form to determine which group to share
				
				$.ajax({
					type : "POST",
					dataType: "json",
					url : "<?=base_url()?>logs/view/display_logs",					
					//data : "&group_id=" + group_id + "&more_logs=false",
					data : {group_id : group_id, more_logs : 1},
					success : function (data){	
						$("div#logs").remove();						
						$("div.tab-content").append(data.logs_v);//logs_v				
					},
					error : function(data){
						console.log(data);
					}
				});
			}
			
			function first_load(){
				/* Make first item in vertical tab active*/
				$("#tabs li:first").attr("class", "active");
				
				$.ajax({
					type : "POST",
					dataType: "json",
					url : "<?=base_url()?>logs/view/display_logs",					
					//data : "&group_id=" + <?=$groups[0]['group_id']?> + "&more_logs=false", //takes first group
					data : {group_id : <?=$groups[0]['group_id']?>, more_logs : 1},					
					success : function (data){							
						$("div.tab-content").append(data.logs_v); //logs_v
					}
				});
				
			}
			
		</script>
	</body>
</html>