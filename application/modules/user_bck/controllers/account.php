<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php
/*
 * 
 *controller for the users registration 
 * 
 * */
 class Account extends MX_Controller{
	
	var $data;
	
	public function __construct(){
		parent::__construct();
		$this -> load -> helper("form");
		$this -> load -> library("form_validation");
		$this->load->library('rest_client/curl');
		$this -> load -> helper("url");
		
		$this -> load -> model('user_model');
		
		$this -> data = array();
	}
	
	public function get_captcha(){
		$con = curl_init('http://api.nicheez.zoog/register/user');
		curl_setopt ($con, CURLOPT_RETURNTRANSFER, true);
		$temp_file = tempnam('/tmp', 'nich');
        curl_setopt($con, CURLOPT_COOKIEJAR,$temp_file);
		$this->session->set_userdata('tmp_file', $temp_file);
		curl_setopt($con,CURLOPT_HTTPHEADER,array('appid : 1234','secret : 6b2ded51d81a4403d8a4bd25fa1e57ee'));
		$captcha = curl_exec($con);
		curl_close($con);
		$captcha = json_decode($captcha);
		
		return $captcha -> data -> url;
	}
	
	public function register(){
		

		if($this -> session -> userdata('is_logged')){
			redirect('profile','refresh');
		}else{

			$this -> form_validation -> set_rules('username','username','required|trim');
			$this -> form_validation -> set_rules('email','Email','required|trim|valid_email');
			$this -> form_validation -> set_rules('password','Password','required|trim|matches[cpassword]');
			$this -> form_validation -> set_rules('cpassword','Confirm Password','required|trim|matches[password]');
			$this -> form_validation -> set_rules('captcha','Captcha','required|trim');
			$this->form_validation->set_error_delimiters('<span class="error">', '</span>');
			if($this -> form_validation -> run() == FALSE){
				$this -> data['message'] = " ";
				$this -> data['captcha'] = $this -> get_captcha();
				$this -> load -> view("register_form",$this -> data);
			}else{
				$username = $this -> input -> post('username');
				$email = $this -> input -> post('email');
				$password = $this -> input -> post('password');
				$captcha = $this -> input -> post('captcha');
				
				$parameters = array(
				'captcha'	=>	$captcha,
				'username'	=>	$username,
				'password'	=>	$password,
				'email'		=>	$email
				);
				
				$success = $this -> register_user($parameters);
				
				if($success -> error == 1){
					
					if($success -> message == 'captcha mismatch'){
						$this -> data['message'] = $success -> message;
						$this -> data['captcha'] = $this -> get_captcha();
						$this -> load -> view("register_form",$this -> data);
					}
					if($success -> message == 'validation error'){
							$this -> data['message'] = "";
						foreach($success -> data as $value){
							$this -> data['message'] .= $value;
						}
						
						$this -> data['captcha'] = $this -> get_captcha();
						$this -> load -> view("register_form",$this -> data);
					}
					
				}
				if($success -> error == 0){
							
					$done = $this -> user_model -> insert_user_information($success -> data, $username, $email);
					$user_info =  $this->curl->init()->add_cookie('user')->post('auth/login',array(
						'username'      =>  $username,
						'password'      =>  $password
					));

					$user_info = json_decode($user_info);
					if($user_info -> error == 0){
						
						$nicheez_id = $user_info-> data -> user_id;
						$success = $this -> user_model -> data_login($nicheez_id);
						
						$info = array();
						
						foreach ($success->result() as $value){
							
							$info['user_id'] = $value -> user_id;
							$info['username'] = $value -> username;
							$this -> session -> set_userdata('avatar',$value -> avatar);
						}
						$this -> session -> set_userdata('is_logged',$info);
						
						redirect('','refresh');

					}else{
						$this -> load -> view("login_view");
					}
					// if($done){
					// 	redirect('activate','refresh');
					// }
				}
				
			}
		
		}
	}

	public function register_user($parameters){
		
		$con = curl_init('http://api.nicheez.zoog/register/user');
		curl_setopt($con, CURLOPT_POST, true);
		curl_setopt($con, CURLOPT_POSTFIELDS, $parameters);
		curl_setopt ($con, CURLOPT_RETURNTRANSFER, true);
		$temp_file = $this->session->userdata('tmp_file');
        curl_setopt($con, CURLOPT_COOKIEFILE,$temp_file);
		curl_setopt($con,CURLOPT_HTTPHEADER,array('appid : 1234','secret : 6b2ded51d81a4403d8a4bd25fa1e57ee'));
		$register = curl_exec($con);
		curl_close($con);
		$register = json_decode($register);
		//print_r($register);
		return $register;
	}
	
	 public function profile(){


		
	 	if($this -> session -> userdata('is_logged')){

	 		$user = $this -> session -> userdata('is_logged');

	 		$user_id = $user['user_id'];
	 		$this -> data['profile'] = $this -> session -> userdata('avatar');
	 		
	 		$info = $this -> user_model -> is_activated($user_id);

	 		if($info[0] -> is_verified == 0){

	 			redirect('activate','refresh');
	 		}else{


	 			$user_info =  $this->curl->init()->cookie()->post('users/view_profile');
			 	$this-> data['user_info'] = json_decode($user_info);
			 	
				$this -> load -> view("profile_view",$this -> data);
	 		}

	 		

	 	}else{

	 		redirect('login','refresh');
	 	}

		
	}

	
	public function update(){
		
		$firstname = $this -> input -> post('firstname');
		$lastname = $this -> input -> post('lastname');
		$birthdate =  $this -> input -> post('birthdate');
		$phone_number = $this -> input -> post('phonenumber');
		$address = $this -> input -> post('address');

		$parameters =  array(
			'firstname'	=>  $firstname,
			'lastname'	=>  $lastname,
			'birthdate'	=>  $birthdate,
			'phone_number'	=>  $phone_number,
			'address'	=>  $address
        );
		
		$user_info =  $this->curl->init()->cookie()->post('users/update_profile',$parameters);
		

		$user_info = json_decode($user_info);

		redirect('profile','refresh');


	}
	
	 public function activate(){
		
	 	if($this -> session -> userdata('is_logged')){

		 $this -> form_validation -> set_rules('verification_code','Activation Code','required|trim');
			 $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
			 if($this -> form_validation -> run() == FALSE){
				$this -> data['message'] = " ";
				$this -> load -> view('activation_form',$this -> data);
			 }else{
				
				$code = $this -> input -> post('verification_code');
				$log = $this -> session -> userdata('is_logged');
				
				$user_id =  $log['user_id'];
				$this -> user_model -> activate_account($code, $user_id);
				
				redirect('profile','refresh');
			 }
		}else{
			redirect('','refresh');
		}
	}

	
	 public function dummy(){
		 $user_info =  $this->curl->init()->add_cookie('user')->post('auth/login',array(
		 'username'      =>  'rlaurente',
		 'password'      =>  'rlaurente'
		 ));
	 }

	public function profile_upload(){

		if($this -> session -> userdata('is_logged')){


			$config['upload_path'] = './application/views/assets/new/uploads/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '100';
			$config['max_width']  = '1024';
			$config['max_height']  = '768';

			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload())
			{
				$error = array('error' => $this->upload->display_errors());
				echo "<pre>";
				print_r($error);
				echo "</pre>";
			}
			else
			{
				$user = $this -> session -> userdata('is_logged');
				$user_id = $user['user_id'];
				$data = $this->upload->data();
				// echo "<pre>";
				// print_r($data['file_name']);
				// echo "</pre>";
				$this -> session -> unset_userdata('avatar');
				$this -> session -> set_userdata('avatar',$data['file_name']);
				$image = $data['file_name'];
				$this -> user_model ->  update_image($image,$user_id);

				redirect('profile','refresh');
			}

		}else{
			redirect('profile','refresh');
		}


	}
 }

