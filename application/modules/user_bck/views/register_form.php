<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bootstrap</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php $this -> load -> view('loader/head_tag');?>
	</head>
	<body>

		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<button class="btn btn-navbar" type="button" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="brand" href="#">Done System</a>
					<div class="nav-collapse collapse">
						<ul class="nav pull-right">
							<li><a href="#">Welcome back Jonathan! </a></li>
							<li class="dropdown">
								<a class="dropdown-toggle" id="dLabel"  role="button"  data-toggle="dropdown" data-target="#" href="#"><b class="icon-envelope"> </b> Notification <b class="caret"></b></a>
								
								<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
									<li class=""><a href="#">Bryan invited you to join BLUH Group</a></li>
									<li class="divider"></li>
									<li class=""><a href="#">Hara accepted your request.</a></li>
									<li class="divider"></li>
									<li class=""><a href="#">Nino added you in Chugs Group.</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a class="dropdown-toggle" id="dLabel"  role="button" data-toggle="dropdown" data-target="#" href="#"><b class="icon-cog"> </b> Settings <b class="caret"></b></a>
								 
								<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
									<li class=""><a href="<?php echo base_url('profile')?>"><b class="icon-user"></b> Profile</a></li>
									<li class=""><a href="<?php echo base_url('profile')?>"><b class="icon-signal"></b> Account</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-top: 100px;">
			<div class="container">
				<?php echo anchor('login','Login');?>
				<span><?php echo $message?></span>
				<?php echo validation_errors()?>
				<?php echo form_open("register","class='form-horizontal'");?>
					<fieldset>
						<legend>Register</legend>
						<!-- USERNAME -->
						<div class="control-group">
						    <label class="control-label" >Username</label>
						    <div class="controls">
						    	<?php echo form_error('username');?>
						      	<input type="text" value="<?php echo set_value('username');?>" name="username" placeholder="Username"/>
						    </div>
					  	</div>
					  	<!-- EMAIL -->
					  	<div class="control-group">
					  	    <label class="control-label" for="inputEmail">Email</label>
					  	    <div class="controls">
					  	    	<?php echo form_error('email');?>
					  	      	<input type="text" id="inputEmail" name="email" value="<?php echo set_value('email'); ?>" placeholder="Email">
					  	    </div>
					  	 </div>
					  	<!-- PASSWORD -->
						<div class="control-group">
						    <label class="control-label" for="inputPassword">Password</label>
						    <div class="controls">
						    	<?php echo form_error('password');?>
						      	<input type="password" name="password" id="inputPassword" placeholder="Password">
						    </div>
						</div>
						<!-- CONFIRM PASSWORD -->
						<div class="control-group">
						    <label class="control-label" for="inputPassword">Confirm Password</label>
						    <div class="controls">
						      	<input type="password" name="cpassword" id="inputPassword" placeholder="Confirm Password">
						    </div>
						</div>
						<!-- captcha -->
						<div class="control-group">
						    <label class="control-label">Captcha</label>
						    <div class="controls">
						    	<img src="<?php echo $captcha?>"/>
						    </div>
						</div>
						<div class="control-group">
						    <label class="control-label"></label>
						    <div class="controls">
						     	<input type="text" type="text" name="captcha" placeholder="captcha">
						    </div>
						</div>
					  	<div class="control-group">
						    <div class="controls">
						      <label class="checkbox">
						        
						      </label>
						      <button type="submit" class="btn">Register</button>
						    </div>
					  	</div>
					<fieldset>
				<?php echo form_close();?>
			</div>
		</div>




	<?php echo $this -> load -> view('loader/footer_tags');?>
	</body>
</html>