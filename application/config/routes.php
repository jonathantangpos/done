<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "user/account/register";
$route['404_override'] = '';

$route['login'] = 'authenticate/validate/login';
$route['profile'] = 'user/account/profile';
$route['profile/upload'] = 'user/account/profile_upload';
$route['profile/update'] = "user/account/update";
$route['logout'] = 'authenticate/validate/logout';
$route['register'] = 'user/account/register';
$route['activate'] = 'user/account/activate';
$route['fbregister'] = 'authenticate/validate/fbregister';

$route['settings'] = 'settings/settings/account';
$route['notifications'] = 'notifications/notifications/view_notifications';


$route['group/create'] = 'group/group/create_group';
$route['group/update'] = 'group/group/update_group';
$route['group/delete'] = 'group/group/delete_group';
$route['groups'] = "group/group/view_group";
$route['groups/member/add'] = "group/group/add_group_member";
$route['groups/member/accept'] = "group/group/confirm_membership";
$route['groups/members'] = "group/group/get_members";


$route['settings/upload'] = "settings/settings/upload_image";
$route['settings/update'] = "settings/settings/update_profile";

$route['logs'] = "logs/view/index";


/* End of file routes.php */
/* Location: ./application/config/routes.php */