-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 05, 2013 at 02:01 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `done`
--

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `log_id` int(10) NOT NULL AUTO_INCREMENT,
  `log_content` text NOT NULL,
  `log_date` datetime NOT NULL,
  `log_author` int(11) NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='contains the entity between the users and logs' AUTO_INCREMENT=29 ;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`log_id`, `log_content`, `log_date`, `log_author`) VALUES
(1, 'Hello!', '2012-11-14 04:21:40', 2),
(2, 'Hello 2...', '2012-11-14 00:00:00', 2),
(3, 'Log by atan....', '2012-11-15 00:00:00', 1),
(4, 'another log by atan', '2012-11-15 00:00:00', 1),
(5, 'new logs..', '2012-11-16 00:00:00', 1),
(6, 'New log on Design group by bryan', '2012-11-17 00:00:00', 2),
(7, 'hello!', '2012-11-17 00:00:00', 2),
(8, 'asdf', '2012-11-19 00:00:00', 2),
(9, 'Another log on the design group by bserad!', '2012-11-19 00:00:00', 2),
(10, 'Another pa jud share sa design by bserad....', '2012-11-20 00:00:00', 2),
(11, 'dugangan pa jud be...', '2012-11-23 00:00:00', 2),
(12, 'New log on design...', '2012-12-06 00:00:00', 2),
(13, 'Another log jan 2', '2013-01-02 00:00:00', 2),
(14, 'log nasad...', '2013-01-02 05:21:50', 2),
(15, 'Today is jan 9... first', '2013-01-09 02:39:49', 2),
(16, 'jan 9. second..', '2013-01-09 02:51:33', 2),
(17, 'jan 9.. 3rd..', '2013-01-09 02:52:17', 2),
(18, 'testingan daw...', '2013-01-09 04:02:49', 2),
(19, 'hahay... hapit na jud!', '2013-01-09 04:51:08', 2),
(20, 'lets see.. adding comma, hehi...', '2013-01-09 07:18:48', 2),
(21, 'add sad ko diri bi...', '2013-01-09 07:20:55', 2),
(22, 'dugangan ug naay comman, ....', '2013-01-09 07:21:12', 2),
(23, 'test on new.. jan10', '2013-01-10 09:12:00', 2),
(24, 'new log nasad...', '2013-01-10 10:37:23', 2),
(25, 'log loglog', '2013-01-10 10:37:41', 2),
(26, 'logs... done eating', '2013-01-10 10:38:16', 2),
(27, 'just eat chicken', '2013-01-10 10:38:31', 2),
(28, 'eat manok... nam nam', '2013-01-10 10:39:15', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
