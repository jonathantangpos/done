-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 08, 2012 at 10:27 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `meridia2_products`
--

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `client_id_pk` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `street_address` varchar(200) NOT NULL,
  `city_address` varchar(200) NOT NULL,
  `zip_address` varchar(200) NOT NULL,
  `country_address` varchar(200) NOT NULL,
  `contact_number` varchar(200) NOT NULL,
  PRIMARY KEY (`client_id_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `country_id_pk` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) NOT NULL,
  `code` varchar(200) NOT NULL,
  `continent` varchar(200) NOT NULL,
  `continent_code` varchar(200) NOT NULL,
  PRIMARY KEY (`country_id_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE IF NOT EXISTS `manufacturer` (
  `manufacturer_id_pk` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `country_id_fk` int(11) NOT NULL,
  `city_address` varchar(200) NOT NULL,
  `state_address` varchar(200) NOT NULL,
  `street_address` varchar(200) NOT NULL,
  `contact_number` varchar(200) NOT NULL,
  `phone_number` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `port` varchar(200) NOT NULL,
  `distance` float NOT NULL,
  `sailing_schedule` varchar(200) NOT NULL,
  `factory_code` varchar(200) NOT NULL,
  PRIMARY KEY (`manufacturer_id_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_id_pk` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `date_created` varchar(200) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0 - disabled, 1 - enabled',
  PRIMARY KEY (`product_id_pk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id_pk`, `name`, `date_created`, `status`) VALUES
(1, 'Nightstand', '2011-12-28 07:50:10', 1),
(2, 'Dining Table', '2011-12-28 07:50:10', 1),
(3, 'Credenza', '2011-12-28 07:50:10', 1),
(4, 'Cocktail Table', '2011-12-28 07:50:10', 1),
(5, 'Side Table', '2011-12-28 07:50:10', 0),
(6, 'Luggage Bench', '2011-12-28 07:50:10', 1),
(7, 'Dining Chair', '2011-12-28 07:50:10', 1),
(8, 'Sofa', '2011-12-28 07:50:10', 1),
(9, 'Lounge Chair', '2011-12-28 07:50:10', 1),
(10, 'Desk Chair', '2011-12-28 07:50:10', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_group`
--

CREATE TABLE IF NOT EXISTS `product_group` (
  `product_group_id_pk` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`product_group_id_pk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `product_group`
--

INSERT INTO `product_group` (`product_group_id_pk`, `name`) VALUES
(1, 'archive'),
(2, 'collection nominies'),
(3, 'collections');

-- --------------------------------------------------------

--
-- Table structure for table `product_group_reference`
--

CREATE TABLE IF NOT EXISTS `product_group_reference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id_fk` int(11) NOT NULL,
  `product_group_id_fk` int(11) NOT NULL,
  `product_type_id_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `product_group_reference`
--

INSERT INTO `product_group_reference` (`id`, `product_id_fk`, `product_group_id_fk`, `product_type_id_fk`) VALUES
(1, 1, 1, 3),
(2, 2, 1, 3),
(3, 3, 1, 3),
(4, 4, 1, 3),
(5, 5, 1, 3),
(6, 6, 1, 4),
(7, 7, 1, 4),
(8, 8, 1, 4),
(9, 9, 1, 4),
(10, 10, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `product_group_reference_history`
--

CREATE TABLE IF NOT EXISTS `product_group_reference_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id_fk` int(11) NOT NULL,
  `product_group_id_fk` int(11) NOT NULL,
  `product_type_id_fk` int(11) NOT NULL,
  `date_altered` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_identity`
--

CREATE TABLE IF NOT EXISTS `product_identity` (
  `product_identity_pk` int(11) NOT NULL AUTO_INCREMENT,
  `product_id_fk` int(11) NOT NULL,
  `client_product_description` text NOT NULL,
  `client_map_code` varchar(200) NOT NULL,
  `client_map_location` varchar(200) NOT NULL,
  `client_product_number` varchar(200) NOT NULL,
  PRIMARY KEY (`product_identity_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_sku_reference`
--

CREATE TABLE IF NOT EXISTS `product_sku_reference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sku_reference_id_fk` int(11) NOT NULL,
  `product_id_fk` int(11) NOT NULL,
  `manufacturer_id_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_type`
--

CREATE TABLE IF NOT EXISTS `product_type` (
  `product_typ_id_pk` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`product_typ_id_pk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `product_type`
--

INSERT INTO `product_type` (`product_typ_id_pk`, `name`) VALUES
(1, 'Case Goods'),
(2, 'Seating Goods'),
(3, 'Textiles'),
(4, 'Lighting'),
(5, 'Decor');

-- --------------------------------------------------------

--
-- Table structure for table `product_type_group`
--

CREATE TABLE IF NOT EXISTS `product_type_group` (
  `product_type_group_id_pk` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`product_type_group_id_pk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `product_type_group`
--

INSERT INTO `product_type_group` (`product_type_group_id_pk`, `name`) VALUES
(1, 'Furniture'),
(2, 'Non-furniture');

-- --------------------------------------------------------

--
-- Table structure for table `product_type_group_reference`
--

CREATE TABLE IF NOT EXISTS `product_type_group_reference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_type_id_fk` int(11) NOT NULL,
  `product_type_group_id_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `product_type_group_reference`
--

INSERT INTO `product_type_group_reference` (`id`, `product_type_id_fk`, `product_type_group_id_fk`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 2),
(4, 4, 2),
(5, 5, 2);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `project_id_pk` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`project_id_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sku_reference`
--

CREATE TABLE IF NOT EXISTS `sku_reference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(200) NOT NULL,
  `product_type_id_fk` int(11) NOT NULL,
  `product_category_id_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
